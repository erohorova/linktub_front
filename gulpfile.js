/**
 *  Welcome to your gulpfile!
 *  The gulp tasks are split into several files in the gulp directory
 *  because putting it all here was too long
 */

'use strict';

var gulp = require('gulp');
var wrench = require('wrench');
var gulpNgConfig = require('gulp-ng-config');
var awspublish = require('gulp-awspublish');
var fs = require('fs')
var cloudfiles = require("gulp-cloudfiles");

/**
 *  This will load all js or coffee files in the gulp directory
 *  in order to load all gulp tasks
 */
wrench.readdirSyncRecursive('./gulp').filter(function (file) {
  return (/\.(js|coffee)$/i).test(file);
}).map(function (file) {
  require('./gulp/' + file);
});

/**
 *  Default task clean temporaries directories and launch the
 *  main optimization build task
 */
gulp.task('default', ['clean'], function () {
  gulp.start('build');
});

gulp.task('config-dev', function () {
  gulp.src('config.json')
    .pipe(gulpNgConfig('linktubConfig', {
      environment: 'development'
    }))
    .pipe(gulp.dest('src/app/'))
});

gulp.task('config-prod', function () {
  gulp.src('config.json')
    .pipe(gulpNgConfig('linktubConfig', {
      environment: 'production'
    }))
    .pipe(gulp.dest('src/app/'))
});

gulp.task('deploy:s3', ['build', 'config-prod'], function () {
  var AWS = require('aws-sdk');
  var publisher = awspublish.create({
    region: 'us-east-1',
    params: {
      Bucket: 'linktub'
    },
    credentials: AWS.config.loadFromPath('./aws.json').credentials
  });

  return gulp.src('dist/**/*.*')
    .pipe(publisher.publish({
      'Cache-Control': 'max-age=315360000, no-transform, public'
    }))
    .pipe(publisher.sync())
    .pipe(publisher.cache())
    .pipe(awspublish.reporter());
});

gulp.task('deploy:rackspace', ['build', 'config-prod'], function () {
  var rackspace = JSON.parse(fs.readFileSync('./rackspace.json'));
  return gulp.src('./dist/**', {
      read: false
    })
    .pipe(cloudfiles(rackspace, options));
});
