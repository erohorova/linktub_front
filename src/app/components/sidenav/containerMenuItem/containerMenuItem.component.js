(function () {
  'use strict';

  angular
    .module('linktub')
    .component('containerMenuItem', component());

  /* @ngInject */
  function component() {
    var component = {
      templateUrl: 'app/components/sidenav/containerMenuItem/containerMenuItem.html',
      controller: ContainerMenuItemController,
      controllerAs: 'vm',
      transclude: true,
      bindings: {
        showingNested: '=',
        text: '@',
        icon: '@',
        nestedStates: '<'
      }
    };

    return component;
  }

  /* @ngInject */
  function ContainerMenuItemController($state, $mdSidenav, lodash) {
    var vm = this;

    // Accessible functions
    vm.clickMenu = clickMenu;
    vm.isActive = isActive;

    // Functions
    function clickMenu() {
      vm.showingNested = !vm.showingNested;
    }

    function isActive() {
      return angular.isDefined(lodash.find(vm.nestedStates, function (state) {
        return state == $state.current.name;
      }));
    }
  }
})();
