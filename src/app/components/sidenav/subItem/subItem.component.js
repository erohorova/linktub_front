(function () {
  'use strict';

  angular
    .module('linktub')
    .component('subItem', component());

  /* @ngInject */
  function component() {
    var component = {
      templateUrl: 'app/components/sidenav/subItem/subItem.html',
      controller: SubItemController,
      controllerAs: 'vm',
      bindings: {
        text: '@',
        state: '@'
      }
    };

    return component;
  }

  /* @ngInject */
  function SubItemController($mdSidenav) {
    var vm = this;

    // Accessible functions
    vm.closeMenu = closeMenu;

    // Functions
    function closeMenu() {
      $mdSidenav('left').close();
    }
  }
})();
