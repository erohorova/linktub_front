(function () {
  'use strict';

  angular
    .module('linktub')
    .component('sidenav', sidenav());

  /* @ngInject */
  function sidenav() {
    var component = {
      templateUrl: 'app/components/sidenav/sidenav.html',
      controller: SidenavController,
      controllerAs: 'vm'
    };

    return component;
  }

  /* @ngInject */
  function SidenavController($log, $rootScope, toastr, OrdersService, SessionService) {
    var vm = this;

    // Accessible attributes
    vm.cartItems = [];

    //Accessible functions
    vm.notImplemented = notImplemented;
    vm.isAdvertiser = isAdvertiser;
    vm.isPublisher = isPublisher;

    // Functions
    vm.$onInit = function () {
      if(isAdvertiser()) {
        OrdersService.indexOrders(function (response) {
          $rootScope.cartItems = response;
        }, function () {
          $log.error('Error retrieving your orders.');
          toastr.error('There was an error retrieving your orders.')
        });
      }
    }

    function notImplemented() {
      toastr.warning('Not implemented yet!');
    }

    function isAdvertiser() {
      return SessionService.isAdvertiser();
    }

    function isPublisher() {
      return SessionService.isPublisher();
    }
  }
})();
