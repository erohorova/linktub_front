(function () {
  'use strict';

  angular
    .module('linktub')
    .component('profileMini', profileMini());

  /* @ngInject */
  function profileMini() {
    var component = {
      templateUrl: 'app/components/cards/profileMini/profileMini.html',
      controller: ProfileMiniController,
      controllerAs: 'vm',
      bindings: {
        profile: '='
      }
    };

    return component;
  }

  /* @ngInject */
  function ProfileMiniController($mdMedia, lodash, RoutesService) {
    var vm = this;

    // Accessible functions
    vm.$mdMedia = $mdMedia;
    vm.categoriesNames = categoriesNames;
    vm.hasImage = hasImage;
    vm.getImage = getImage;
    vm.editProfile = editProfile;

    init();

    // Functions
    function init() {
      vm.profileType = [{ name: vm.profile.profile_type }];
    }

    function categoriesNames() {
      return lodash.map(vm.profile.categories, 'name');
    }

    function hasImage() {
      return !!vm.profile.image;
    }

    function getImage() {
      return vm.profile.image || 'assets/images/no-image.png';
    }

    function editProfile() {
        RoutesService.editProfile(vm.profile.id);
    }
  }
})();
