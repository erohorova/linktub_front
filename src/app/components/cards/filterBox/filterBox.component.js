(function () {
  'use strict';

  angular
    .module('linktub')
    .component('filterBox', filterBox());

  /* @ngInject */
  function filterBox() {
    var component = {
      templateUrl: 'app/components/cards/filterBox/filterBox.html',
      controller: FilterBoxController,
      controllerAs: 'vm',
      bindings: {
        title: '@',
        minText: '@',
        maxText: '@',
        config: '=',
        suffix: '@',
        prefix: '@'
      }
    };

    return component;
  }

  /* @ngInject */
  function FilterBoxController() {}
})();
