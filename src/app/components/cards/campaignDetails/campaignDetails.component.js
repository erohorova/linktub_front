(function () {
  'use strict';

  angular
    .module('linktub')
    .component('campaignDetails', campaignDetails());

  /* @ngInject */
  function campaignDetails() {
    var component = {
      templateUrl: 'app/components/cards/campaignDetails/campaignDetails.html',
      controller: CampaignDetailsController,
      controllerAs: 'vm',
      bindings: {
        campaign: '=',
        onDelete: '&'
      }
    };

    return component;
  }

  /* @ngInject */
  function CampaignDetailsController($log, lodash, toastr, CampaignsService) {
    var vm = this;

    //Accessible variables
    vm.categoriesNames = categoriesNames;
    vm.selectedCategories = [];
    vm.showCategories = false;

    //Accessible functions
    vm.submitCategories = submitCategories;

    loadCategoriesFromCampaign();

    // Functions
    function categoriesNames() {
      return lodash.map(vm.campaign.categories, 'name');
    }

    function loadCategoriesFromCampaign() {
      vm.selectedCategories = vm.campaign.categories;
    }

    function submitCategories(campaign) {
      campaign.categories = vm.selectedCategories;
      vm.showCategories = false;

      CampaignsService.updateCampaign(campaign.id, campaign,
        function () {
          toastr.success('Campaign categories successfuly updated.');
        },
        function () {
          $log.error('Error updating categories.');
          toastr.error('There was a problem while updating the categories. Please retry.');
        });
    }
  }
})();
