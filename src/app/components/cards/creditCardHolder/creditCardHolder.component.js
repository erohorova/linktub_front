(function () {
  'use strict';

  angular
    .module('linktub')
    .component('creditCardHolder', creditCardHolder());

  /* @ngInject */
  function creditCardHolder() {
    var component = {
      templateUrl: 'app/components/cards/creditCardHolder/creditCardHolder.html',
      controller: CreditCardHolderController,
      controllerAs: 'vm'
    };

    return component;
  }

  /* @ngInject */
  function CreditCardHolderController($state, $mdDialog, toastr, lodash, CreditCardService, RoutesService) {
    var vm = this;

    // Accessible attributes
    vm.cards;

    // Accessible functions
    vm.addNewCC = addNewCC;
    vm.editCC = editCC;
    vm.removeCC = removeCC;

    // Functions
    vm.$onInit = function () {
      CreditCardService.indexCreditCards(function (response) {
        vm.cards = response.data.credit_cards;
      }, function () {
        toastr.error('Can\'t retrieved credit cards.');
      })
    }

    function addNewCC() {
      RoutesService.creditCards();
    }

    function editCC(id) {
      RoutesService.creditCards(id);
    }

    function removeCC(id) {
      var confirm = $mdDialog.confirm()
        .title('Would you like to delete this credit card?')
        .textContent('This credit card will be removed permanently.')
        .ariaLabel('Removing credit card')
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function () {
        CreditCardService.deleteCreditCard(id, function() {
          lodash.remove(vm.cards, function(card) {
            return card.id == id;
          });
          toastr.success('Successfully deleted the credit card');
        }, function() {
          toastr.error('There was a problem while deleting the credit card. Please retry');
        });
      });
    }
  }
})();
