(function () {
  'use strict';

  angular
    .module('linktub')
    .component('adSpot', adSpot());

  /* @ngInject */
  function adSpot() {
    var component = {
      templateUrl: 'app/components/cards/adSpot/adSpot.html',
      controller: AdSpotController,
      controllerAs: 'vm',
      bindings: {
        url: '='
      }
    };

    return component;
  }

  /* @ngInject */
  function AdSpotController($log, $mdDialog, $rootScope, toastr, OrdersService, PROFILE_TYPE, ORDER_CREATED_ORIGIN) {
    var vm = this;

    // Accessible functions
    vm.added = added;
    vm.checkIfOrdered = checkIfOrdered;
    vm.hasImage = hasImage;
    vm.getImage = getImage;

    // Functions
    function added() {
      return OrdersService.isOrdered(vm.url.id);
    }

    function checkIfOrdered() {
      OrdersService.alreadyOrdered(vm.url.id, function (response) {
        if (response.data.already_ordered) {
          var confirm = $mdDialog.confirm()
            .title('Warning!')
            .htmlContent('<p>Remember, you have already purchased a link on this domain for the campaign: <b>' +
              response.data.campaign_name + '</b> .</p><br> Please make note of this when viewing items in your cart.')
            .ok('I Understand')
            .cancel('Cancel');

          $mdDialog.show(confirm).then(function (result) {
            addOrderToCart()
          }, function () {});
        } else {
          addOrderToCart();
        }
      }, function () {
        $log.error('Error adding ad spot to your cart.');
        toastr.error('Please try again.');
      });
    }

    function hasImage() {
      return vm.url.image;
    }

    function getImage() {
      if (vm.url.profile_type === PROFILE_TYPE.BROKER) {
        return 'assets/images/avatarSearch.png';
      } else {
        return vm.url.image || 'assets/images/avatarSearch.png';
      }
    }

    function addOrderToCart() {
      OrdersService.createOrder(vm.url.id, ORDER_CREATED_ORIGIN.SEARCH, function () {
        $rootScope.isLoading = true;
        toastr.success('This ad spot has been added to your cart.');
      }, function () {
        $log.error('Error adding ad spot to your cart.');
        toastr.error('There was an error adding this ad spot to your cart, please retry.');
      });
    }
  }
})();
