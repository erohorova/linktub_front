(function () {
  'use strict';

  angular
    .module('linktub')
    .component('cartItem', {
      templateUrl: 'app/components/cards/cartItem/cartItem.html',
      controller: CartItemController,
      controllerAs: 'vm',
      bindings: {
        order: '=',
        list: '='
      }
    });

  /** @ngInject */
  function CartItemController($log, $mdDialog, toastr, lodash, OrdersService, ORDER_CREATED_ORIGIN) {
    var vm = this;

    // Accessible functions
    vm.toggle = toggle;
    vm.exists = exists;
    vm.blogPostDialog = blogPostDialog;
    vm.getBlogPostStatus = getBlogPostStatus;
    vm.isCreatedFromSearch = isCreatedFromSearch;
    vm.getOrderUrl = getOrderUrl;

    // Functions
    function toggle() {
      var pos = lodash.indexOf(vm.list, vm.order);
      (pos < 0) ? vm.list.push(vm.order): vm.list.splice(pos, 1);
    }

    function exists() {
      return lodash.indexOf(vm.list, vm.order) >= 0;
    }

    function getBlogPostStatus() {
      vm.order.showMessageError = hasToShowErrorMessage();
      var ad = vm.order.ad;
      if (!ad || !(ad.blog_post_title || ad.link_text || ad.full_pub_url)) {
        return 'Create blog post now';
      } else {
        return 'Edit blog post';
      }
    }

    function hasToShowErrorMessage() {
      var order = vm.order;
      return exists(order) && order.ad !== null &&
        order.ad.word === null && order.ad.blog_post_body === null;
    }

    function isOrderSelected(order) {
      return lodash.indexOf(vm.list, order) >= 0;
    }

    function blogPostDialog(ev) {
      checkIfOrdered();
      $mdDialog.show({
        controller: 'BlogPostController',
        controllerAs: 'blgCtrl',
        templateUrl: 'app/pages/cart/blogPost/blogPost.html',
        hasBackdrop: true,
        targetEvent: ev,
        clickOutsideToClose: true,
        escapeToClose: true,
        focusOnOpen: true,
        locals: {
          order: vm.order
        }
      }).then(function (blogPost) {
        vm.order.ad = blogPost;
      }, function () {
        $log.log('Dialog was canceled');
      });
    }

    function isCreatedFromSearch() {
      return vm.order.created_from === ORDER_CREATED_ORIGIN.SEARCH;
    }

    function getOrderUrl() {
      return (vm.order.created_from !== ORDER_CREATED_ORIGIN.SEARCH) ? vm.order.url.href : 'Use Quote function to see domains.';
    }

    function checkIfOrdered() {
      OrdersService.alreadyOrdered(vm.order.url.id, function (response) {
        vm.order.already_ordered = response.data.already_ordered;
        vm.order.already_ordered_campaign = response.data.campaign_name;
      }, function () {
        $log.error('Error adding finding order cart.');
        toastr.error('Please try again.');
      });
    }
  }
})();
