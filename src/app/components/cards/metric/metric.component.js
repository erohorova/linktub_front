(function () {
  'use strict';

  angular
    .module('linktub')
    .component('metric', metric());

  /* @ngInject */
  function metric() {
    var component = {
      templateUrl: 'app/components/cards/metric/metric.html',
      controller: MetricController,
      controllerAs: 'vm',
      bindings: {
        title: '@',
        badge: '@',
        active: '@',
        icon: '@'
      },
      transclude: true
    };

    return component;
  }

  /* @ngInject */
  function MetricController() {
    var vm = this;

    vm.iconCss = iconCss;
    vm.xlinkHref = xlinkHref;

    function iconCss() {
      return 'icon icon--' + vm.icon;
    }

    function xlinkHref() {
      return 'assets/images/icons.svg#' + vm.icon;
    }
  }
})();
