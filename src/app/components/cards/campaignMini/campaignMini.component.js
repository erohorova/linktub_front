(function () {
  'use strict';

  angular
    .module('linktub')
    .component('campaignMini', campaignMini());

  /* @ngInject */
  function campaignMini() {
    var component = {
      templateUrl: 'app/components/cards/campaignMini/campaignMini.html',
      controller: campaignMiniController,
      controllerAs: 'vm',
      bindings: {
        campaign: '='
      }
    };

    return component;
  }

  /* @ngInject */
  function campaignMiniController($mdMedia, $mdDialog, $log, lodash, toastr, RoutesService, CampaignsService, CAMPAIGN_STATE) {
    var vm = this;

    // Accessible variables
    vm.campaign.products = [{name: "sponsored"},{name: "search"}, {name: "social"}];
    vm.categoriesToDisplay = [];

    // Accessible functions
    vm.$mdMedia = $mdMedia;
    vm.categoriesNames = categoriesNames;
    vm.getProductBadgeClass = getProductBadgeClass;
    vm.hasImage = hasImage;
    vm.getImage = getImage;
    vm.isCampaignRejected = isCampaignRejected;
    vm.editCampaign = editCampaign;
    vm.destroy = destroy;

    init();

    // Functions
    function init() {
      vm.categoriesToDisplay = lodash.take(vm.campaign.categories, 2);
    }

    function categoriesNames() {
      return lodash.map(vm.campaign.categories, 'name');
    }

    function getProductBadgeClass(productName) {
      return 'badge badge-' + productName.toLowerCase();
    }

    function hasImage() {
      return !!vm.campaign.image;
    }

    function getImage() {
      return vm.campaign.image || 'assets/images/no_image_found.png';
    }

    function isCampaignRejected() {
      return vm.campaign.state === CAMPAIGN_STATE.REJECTED;
    }

    function editCampaign(id) {
      if (!isCampaignRejected()) {
        RoutesService.editCampaign(id);
      }
    }

    function destroy() {
      var confirm = $mdDialog.confirm()
        .title('Would you like to delete this campaign?')
        .textContent('This campaign will be removed permanently.')
        .ariaLabel('Removing campaign')
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function () {
        CampaignsService.deleteCampaign(vm.campaign.id, function () {
          toastr.success('Campaign was removed successfuly.');
          vm.campaign.destroyed = true;
        }, function () {
          $log.error('Error deleting campaign.');
          toastr.error('There was an error deleting campaign.');
        });
      });
    }
  }
})();
