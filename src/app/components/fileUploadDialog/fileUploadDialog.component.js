(function () {
  'use strict';

  angular
    .module('linktub')
    .component('fileUploadDialog', fileUploadDialog());

  /* @ngInject */
  function fileUploadDialog() {
    var component = {
      templateUrl: 'app/components/fileUploadDialog/fileUploadDialog.html',
      controller: FileUploadDialogController,
      controllerAs: 'vm',
      bindings: {
        title: '@',
        fileExt: '@'
      }
    };

    return component;
  }

  /* @ngInject */
  function FileUploadDialogController($mdDialog) {
    var vm = this;

    vm.submit = submit;
    vm.cancel = cancel;
    vm.getInputLabel = getInputLabel;

    vm.$onInit = function () {
      if (!vm.fileExt) {
        vm.fileExt = '.xlsx';
      }
      if (!vm.fileMax) {
        vm.fileMax = '25MB';
      }
      if (!vm.fileName) {
        vm.fileName = 'upload';
      }
    }

    function cancel() {
      $mdDialog.cancel();
    }

    function submit() {
      $mdDialog.hide(vm.file);
    }

    function getInputLabel() {
      return !vm.file ? 'Select a file to upload' : 'Selected file name';
    }
  }
})();
