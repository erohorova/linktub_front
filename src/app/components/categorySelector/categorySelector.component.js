(function () {
  'use strict';

  angular
    .module('linktub')
    .component('categorySelector', categorySelector());

  /* @ngInject */
  function categorySelector() {
    var component = {
      templateUrl: 'app/components/categorySelector/categorySelector.html',
      controller: CategorySelectorController,
      controllerAs: 'vm',
      bindings: {
        categories: '=ngModel',
        placeholder: '@',
        subtitle: '@',
        search: '&?',
        onChange: '&?',
        hideInput: '@',
        textToShow: '@?',
        hasChanged: '=?',
        hideCategories: '<',
        accentButton: '<',
        customButtonText: '@?',
        showOwnHideButton: '<',
        shrinked: '<'
      }
    };

    return component;
  }

  /* @ngInject */
  function CategorySelectorController($log, $scope, $mdMedia, lodash, toastr, CategoriesService) {
    var vm = this;

    // Accessible attributes
    vm.mCategories = [];

    // Accessible functions
    vm.isSelected = isSelected;
    vm.querySearch = querySearch;
    vm.toggleCategory = toggleCategory;
    vm.$mdMedia = $mdMedia;
    vm.buttonText = buttonText;
    vm.toggleCatList = toggleCatList;
    vm.toggleBtnText = toggleBtnText;

    // Functions
    vm.$onInit = function () {
      vm.calls = 0;
      vm.hasChanged = false;
      CategoriesService.indexCategories(function (response) {
        vm.mCategories = lodash.orderBy(response.data.categories, function (cat) {
          return cat.name.toLowerCase();
        }, ['asc']);
      }, function () {
        $log.error('Error retrieving the categories.');
        toastr.error('There was an error retrieving the categories.');
      });

      if (angular.isDefined(vm.onChange)) {
        $scope.$watchCollection('vm.categories', function (newValue, oldValue) {
          if (newValue !== oldValue) {
            vm.onChange();
            vm.hasChanged = vm.calls++ > 0 && oldValue.length !== 0;
          }
        });
      }
    }

    function isSelected(category) {
      return lodash.findIndex(vm.categories, ['name', category.name]) >= 0;
    }

    function toggleCategory(category) {
      var pos = lodash.findIndex(vm.categories, ['name', category.name]);
      (pos < 0) ? vm.categories.push(category): vm.categories.splice(pos, 1);
    }

    function querySearch(criteria) {
      var categoriesFiltered = vm.mCategories.filter(createFilterFor(criteria));
      var resultExists = categoriesFiltered.length > 0;
      if (resultExists) {
        return categoriesFiltered;
      } else {
        $scope.$$childTail.$mdContactChipsCtrl.searchText = "";
      }
    }

    function createFilterFor(query) {
      var uppercaseQuery = angular.uppercase(query);
      return function filterFn(category) {
        return category.name.indexOf(uppercaseQuery) != -1;
      }
    }

    function buttonText() {
      return vm.customButtonText ? vm.customButtonText : 'Show Categories';
    }

    function toggleCatList() {
      vm.hideCategories = !vm.hideCategories;
    }

    function toggleBtnText() {
      return vm.hideCategories ? 'Show Categories' : 'Hide Categories';
    }
  }
})();
