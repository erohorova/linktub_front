(function () {
  'use strict';

  angular
    .module('linktub')
    .component('chipsInput', chipsInput());

  /* @ngInject */
  function chipsInput() {
    var component = {
      templateUrl: 'app/components/filterComponentInput/chipsInput.html',
      controller: ChipsInputController,
      controllerAs: 'vm',
      bindings: {
        categories: '=ngModel',
        placeholder: '@',
        subtitle: '@',
        search: '&?',
        onChange: '&?',
        hideCategories: '<',
        hideToggle: '<'
      }
    };

    return component;
  }

  /* @ngInject */
  function ChipsInputController($log, $scope, $mdMedia, lodash, toastr, CategoriesService) {
    var vm = this;

    // Accessible attributes
    vm.mCategories = [];

    // Accessible functions
    vm.isSelected = isSelected;
    vm.querySearch = querySearch;
    vm.toggleCategory = toggleCategory;
    vm.$mdMedia = $mdMedia;
    vm.onAdd = onAdd;
    vm.onRemove = onRemove;
    vm.chipName = chipName;
    vm.checkChips = checkChips;
    vm.toggleCatList = toggleCatList;

    // Functions
    vm.$onInit = function () {
      vm.calls = 0;
      vm.hasChanged = false;
      CategoriesService.indexCategories(function (response) {
        vm.mCategories = lodash.orderBy(response.data.categories, function (cat) {
          return cat.name.toLowerCase();
        }, ['asc']);
      }, function () {
        $log.error('Error retrieving the categories.');
        toastr.error('There was an error retrieving the categories.');
      });

      if (angular.isDefined(vm.onChange)) {
        $scope.$watchCollection('vm.categories', function (newValue, oldValue) {
          if (newValue !== oldValue) {
            vm.onChange();
            vm.hasChanged = vm.calls++ > 0 && oldValue.length !== 0;
          }
        });
      }
    }

    function checkChips(criteria) {
      if (criteria !== '' && criteria.length >= 2) {
        var newObject = {
          name: onAdd,
          category: 'user'
        };
        vm.searchText = '';
        onAdd(criteria, false);
      } else {
        vm.searchText = '';
      }
    }

    function isSelected(category) {
      return lodash.findIndex(vm.categories, ['name', category.name]) >= 0;
    }

    function toggleCategory(category) {
      var pos = lodash.findIndex(vm.categories, ['name', category.name]);
      (pos < 0) ? vm.categories.push(category): vm.categories.splice(pos, 1);
    }

    function querySearch(criteria) {
      let res = vm.mCategories.filter(createFilterFor(criteria));
      res.map(function (item) {
        let uppercaseQuery = angular.uppercase(criteria);
        let upperKeyword = item.keyword && angular.uppercase(item.keyword);
        if (upperKeyword && (upperKeyword.indexOf(uppercaseQuery) !== -1)) {
          item.byKeywords = true;
          let keywordArray = item.keyword.split(', ');
          keywordArray = keywordArray.filter(keywordsFilter(criteria));
          keywordArray.forEach(function (keyItem) {
            let currentItem = Object.assign({}, item);
            currentItem.activeKey = keyItem;
            delete currentItem.$$hashKey;
            res.push(currentItem);
          });
          res = res.filter(function (element) {
            return (!element.byKeywords || element.activeKey)
          });
        }
      });
      return res;
    }

    function createFilterFor(query) {
      let uppercaseQuery = angular.uppercase(query);
      return function filterFn(category) {
        let upperKeyword = category.keyword && angular.uppercase(category.keyword);
        let nameResult = category.name && (category.name.indexOf(uppercaseQuery) !== -1);
        let keyResult = upperKeyword && (upperKeyword.indexOf(uppercaseQuery) !== -1);
        return nameResult || keyResult;
      }
    }

    function keywordsFilter(query) {
      let uppercaseQuery = angular.uppercase(query);
      return function filterFunction(keyword) {
        let upperKeyword = keyword && angular.uppercase(keyword);
        return upperKeyword.indexOf(uppercaseQuery) !== -1;
      }
    }

    function onAdd(chip, manuallyAdded) {
      if (manuallyAdded) {
        vm.categories.splice(vm.categories.length - 1, 1);
      }
      if (angular.isDefined(chip.name)) {
        let duplicate = false;
        let pos = lodash.findIndex(vm.categories, ['name', chip.name]);
        if ((pos > 0) && (vm.categories[pos].activeKey === chip.activeKey)) {
          duplicate = true;
        }
        (duplicate) ? vm.categories.splice(pos, 1) : vm.categories.push(chip);
      } else if(!checkIfLabeled(chip)) {
        let newObject = {
          name: chip,
          category: 'user'
        };
        let pos = lodash.findIndex(vm.categories, ['name', chip]);
        if (pos < 0) {
          vm.categories.push(newObject);
        }
      }
    }

    function checkIfLabeled(chip) {
      var pos = lodash.findIndex(vm.mCategories, ['name', angular.uppercase(chip)]);
      if(pos >= 0) { vm.categories.push(vm.mCategories[pos]) };
      return pos >= 0;
    }

    function onRemove(chip) {
      var pos = lodash.findIndex(vm.categories, ['name', chip]);
    }

    function chipName(chip) {
      if (chip.byKeywords && chip.activeKey) {
        return chip.activeKey;
      } else {
        return (angular.isDefined(chip.name)) ? chip.name : chip;
      }
    }

    function toggleCatList() {
      vm.hideCategories = !vm.hideCategories;
    }
  }
})();
