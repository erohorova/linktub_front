(function () {
  'use strict';

  angular
    .module('linktub')
    .component('faqSite', faqSite());

  /** @ngInject */
  function faqSite() {
    var component = {
      templateUrl: 'app/components/faqSite/faqSite.html',
      controller: FaqSiteController,
      controllerAs: 'vm',
      bindings: {
        role: '@',
      }
    };

    return component;
  }

  /** @ngInject */
  function FaqSiteController(ROLE) {
    var vm = this;

    // Accessible functions
    vm.isPublisher = isPublisher;

    // Functions
    function isPublisher() {
      return ROLE.PUB === vm.role;
    }
  }
})();
