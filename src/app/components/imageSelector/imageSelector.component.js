(function () {
  'use strict';

  angular
    .module('linktub')
    .component('imageSelector', imageSelector());

  /* @ngInject */
  function imageSelector() {
    var component = {
      templateUrl: 'app/components/imageSelector/imageSelector.html',
      controller: ImageSelectorController,
      controllerAs: 'vm',
      bindings: {
        image: '=',
        edit: '<',
        ngDisabled: '=ngDisabled'
      }
    };

    return component;
  }

  /* @ngInject */
  function ImageSelectorController($mdDialog, $scope, lodash, Upload) {
    var vm = this;

    vm.croppedDataUrl = '';
    vm.result = null;

    // Accessible functions
    vm.hide = hide;
    vm.select = select;
    vm.cancel = cancel;
    vm.lastImage = lastImage;
    vm.imageChange = imageChange;

    vm.$onInit = function () {
      if (angular.isUndefined(vm.edit)) {
        vm.edit = false;
      }
    }

    // Functions
    function hide() {
      $mdDialog.hide();
    }

    function cancel() {
      $mdDialog.cancel();
    }

    function select() {
      vm.result = vm.croppedDataUrl;
      vm.image = Upload.dataUrltoBlob(vm.result, 'profile_avatar' + Math.random() + '.jpeg');
      $mdDialog.hide();
    }

    function lastImage() {
      return lodash.last(vm.images);
    }

    function imageChange(files, file, newFiles, duplicateFiles, invalidFiles, evt) {
      vm.imageTemp = newFiles[0];
      $mdDialog.show({
        contentElement: '#cropper',
        parent: angular.element(document.body),
        targetEvent: evt,
        clickOutsideToClose: true
      });
    }
  }
})();
