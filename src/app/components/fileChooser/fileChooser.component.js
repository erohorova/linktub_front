(function () {
  'use strict';

  angular
    .module('linktub')
    .component('fileChooser', fileChooser());

  /* @ngInject */
  function fileChooser() {
    var component = {
      templateUrl: 'app/components/fileChooser/fileChooser.html',
      controller: FileChooserController,
      controllerAs: 'vm',
      bindings: {
        file: '=',
        fileName: '@',
        fileExt: '@',
        fileMax: '@',
        disable: '=ngDisabled',
        label: '@',
        text: '@',
        smaller: '@',
        smallInput: '@'
      }
    };

    return component;
  }
  /* @ngInject */
  function FileChooserController() {
    var vm = this;

    // Accessible functions
    vm.clearFile = clearFile;

    // Functions
    vm.$onInit = function () {}

    function clearFile() {
      vm.file = null;
    }
  }
})();
