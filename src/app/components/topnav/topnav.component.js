(function () {
  'use strict';

  angular
    .module('linktub')
    .component('topnav', component());

  /* @ngInject */
  function component() {
    var component = {
      templateUrl: 'app/components/topnav/topnav.html',
      controller: TopnavController,
      controllerAs: 'vm',
      transclude: true
    };

    return component;
  }

  /* @ngInject */
  function TopnavController($mdDialog, $state, toastr, RoutesService, SessionService, AuthenticationService,
    AccountService) {
    var vm = this;

    // Accessible attributes
    vm.email;
    vm.userMenuActive = false;
    vm.mobileMenuActive = false;

    // Accessible functions
    vm.openMenu = openMenu;
    vm.changeAccount = changeAccount;
    vm.signOut = signOut;
    vm.goToDashboard = goToDashboard;
    vm.accountMessage = accountMessage;
    vm.isAdvertiser = isAdvertiser;
    vm.isPublisher = isPublisher;
    vm.setMobileMenuInactive = setMobileMenuInactive;
    vm.toggleMobileMenu = toggleMobileMenu;
    vm.setUserMenuInactive = setUserMenuInactive;
    vm.toggleUserMenu = toggleUserMenu;

    initialize();

    // Functions
    function initialize() {
      vm.email = SessionService.sessionUser().email;
      if (!vm.email) {
        if (isAdvertiser()) {
          AccountService.getAdvertiser(function (response) {
            vm.email = response.data.email;
          });
        } else {
          AccountService.getPublisher(function (response) {
            vm.email = response.data.email;
          });
        }
      }
    }

    function openMenu($mdOpenMenu, ev) {
      $mdOpenMenu(ev);
    }

    function changeAccount(ev) {
      if (SessionService.switchAccount()) {
        RoutesService.dashboard();
      } else {
        var confirm = $mdDialog.confirm()
          .title('Are you sure?')
          .textContent('Remember that when you login your account will default to the original account you signed up with')
          .targetEvent(ev)
          .ok('Yes')
          .cancel('No');

        $mdDialog.show(confirm).then(function () {
          if (isAdvertiser()) {
            AccountService.createPublisher(function (response)  {
              SessionService.updateSession(response.data);
              RoutesService.dashboard();
            }, function () {
              toastr.error('There was an error creating the Publisher Account. Try again.');
            });
          } else if (isPublisher()) {
            AccountService.createAdvertiser(function (response)  {
              SessionService.updateSession(response.data);
              RoutesService.dashboard();
            }, function () {
              toastr.error('There was an error creating the Publisher Account. Try again.');
            });
          } else {
            toastr.error('No role is set to the account. Try again');
          }
        }, function () {
          return;
        });
      }
    }

    function accountMessage() {
      var message = '';
      if (SessionService.canSwithAccount()) {
        if (isAdvertiser()) {
          message = 'Switch to Publisher\'s side';
        } else if (isPublisher()) {
          message = 'Switch to Advertiser\'s side';
        }
      } else {
        if (isAdvertiser()) {
          message = 'Create a Publisher Account';
        } else if (isPublisher()) {
          message = 'Create an Advertiser Account';
        }
      }
      return message;
    }

    function signOut() {
      AuthenticationService.logout(RoutesService.signIn);
    }

    function goToDashboard() {
      RoutesService.dashboard();
    }

    function isAdvertiser() {
      return SessionService.isAdvertiser();
    }

    function isPublisher() {
      return SessionService.isPublisher();
    }

    function setMobileMenuInactive() {
      vm.mobileMenuActive = false;
    }

    function toggleMobileMenu() {
      vm.mobileMenuActive = !vm.mobileMenuActive;
    }

    function setUserMenuInactive() {
      vm.userMenuActive = false;
    }

    function toggleUserMenu() {
      vm.userMenuActive = !vm.userMenuActive;
    }
  }
})();
