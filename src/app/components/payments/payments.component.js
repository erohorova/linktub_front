(function () {
  'use strict';

  angular
    .module('linktub')
    .component('payments', payments());

  /* @ngInject */
  function payments() {
    var component = {
      templateUrl: 'app/components/payments/payments.html',
      controller: PaymentsController,
      controllerAs: 'vm'
    };

    return component;
  }

  /* @ngInject */
  function PaymentsController($mdDialog, $log, $mdMedia, $state, $scope, $rootScope,
    toastr, lodash, StatesService, CountriesService, PaymentsService,
    SelectedOrdersService, StorageService, CreditCardService, RoutesService, OrdersService,
    ERROR_CODES, CHECKOUT, ORDER_CREATED_ORIGIN) {
    var vm = this;

    vm.orders = [];
    vm.remember = true;
    vm.agreed = true;
    vm.ccType;
    vm.country = 'United States of America';
    vm.perPage = 1;
    vm.filtered = [];
    vm.errorLabel = '';
    vm.expDate = {
      month: '',
      year: ''
    };
    vm.ccNumber = '';
    vm.cvvNumber = '';
    vm.zipMask = '';
    vm.defaultCvvMask;
    vm.query = {
      filter: '',
      order: '-domain_authority',
      limit: 5,
      page: 1
    };
    vm.cards;

    // Accessible functions
    vm.showInfo = showInfo;
    vm.toggleZipMask = toggleZipMask;
    vm.toggleCvvMask = toggleCvvMask;
    vm.toggleCreditCardMask = toggleCreditCardMask;
    vm.submit = submit;
    vm.getTotalPrice = getTotalPrice;
    vm.$mdMedia = $mdMedia;
    vm.checkExpirationDate = checkExpirationDate;
    vm.openMenu = openMenu;
    vm.orderSendBack = orderSendBack;
    vm.creditCardsDialog = creditCardsDialog;
    vm.closeDialog = closeDialog;
    vm.setSelectedCard = setSelectedCard;
    vm.hasStoredCards = hasStoredCards;
    vm.cantPurchase = cantPurchase;
    vm.createdFromSearch = createdFromSearch;
    vm.createdFromQuote = createdFromQuote;

    vm.$onInit = function () {

      vm.orders = SelectedOrdersService.getOrders();
      CreditCardService.indexCreditCards(function (response) {
        vm.cards = response.data.credit_cards;
        vm.selectedCard = lodash.first(vm.cards);
      }, function () {
        toastr.error('There was an error retrieving credit cards.');
      });
      if (angular.isDefined(vm.orders) && vm.orders.length > 0) {
        StorageService.set(CHECKOUT, vm.orders);
      } else {
        vm.orders = StorageService.get(CHECKOUT);
      }

      vm.amount = vm.getTotalPrice();
      vm.months = lodash.range(1, 13);
      vm.years = lodash.range(moment().year(), moment().year() + 20);
      vm.states = StatesService.getAllUSStates();
      vm.countries = CountriesService.getAllCountries();

      // Watch on change of credit card type to setup the mask
      $scope.$watch('vm.ccType', function () {
        if (vm.ccType) {
          switch (vm.ccType.digits) {
          case 14:
            vm.ccNumberMask = '9999 999999 9999';
            break;
          case 15:
            vm.ccNumberMask = '9999 999999 99999';
            vm.defaultCvvMask = '9999'
            break;
          case 16:
            vm.ccNumberMask = '9999 9999 9999 9999';
            vm.defaultCvvMask = '999'
            break;
          }
        }
      })
    }

    function createdFromSearch(order) {
      return order && order.created_from === ORDER_CREATED_ORIGIN.SEARCH;
    }

    function createdFromQuote(order) {
      return order && _.includes([ORDER_CREATED_ORIGIN.QUOTE, ORDER_CREATED_ORIGIN.ARTICLE], order.created_from);
    }

    function getTotalPrice() {
      var price = 0;
      lodash.map(vm.orders, function (order) {
        price += order.url.shown_price;
      });
      return price;
    }

    function toggleZipMask() {
      if (!vm.zipMask) {
        vm.zipMask = '99999';
      } else if (!vm.zipCode) {
        vm.zipMask = '';
      }
    }

    function toggleCreditCardMask() {
      if (!vm.ccNumberMask) {
        vm.ccNumberMask = '9999 9999 9999 9999';
      } else if (!vm.ccNumber) {
        vm.ccNumberMask = '';
      }
    }

    function toggleCvvMask() {
      if (!vm.cvvMask || vm.cvvNumber) {
        vm.cvvMask = vm.defaultCvvMask || '999?9';
      } else if (!vm.cvvNumber) {
        vm.cvvMask = '';
      }
    }

    function showInfo() {
      $mdDialog.show(
        $mdDialog.alert()
        .clickOutsideToClose(true)
        .title('CVV number')
        .textContent("The CVV Number (\"Card Verification Value\") on your credit card or debit card is a 3 digit number on VISA®, MasterCard® and Discover® branded credit and debit cards. On your American Express® branded credit or debit card it is a 4 digit numeric code.")
        .ariaLabel('Notes')
        .ok('Got it!')
      );
    }

    function submit() {
      vm.selectedCard ? storedPaymentMethod() : newPaymentMethod();
    }

    function storedPaymentMethod() {
      createPayment(lodash.map(vm.orders, 'id'), null, vm.selectedCard.token);
    }

    function newPaymentMethod() {
      createPayment(lodash.map(vm.orders, 'id'), null, null);
      // PaymentsService.getPaymentToken(function (response) {
      //   var creditCard = {
      //     number: vm.ccNumber,
      //     cardholderName: vm.firstName + ' ' + vm.lastName,
      //     expirationMonth: vm.expDate.month,
      //     expirationYear: vm.expDate.year,
      //     cvv: vm.cvvNumber,
      //     billingAddress: {
      //       postalCode: vm.zipCode,
      //       firstName: vm.firstName,
      //       lastName: vm.lastName,
      //       streetAddress: vm.address,
      //       countryName: vm.country,
      //       locality: vm.city,
      //       region: vm.state
      //     }
      //   }
      //   if (angular.isDefined(vm.address2)) {
      //     creditCard.billingAddress.extendedAddress = vm.address2;
      //   }
      //   sendToken(response.data.token, creditCard);
      // }, function () {
      //   $log.error('There was an error getting token.');
      // });
    }

    function sendToken(token, cardInfo) {
      // $rootScope.isLoading = true;
      // if (token == null) {
      //   toastr.error('There was an error creating a token');
      //   $log.error('Token is null');
      //   return;
      // }
      // var client = new braintree.api.Client({
      //   clientToken: token
      // });
      //
      // if (client) {
      //   client.tokenizeCard(cardInfo, function (err, nonce) {
      //     if (err) {
      //       $log.error(err);
      //       toastr.error(err);
      //       return;
      //     }
      //     createPayment(lodash.map(vm.orders, 'id'), nonce, null);
      //   });
      // } else {
      //   $log.error('There was an error contacting the payment gateway.');
      //   toastr.error('There was an error connecting the payment gateway. Please try againg in a few minutes.');
      // }
    }

    function createPayment(ordersIds, payment_nonce, token) {
      PaymentsService.payment(payment_nonce, token, ordersIds, lastCCFourDigits(), vm.remember,
        function () {
          showDialogMessage('Thank you for your patronage!',
            'Please allow 5 days for Publishers to accept or decline your order.' +
            'The charge today will be pending during this time. For any publishers ' +
            'that reject your order, we will automatically void that portion of today\'s order.');
          StorageService.remove(CHECKOUT);
          OrdersService.indexOrders(function () {
            RoutesService.dashboard();
          });
        },
        function (response) {
          if(response.data.errors.masked_number) {
            toastr.error('IMPORTANT! You are trying to save a Credit Card that already exists. Please email admin@scalefluence.com immediately to resolve the issue.');
          } else {
            var error = parseInt(response.data.errors[0].code);
            switch (error) {
            case 2000:
              vm.errorLabel = "Your bank is unwilling to accept the transaction. Please contact your bank."
              break;
            case 2001:
              vm.errorLabel = "The account did not have " +
                "sufficient funds to cover the transaction amount."
              break;
            case 2005:
            case 81717:
            case 81715:
              vm.errorLabel = "The Credit Card Number is invalid. " +
                "Please double check and edit your card number. Thank you."
              break;
            default:
              vm.errorLabel = "We validate billing zip code, CVV, and expiration date. " +
                "If those are wrong, it will decline. If they are correct, " +
                "you need to check with your financial instution. They may likely be blocking this transaction."
            }
          }
        });
    }

    function showDialogMessage(title, message) {
      $mdDialog.show(
        $mdDialog.alert()
        .clickOutsideToClose(true)
        .title(title)
        .htmlContent(message)
        .ok('Got it!')
      );
    }

    function cantPurchase() {
      return !vm.agreed || angular.isUndefined(vm.orders) ||
        vm.orders.length === 0;
    }

    function checkExpirationDate() {
      if (vm.expDate.month && vm.expDate.year) {
        var toDay = new Date(moment().year() + '-' + (moment().month() + 1));
        var date = new Date(vm.expDate.year + '-' + vm.expDate.month);
        $scope.paymentForm.expDateMonth.$setValidity('past', date >= toDay);
      }
    }

    function lastCCFourDigits() {
      return vm.ccNumber.slice(vm.ccNumber.length - 4);
    }

    function openMenu($mdOpenMenu, ev) {
      $mdOpenMenu(ev);
    }

    function orderSendBack(order) {
      vm.orders.splice(order, 1);
      StorageService.set(CHECKOUT, vm.orders);

      if (vm.orders.length == 0) {
        toastr.info('No more items to checkout.');
        $state.go('cart');
      }
    }

    function creditCardsDialog() {
      $mdDialog.show({
        contentElement: '#myDialog',
        parent: angular.element(document.body),
        clickOutsideToClose: false
      });
    }

    function closeDialog() {
      vm.selectedCardId = null;
      $mdDialog.cancel();
    }

    function hasStoredCards() {
      return vm.cards && vm.cards.length > 0;
    }

    function setSelectedCard() {
      vm.selectedCard = lodash.find(vm.cards, function (card) {
        return card.id == vm.selectedCardId;
      })
      vm.selectedCardId = null;
      $mdDialog.hide();
    }
  }
})();
