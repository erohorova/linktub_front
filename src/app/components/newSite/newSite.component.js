(function () {
  'use strict';

  angular
    .module('linktub')
    .component('newSite', newSite());

  /** @ngInject */
  function newSite() {
    var component = {
      templateUrl: 'app/components/newSite/newSite.html',
      controller: NewSiteController,
      controllerAs: 'vm',
      bindings: {
        campaign: '=',
        showCategories: '=',
        cannotSelectCampaign: '=',
        showComponent: '=',
        onChange: '&?'
      }
    };

    return component;
  }

  /** @ngInject */
  function NewSiteController($scope, $log, $state, $mdDialog, lodash, toastr, CampaignsService) {
    var vm = this;

    // Accessible attributes
    vm.csv;
    vm.campaigns = [];
    vm.categories = [];
    vm.campaignUrl;
    vm.selectedCampaign;

    // Accessible functions
    vm.submit = submit;
    vm.clearCsv = clearCsv;
    vm.getSelectedCampaign = getSelectedCampaign;
    vm.isSelectedCategoriesEmpty = isSelectedCategoriesEmpty;
    vm.showDialog = showDialog;

    initialize();

    // Functions
    function initialize() {
      CampaignsService.indexCampaigns(function (response) {
        vm.campaigns = response.data.campaigns;
        if(angular.isDefined(vm.showCategories)) {
          vm.showCategories = vm.showCategories;
        }
        if (angular.isDefined(vm.campaign.id) && vm.campaign.id != '') {
          vm.selectedCampaign = lodash.find(vm.campaigns, function (cam) {
            return cam.id == vm.campaign.id;
          });
        }
      }, function () {
        $log.error('Error retrieving your campaigns.');
        toastr.error('There was an error retrieving your campaigns.');
      });
    }

    function getSelectedCampaign() {
      if (angular.isDefined(vm.selectedCampaign)) {
        vm.categories = vm.selectedCampaign.categories;
        return vm.selectedCampaign.name;
      } else {
        return 'Please select a campaign';
      }
    }

    function submit() {
      vm.selectedCampaign.url = vm.campaignUrl;
      vm.selectedCampaign.categories = vm.categories;
      vm.showComponent = false;
      if (vm.csv) {
        vm.selectedCampaign.csv = vm.csv;
        delete vm.selectedCampaign.url;
      } else if(angular.isDefined(vm.campaignUrl)) {
          clearCsv();
          delete vm.selectedCampaign.csv;
      }
      CampaignsService.updateCampaign(vm.selectedCampaign.id, vm.selectedCampaign,
        function () {
          vm.onChange();
          $state.go('manage-campaign', { campaignId: vm.selectedCampaign.id });
          toastr.success('Site successfuly added.');
        },
        function () {
          $log.error('Error submiting new site.');
          toastr.error('There was a problem while submiting the new site. Please retry.');
        });
    }

    function clearCsv() {
      vm.csv = null;
    }

    function isSelectedCategoriesEmpty() {
      if(vm.showCategories) {
        return false;
      }
      return vm.categories.length == 0;
    }

    function showDialog() {
      $mdDialog.show({
        controller: 'ModalCampaignController',
        controllerAs: 'modCampCtrl',
        templateUrl: 'app/pages/campaigns/modalCampaign/modalCampaign.html',
        clickOutsideToClose: true
      }).then(function (uploadedFile) {
        vm.csv = uploadedFile;
      });
    }
  }
})();
