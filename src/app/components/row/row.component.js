(function () {
  'use strict';

  angular
    .module('linktub')
    .component('row', row());

  /* @ngInject */
  function row() {
    var component = {
      templateUrl: 'app/components/row/row.html',
      controller: RowController,
      controllerAs: 'vm',
      bindings: {
        followers: '=',
        network: '=',
        header: '='
      }
    };

    return component;
  }

  /* @ngInject */
  function RowController($scope, lodash, PricingService) {
    var vm = this;

    vm.isSelected = isSelected;
    vm.around = around;
    vm.rowPricingIn = PricingService.getPricingIn();
    vm.rowPricingYt = PricingService.getPricingYt();
    vm.rowPricingFbTwPi = PricingService.getFbTwPiPricing();
    vm.rowPricingGA = PricingService.getGaPricing();

    vm.getHeader = getHeader;

    vm.$onInit = function () {
      $scope.$watch('vm.followers', function () {
        renderRow(vm.network);
      })
    }

    function getHeader() {
      return vm.header;
    }

    function renderRow(network) {
      var pos = getSelected(vm.followers, vm.network);
      vm.min = 0;
      vm.max = 7;

      if (isRowPricingFbTwPi(network)) {
        if (pos > 10) {
          vm.min = 7;
          vm.max = 14;
        } else if (pos > 3) {
          vm.min = pos - 3;
          vm.max = pos + 4;
        }
        vm.showedRows = vm.rowPricingFbTwPi.slice(vm.min, vm.max);
      } else if (isRowPricingYt(network)) {
        if (pos > 11) {
          vm.min = 8;
          vm.max = 15;
        } else if (pos > 3) {
          vm.min = pos - 3;
          vm.max = pos + 4;
        }
        vm.showedRows = vm.rowPricingYt.slice(vm.min, vm.max);
      } else if (isRowPricingInsta(network)) {
        if (pos > 12) {
          vm.min = 8;
          vm.max = 15;
        } else if (pos > 3) {
          vm.min = pos - 3;
          vm.max = pos + 4;
        }
        vm.showedRows = vm.rowPricingIn.slice(vm.min, vm.max);
      } else {
        if (pos > 13) {
          vm.min = 9;
          vm.max = 16;
        } else if (pos > 3) {
          vm.min = pos - 3;
          vm.max = pos + 4;
        }
        vm.showedRows = vm.rowPricingGA.slice(vm.min, vm.max);
      }
      if (vm.min > 0 && vm.min < 7) {
        vm.center = vm.min + 3;
        vm.preCenter = vm.min + 2;
        vm.posCenter = vm.min + 4;
      }
    }

    function isRowPricingFbTwPi(network) {
      return lodash.includes(['facebook', 'twitter', 'pinterest'], network);
    }

    function isRowPricingYt(network) {
      return 'youtube' == network;
    }

    function isRowPricingInsta(network) {
      return 'instagram' == network;
    }

    function between(x, min, max) {
      return x >= min && x < max;
    }

    function getSelected(value, network) {
      var redux = Math.floor(value / 1000);
      vm.center = -1;
      if (isRowPricingFbTwPi(network)) {
        vm.center = lodash.findIndex(vm.rowPricingFbTwPi, function (elem) {
          return between(redux, elem.range.min, elem.range.max);
        });
        vm.posCenter = vm.center < vm.rowPricingFbTwPi.length - 1 ? vm.center + 1 : -1;
      } else if (isRowPricingYt(network)) {
        vm.center = lodash.findIndex(vm.rowPricingYt, function (elem) {
          return between(redux, elem.range.min, elem.range.max);
        });
        vm.posCenter = vm.center < vm.rowPricingYt.length - 1 ? vm.center + 1 : -1;
      } else if (isRowPricingInsta(network)) {
        vm.center = lodash.findIndex(vm.rowPricingIn, function (elem) {
          return between(redux, elem.range.min, elem.range.max);
        });
        vm.posCenter = vm.center < vm.rowPricingIn.length - 1 ? vm.center + 1 : -1;
      } else {
        vm.center = lodash.findIndex(vm.rowPricingGA, function (elem) {
          return between(redux, elem.range.min, elem.range.max);
        });
        vm.posCenter = vm.center < vm.rowPricingGA.length - 1 ? vm.center + 1 : -1;
      }
      vm.preCenter = vm.center > 0 ? vm.center - 1 : -1;

      return vm.center;
    }

    function around(index) {
      return index == vm.preCenter || index == vm.posCenter;
    }

    function isSelected(index) {
      return index == vm.center;
    }
  }
})();
