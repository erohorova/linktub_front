(function () {
  'use strict';

  angular
    .module('linktub')
    .component('grid', grid());

  /* @ngInject */
  function grid() {
    var component = {
      templateUrl: 'app/components/grid/grid.html',
      controller: GridController,
      controllerAs: 'vm',
      bindings: {
        trustFlow: '<',
        domainAuthority: '<'
      }
    };

    return component;
  }

  /* @ngInject */
  function GridController($scope, lodash) {
    var vm = this;

    ////////// Attributes //////////
    vm.rows = 9;
    vm.around = [];
    vm.matrix = [
      ['  ', '10', '20', '30', '40', '50', '60', '70', '80', '90'],
      ['20', '$30', '$50', '$70', '$90', '$110', '$125', '$150', '$175', '$200'],
      ['30', '$50', '$70', '$90', '$110', '$125', '$150', '$175', '$200', '$250'],
      ['40', '$70', '$90', '$110', '$125', '$150', '$175', '$200', '$250', '$300'],
      ['50', '$90', '$110', '$125', '$150', '$175', '$200', '$250', '$300', '$350'],
      ['60', '$110', '$125', '$150', '$175', '$200', '$250', '$300', '$350', '$400'],
      ['70', '$125', '$150', '$175', '$200', '$250', '$300', '$350', '$400', '$450'],
      ['80', '$150', '$175', '$200', '$250', '$300', '$350', '$400', '$450', '$500'],
      ['90', '$175', '$200', '$250', '$300', '$350', '$400', '$450', '$500', '$600']
    ];

    ////////// Accessible functions //////////
    vm.isAround = isAround;
    vm.isTarget = isTarget;

    ////////// Functions //////////
    vm.$onInit = function () {
      vm.trustFlow = Math.floor(vm.trustFlow / 10) * 10;
      vm.domainAuthority = Math.floor(vm.domainAuthority / 10) * 10;

      vm.posX = lodash.findIndex(vm.matrix[0], function (trustFlow) {
        return trustFlow == vm.trustFlow;
      });

      vm.posY = lodash.findIndex(vm.matrix, function (row) {
        return row[0] == vm.domainAuthority;
      });

      vm.posTarget = ((vm.posY - 1) * vm.rows) + vm.posX + vm.posY - 1;
      vm.around = getAround();
    }

    function isAround(pos) {
      return lodash.findIndex(vm.around, function (id) {
        return pos == id;
      }) != -1;
    }

    function isTarget(id) {
      return id == vm.posTarget;
    }

    function getAround() {
      var around = new Array(8);
      around[0] = vm.posTarget - vm.rows - 2;
      around[1] = vm.posTarget - vm.rows - 1;
      around[2] = vm.posTarget - vm.rows;
      around[3] = vm.posTarget - 1;
      around[4] = vm.posTarget + 1;
      around[5] = vm.posTarget + vm.rows;
      around[6] = vm.posTarget + vm.rows + 1;
      around[7] = vm.posTarget + vm.rows + 2;
      return around;
    }
  }
})();
