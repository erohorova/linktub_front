(function () {
  'use strict';

  angular
    .module('linktub')
    .component('topnavext', component());

  /* @ngInject */
  function component() {
    var component = {
      templateUrl: 'app/components/topnavext/topnavext.html',
      controller: TopnavextController,
      controllerAs: 'vm'
    };

    return component;
  }

  /* @ngInject */
  function TopnavextController($mdDialog, $state, toastr, RoutesService, SessionService, AuthenticationService,
    AccountService) {
    var vm = this;

    // Accessible attributes
    vm.mobileMenuActive = false;
    vm.loginMenuActive = false;

    // Accessible functions
    vm.setMobileMenuInactive = setMobileMenuInactive;
    vm.toggleMobileMenu = toggleMobileMenu;
    vm.setLoginMenuInactive = setLoginMenuInactive;
    vm.toggleLoginMenu = toggleLoginMenu;

    function setMobileMenuInactive() {
      vm.mobileMenuActive = false;
    }

    function toggleMobileMenu() {
      vm.mobileMenuActive = !vm.mobileMenuActive;
    }

    function setLoginMenuInactive() {
      vm.loginMenuActive = false;
    }

    function toggleLoginMenu() {
      vm.loginMenuActive = !vm.loginMenuActive;
    }
  }
})();
