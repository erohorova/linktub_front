(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('ContentService', ContentService);

  /* @ngInject */
  function ContentService($http, lodash, Upload, EnvironmentConfig) {
    var CONTENTS_URL = EnvironmentConfig.API_END_POINT + '/advertisers/me/contents';

    var service = {
      indexReceivedContents: indexReceivedContents,
      indexPendingContents: indexPendingContents,
      showContent: showContent,
      createContent: createContent,
      downloadReport: downloadReport,
      uploadReport: uploadReport
    };

    return service;

    // Functions
    function indexReceivedContents(successCallback, errorCallback) {
      $http.get(CONTENTS_URL, {
        params: {
          scope: 'received'
        }
      }).then(successCallback, errorCallback);
    }

    function indexPendingContents(successCallback, errorCallback) {
      $http.get(CONTENTS_URL, {
        params: {
          scope: 'pending'
        }
      }).then(successCallback, errorCallback);
    }

    function showContent(contentId, successCallback, errorCallback) {
      $http.get(CONTENTS_URL + '/' + contentId).then(successCallback, errorCallback);
    }

    function createContent(content, successCallback, errorCallback) {
      $http.post(CONTENTS_URL, {
        content: {
          'campaign_id': content.campaign.id,
          'trust_flow_min': content.minTF,
          'trust_flow_max': content.maxTF,
          'domain_authority_min': content.minDA,
          'domain_authority_max': content.maxDA,
          'category_ids': lodash.map(content.categories, 'id'),
          'agency_content': content.agency_content
        }
      }).then(successCallback, errorCallback);
    }

    function downloadReport(contentId, successCallback, errorCallback) {
      $http.get(CONTENTS_URL + '/' + contentId + '/report').then(successCallback, errorCallback);
    }

    function uploadReport(contentId, file, successCallback, errorCallback) {
      Upload.upload({
        url: CONTENTS_URL + '/' + contentId + '/upload',
        method: 'POST',
        fields: {
          'spreadsheet': file
        }
      }).then(successCallback, errorCallback);
    }
  }
})();
