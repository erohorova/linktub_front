(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('SelectedOrdersService', SelectedOrdersService);

  /* @ngInject */
  function SelectedOrdersService(lodash, ORDER_ORIGIN) {

    var orderList = [];
    var service = {
      clean: clean,
      getOrders: getOrders,
      setOrdersCart: toggleCart,
      setOrdersContentDone: toggleContentDone
    };

    return service;

    function clean() {
      orderList = [];
    }

    function getOrders() {
      return orderList;
    }

    function toggleCart(selectedOrders) {
      lodash.map(selectedOrders, function(order) {
        var newOrder = {};
        angular.copy(order, newOrder);
        newOrder.origin = ORDER_ORIGIN.CART;
        orderList.push(newOrder);
      });
    }

    function toggleContentDone(selectedOrders) {
      lodash.map(selectedOrders, function(order) {
        var newOrder = {};
        angular.copy(order, newOrder);
        newOrder.origin = ORDER_ORIGIN.CONTENT;
        orderList.push(newOrder);
      });
    }
  }
})();
