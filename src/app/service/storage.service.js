(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('StorageService', StorageService);

  /** @ngInject */
  function StorageService(localStorageService) {
    var service = {
      set: set,
      get: get,
      remove: remove,
      clear: clear
    };

    return service;

    ////////// Functions //////////

    function set(key, value) {
      return localStorageService.set(key, value);
    }

    function get(key) {
      return localStorageService.get(key);
    }

    function remove(key) {
      return localStorageService.remove(key);
    }

    function clear() {
      return localStorageService.clearAll();
    }
  }
})();
