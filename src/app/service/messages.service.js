(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('MessagesService', MessagesService);

  /** @ngInject */
  function MessagesService($http, EnvironmentConfig, lodash, SessionService) {
    var ADV_MSG_URL = EnvironmentConfig.API_END_POINT + '/advertisers/me/messages';
    var PUB_MSG_URL = EnvironmentConfig.API_END_POINT + '/publishers/me/messages';

    var service = {
      getMessages: getMessages,
      createMessage: createMessage,
      updateState: updateState
    };

    return service;

    // Functions
    function getMessages(currentPage, successCallback, errorCallback) {
      $http.get(getUrl() + '?page=' + currentPage).then(successCallback, errorCallback);
    }

    function createMessage(data, successCallback, errorCallback) {
      $http.post(getUrl(), {
        message: {
          subject: data.subject,
          question_message: data.question_message
        }
      }).then(successCallback, errorCallback);
    }

    function updateState(data, successCallback, errorCallback) {
      $http.put(getUrl() + '/' + data.id + '/read').then(successCallback, errorCallback);
    }

    function getUrl() {
      if (SessionService.isAdvertiser()) {
        return ADV_MSG_URL;
      } else if(SessionService.isPublisher()) {
        return PUB_MSG_URL;
      }
    }
  }
})();
