(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('RejectedReasonsService', RejectedReasonsService);

  /* @ngInject */
  function RejectedReasonsService() {
    var service = {
      getRejectedReasons: getRejectedReasons
    };

    return service;

    function getRejectedReasons() {
      return [{
          title: 'Content Irrelevant',
          selected: false
        },
        {
          title: 'Content to Salesy',
          selected: false
        },
        {
          title: 'Too short',
          selected: false
        },
        {
          title: 'Too long',
          selected: false
        }
      ];
    }
  }
})();
