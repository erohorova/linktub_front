(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('OrdersService', OrdersService);

  /** @ngInject */
  function OrdersService($http, EnvironmentConfig, Upload, lodash) {
    var ADV_ORDERS_URL = EnvironmentConfig.API_END_POINT + '/advertisers/me/orders';
    var PUB_ORDERS_URL = EnvironmentConfig.API_END_POINT + '/publishers/me/orders';
    var orderList = [];

    var service = {
      indexOrders: indexOrders,
      showOrder: showOrder,
      createOrder: createOrder,
      createOrders: createOrders,
      updateOrder: updateOrder,
      deleteOrder: deleteOrder,
      deleteOrders: deleteOrders,
      isOrdered: isOrdered,
      indexCompletedOrders: indexCompletedOrders,
      alreadyOrdered: alreadyOrdered
    };

    return service;

    // Functions
    function indexOrders(successCallback, errorCallback) {
      $http.get(ADV_ORDERS_URL).then(function (response) {
        angular.copy(response.data.orders, orderList);
        successCallback(orderList);
      }, errorCallback);
    }

    function showOrder(id, successCallback, errorCallback) {
      $http.get(ADV_ORDERS_URL + '/' + id).then(successCallback, errorCallback);
    }

    function createOrder(urlId, orderOrigin, successCallback, errorCallback) {
      $http.post(ADV_ORDERS_URL, {
        order: {
          'url_id': urlId,
          'created_from': orderOrigin
        }
      }).then(function (response) {
        orderList.push(response.data);
        successCallback();
      }, errorCallback);
    }

    function createOrders(urlIds, contentId, orderOrigin, successCallback, errorCallback) {
      $http.post(ADV_ORDERS_URL + '/bulk_create', {
        orders: {
          url_ids: urlIds,
          content_id: contentId,
          created_from: orderOrigin
        }
      }).then(successCallback, errorCallback);
    }

    function updateOrder(id, blogPost, successCallback, errorCallback) {
      var body = {
        order: {
          ad_attributes: {
            id: blogPost.id,
            link_id: blogPost.link.id,
            url_id: blogPost.url.id,
            url_price: blogPost.url.shown_price,
            campaign_id: blogPost.campaign.id,
            blog_post_title: blogPost.blog_post_title,
            blog_post_body: blogPost.blog_post_body,
            blog_post_image_1: blogPost.image_1,
            blog_post_image_2: blogPost.image_2,
            blog_post_doc: blogPost.word,
            link_text: blogPost.link_text
          }
        }
      };

      if (!blogPost.link_text) {
        delete body.order.ad_attributes.link_text;
      }
      if (!blogPost.blog_post_body) {
        delete body.order.ad_attributes.blog_post_body;
      }

      Upload.upload({
        url: ADV_ORDERS_URL + '/' + id,
        method: 'PUT',
        fields: body
      }).then(function (response) {
        successCallback(response)
      }, errorCallback);
    }

    function deleteOrder(orderId, successCallback, errorCallback) {
      $http.delete(ADV_ORDERS_URL + '/' + orderId).then(
        function () {
          lodash.remove(orderList, function (order) {
            return order.id == orderId
          });
          successCallback();
        }, errorCallback);
    }

    function deleteOrders(ordersIds, successCallback, errorCallback) {
      $http.delete(ADV_ORDERS_URL + '/destroy_list', {
        params: {
          'orders_ids[]': ordersIds
        }
      }).then(
        function () {
          lodash.pullAll(orderList, ordersIds);
          successCallback();
        }, errorCallback);
    }

    function isOrdered(urlId) {
      return lodash.findIndex(orderList, ['url.id', urlId]) >= 0;
    }

    function indexCompletedOrders(successCallback, errorCallback) {
      $http.get(PUB_ORDERS_URL).then(successCallback, errorCallback);
    }

    function alreadyOrdered(urlId, successCallback, errorCallback) {
      $http.get(ADV_ORDERS_URL + '/' + urlId + '/already_ordered').then(successCallback, errorCallback);
    }
  }
})();
