(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('AuthenticationService', AuthenticationService);

  /** @ngInject */
  function AuthenticationService($http, EnvironmentConfig, SessionService) {
    var AUTHENTICATION_URL = EnvironmentConfig.API_END_POINT + '/users';

    var service = {
      login: login,
      logout: logout
    };

    return service;

    ////////// Functions //////////

    function login(credentials, successCallback, errorCallback) {
      $http.post(AUTHENTICATION_URL + '/sign_in', {
        user: credentials
      }).then(function (response) {
        SessionService.createSession(response.data);
        successCallback();
      }, errorCallback);
    }

    function logout(successCallback) {
      $http.delete(AUTHENTICATION_URL + '/sign_out').then(function () {
        SessionService.clearSession();
        successCallback();
      });
    }
  }
})();
