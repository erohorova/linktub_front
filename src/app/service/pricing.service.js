(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('PricingService', PricingService);

  /* @ngInject */
  function PricingService(lodash) {
    var service = {
      getFbTwPiPricing: getFbTwPiPricing,
      getPricingYt: getPricingYt,
      getPricingIn: getPricingIn,
      getGaPricing: getGaPricing,
      getPrice: getPrice
    };

    return service;

    function isRowPricingFbTwPi(network) {
      return lodash.includes(['facebook', 'twitter', 'pinterest'], network);
    }

    function isRowPricingYt(network) {
      return 'youtube' == network;
    }

    function isRowPricingIn(network) {
      return 'instagram' == network;
    }

    function between(x, min, max) {
      return x >= min && x < max;
    }

    function getPrice(followers, network) {
      var redux = Math.floor(followers / 1000);
      var element;
      if (isRowPricingFbTwPi(network)) {
        element = lodash.find(getFbTwPiPricing(), function (elem) {
          return between(redux, elem.range.min, elem.range.max);
        });
      } else if (isRowPricingYt(network)) {
        element = lodash.find(getPricingYt(), function (elem) {
          return between(redux, elem.range.min, elem.range.max);
        });
      } else if (isRowPricingIn(network)) {
        element = lodash.find(getPricingIn(), function (elem) {
          return between(redux, elem.range.min, elem.range.max);
        });
      } else {
        element = lodash.find(getGaPricing(), function (elem) {
          return between(redux, elem.range.min, elem.range.max);
        });
      }

      return element.value;
    }

    function getGaPricing() {
      return [{
        range: {
          min: 3,
          max: 5
        },
        index: '3K-5K',
        value: '< $25'
      }, {
        range: {
          min: 5,
          max: 10
        },
        index: '5K-10K',
        value: '$25 - $50'
      }, {
        range: {
          min: 10,
          max: 15
        },
        index: '10K-15K',
        value: '$50 - $75'
      }, {
        range: {
          min: 15,
          max: 20
        },
        index: '15K-20K',
        value: '$75 - $100'
      }, {
        range: {
          min: 20,
          max: 30
        },
        index: '20K-30K',
        value: '$100 - $125'
      }, {
        range: {
          min: 30,
          max: 40
        },
        index: '30K-40K',
        value: '$125 - $150'
      }, {
        range: {
          min: 40,
          max: 50
        },
        index: '40K-50K',
        value: '$150 - $200'
      }, {
        range: {
          min: 50,
          max: 75
        },
        index: '50K-75K',
        value: '$200 - $300'
      }, {
        range: {
          min: 75,
          max: 100
        },
        index: '75K-100K',
        value: '$300 - $400'
      }, {
        range: {
          min: 100,
          max: 150
        },
        index: '100K-150K',
        value: '$400 - $500'
      }, {
        range: {
          min: 150,
          max: 200
        },
        index: '150K-200K',
        value: '$500 - $600'
      }, {
        range: {
          min: 200,
          max: 250
        },
        index: '200K-250K',
        value: '$700 - $800'
      }, {
        range: {
          min: 250,
          max: 350
        },
        index: '250K-350K',
        value: '$800 - $1K'
      }, {
        range: {
          min: 250,
          max: 500
        },
        index: '350K-500K',
        value: '$1K - $1.5K'
      }, {
        range: {
          min: 500,
          max: 100000
        },
        index: '500K +',
        value: '$1.5K +'
      }];
    }

    function getFbTwPiPricing() {
      return [{
        range: {
          min: 1,
          max: 5
        },
        index: '1K-5K',
        value: '< $10'
      }, {
        range: {
          min: 5,
          max: 10
        },
        index: '5K-10K',
        value: '$10 - $15'
      }, {
        range: {
          min: 10,
          max: 15
        },
        index: '10K-15K',
        value: '$15 - $20'
      }, {
        range: {
          min: 15,
          max: 20
        },
        index: '15K-20K',
        value: '$20 - $25'
      }, {
        range: {
          min: 20,
          max: 30
        },
        index: '20K-30K',
        value: '$25 - $30'
      }, {
        range: {
          min: 30,
          max: 40
        },
        index: '30K-40K',
        value: '$30 - $35'
      }, {
        range: {
          min: 40,
          max: 50
        },
        index: '40K-50K',
        value: '$35 - $40'
      }, {
        range: {
          min: 50,
          max: 75
        },
        index: '50K-75K',
        value: '$40 - $45'
      }, {
        range: {
          min: 75,
          max: 100
        },
        index: '75K-100K',
        value: '$45 - $50'
      }, {
        range: {
          min: 100,
          max: 150
        },
        index: '100K-150K',
        value: '$50 - $75'
      }, {
        range: {
          min: 150,
          max: 200
        },
        index: '150K-200K',
        value: '$75 - $100'
      }, {
        range: {
          min: 200,
          max: 250
        },
        index: '200K-250K',
        value: '$100 - $125'
      }, {
        range: {
          min: 250,
          max: 500
        },
        index: '250K-500K',
        value: '$125 - $200'
      }, {
        range: {
          min: 500,
          max: 100000
        },
        index: '500K +',
        value: '$200 +'
      }];
    }

    function getPricingYt() {
      return [{
        range: {
          min: 0,
          max: 2
        },
        index: '1000+',
        value: '$20'
      }, {
        range: {
          min: 2,
          max: 5
        },
        index: '2K-5K',
        value: '$20 - $40'
      }, {
        range: {
          min: 5,
          max: 10
        },
        index: '5K-10K',
        value: '$40 - $75'
      }, {
        range: {
          min: 10,
          max: 15
        },
        index: '10K-15K',
        value: '$75 - $100'
      }, {
        range: {
          min: 15,
          max: 20
        },
        index: '15K-20K',
        value: '$100 - $150'
      }, {
        range: {
          min: 20,
          max: 30
        },
        index: '20K-30K',
        value: '$150 - $200'
      }, {
        range: {
          min: 30,
          max: 40
        },
        index: '30K-40K',
        value: '$200 - $250'
      }, {
        range: {
          min: 40,
          max: 50
        },
        index: '40K-50K',
        value: '$250 - $300'
      }, {
        range: {
          min: 50,
          max: 75
        },
        index: '50K-75K',
        value: '$300 - $350'
      }, {
        range: {
          min: 75,
          max: 100
        },
        index: '75K-100K',
        value: '$350 - $400'
      }, {
        range: {
          min: 100,
          max: 150
        },
        index: '100K-150K',
        value: '$400 - $500'
      }, {
        range: {
          min: 150,
          max: 200
        },
        index: '150K-200K',
        value: '$500 - $600'
      }, {
        range: {
          min: 200,
          max: 250
        },
        index: '200K-250K',
        value: '$600 - $750'
      }, {
        range: {
          min: 250,
          max: 500
        },
        index: '250K-500K',
        value: '$750 - $1K'
      }, {
        range: {
          min: 500,
          max: 100000
        },
        index: '500K +',
        value: '$1K +'
      }];
    }

    function getPricingIn() {
      return [{
        range: {
          min: 0,
          max: 2
        },
        index: '1000+',
        value: '$25 - $50'
      }, {
        range: {
          min: 2,
          max: 5
        },
        index: '2K-5K',
        value: '$50 - $75'
      }, {
        range: {
          min: 5,
          max: 10
        },
        index: '5K-10K',
        value: '$75 - $100'
      },{
        range: {
          min: 10,
          max: 15
        },
        index: '10K-15K',
        value: '$100 - $125'
      }, {
        range: {
          min: 15,
          max: 20
        },
        index: '15K-20K',
        value: '$125 - $150'
      }, {
        range: {
          min: 20,
          max: 30
        },
        index: '20K-30K',
        value: '$150 - $175'
      }, {
        range: {
          min: 30,
          max: 40
        },
        index: '30K-40K',
        value: '$175 - $200'
      }, {
        range: {
          min: 40,
          max: 50
        },
        index: '40K-50K',
        value: '$225 - $250'
      }, {
        range: {
          min: 50,
          max: 75
        },
        index: '50K-75K',
        value: '$250 - $300'
      }, {
        range: {
          min: 75,
          max: 100
        },
        index: '75K-100K',
        value: '$300 - $350'
      }, {
        range: {
          min: 100,
          max: 150
        },
        index: '100K-150K',
        value: '$350 - $400'
      }, {
        range: {
          min: 150,
          max: 200
        },
        index: '150K-200K',
        value: '$400 - $450'
      }, {
        range: {
          min: 200,
          max: 250
        },
        index: '200K-250K',
        value: '$450 - $500'
      }, {
        range: {
          min: 250,
          max: 350
        },
        index: '250K-350K',
        value: '$500 - $550'
      }, {
        range: {
          min: 350,
          max: 500
        },
        index: '350K-500K',
        value: '$550 - $600'
      }, {
        range: {
          min: 500,
          max: 100000
        },
        index: '500K +',
        value: '$750'
      }];
    }
  }
})();
