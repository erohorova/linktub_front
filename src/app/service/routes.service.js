(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('RoutesService', RoutesService);

  /* @ngInject */
  function RoutesService($state, SessionService) {
    var service = {
      dashboard: dashboard,
      profile: profile,
      creditCards: creditCards,
      editProfile: editProfile,
      pubNewOrders: pubNewOrders,
      pubActiveAds: pubActiveAds,
      pubPricingPausing: pubPricingPausing,
      pubResubmitted: pubResubmitted,
      mainQuotes: mainQuotes,
      signUp: signUp,
      signIn: signIn,
      editCampaign: editCampaign,
      allCampaigns: allCampaigns
    };

    return service;

    ////////// Functions //////////

    function dashboard() {
      if (SessionService.isAdvertiser()) {
        $state.go('adv-dash');
      } else if (SessionService.isPublisher()) {
        $state.go('pub-dash');
      }
    }

    function profile() {
      $state.go('show-profile');
    }

    function creditCards(id) {
      id ? $state.go('credit-card', {
        cardId: id
      }) : $state.go('credit-card');
    }

    function editProfile(id) {
      $state.go('edit-profile', {
        profileId: id
      });
    }

    function pubNewOrders() {
      $state.go('new-orders');
    }

    function pubActiveAds() {
      $state.go('active-ads');
    }

    function pubPricingPausing() {
      $state.go('pricing-pausing');
    }

    function pubResubmitted() {
      $state.go('resubmitted');
    }

    function mainQuotes() {
      $state.go('quotes-main');
    }

    function signUp() {
      $state.go('sign-up');
    }

    function signIn() {
      $state.go('sign-in');
    }

    function editCampaign(id) {
      $state.go('edit-campaign', {
        campaignId: id
      })
    }

    function allCampaigns() {
      $state.go('all-campaigns');
    }
  }
})();
