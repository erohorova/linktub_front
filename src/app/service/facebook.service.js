(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('FacebookService', FacebookService);

  /* @ngInject */
  function FacebookService() {
    var LOGIN_SCOPES = 'manage_pages,instagram_basic';
    var PAGE_FIELDS = 'fan_count,username,instagram_business_account';
    var INSTAGRAM_FIELDS = 'followers_count,username';

    var userId;
    var userAccouts = [];
    var fanPageId;
    var instagramBusinessAccountId;

    var service = {
      authenticate: authenticate,
      getUserAccounts: getUserAccounts,
      getFanPageData: getFanPageData,
      checkForInstagramBusinessAccount: checkForInstagramBusinessAccount,
      getInstagramAccountData: getInstagramAccountData,
      userHasAccounts: userHasAccounts
    };

    return service;

    function authenticate(successCallback, errorCallback) {
      if (!userId) {
        FB.getLoginStatus(function (response) {
          if (response.status !== 'connected') {
            FB.login(function (response) {
              if (response.authResponse) {
                userId = response.authResponse.userID;
                getUserAccounts(successCallback, errorCallback);
              } else {
                errorCallback('Facebook login failed');
              }
            }, {
              scope: LOGIN_SCOPES,
              return_scopes: true
            });
          } else {
            userId = response.authResponse.userID;
            getUserAccounts(successCallback, errorCallback);
          }
        });
      } else {
        getUserAccounts(successCallback, errorCallback);
      }
    }

    function getUserAccounts(successCallback, errorCallback) {
      FB.api('/' + userId + '/accounts', 'GET', function (response) {
        if (response.data.length) {
          userAccouts = response.data;
          fanPageId = userAccouts[0].id;
          successCallback(userId);
        } else {
          errorCallback('No pages found for user');
        }
      });
    }

    function getFanPageData(successCallback, errorCallback) {
      if (!userHasAccounts()) {
        errorCallback('No pages found for user');
      }

      FB.api('/' + fanPageId, {
        fields: PAGE_FIELDS
      }, function (response) {
        if (response.instagram_business_account) {
          instagramBusinessAccountId = response.instagram_business_account.id;
        }
        successCallback(response);
      });
    }

    function checkForInstagramBusinessAccount(successCallback, errorCallback) {
      if (!userHasAccounts()) {
        errorCallback('No pages found for user');
      }

      if(!instagramBusinessAccountId) {
        getFanPageData(function (response) {
          if(instagramBusinessAccountId) {
            getInstagramAccountData(successCallback, errorCallback);
          } else {
            errorCallback('No Instagram business account found');
          }
        }, function (error) {
          errorCallback(error);
        });
      } else {
        getInstagramAccountData(successCallback, errorCallback);
      }
    }

    function getInstagramAccountData(successCallback, errorCallback) {
      FB.api('/' + instagramBusinessAccountId, {
        fields: INSTAGRAM_FIELDS
      }, function (response) {
        if (response) {
          successCallback(response);
        } else {
          errorCallback('Error while retrieving Instagram account information');
        }
      });
    }

    function userHasAccounts() {
      return userAccouts.length > 0;
    }
  }
})();
