(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('CategoriesService', CategoriesService);

  /** @ngInject */
  function CategoriesService($http, EnvironmentConfig) {
    var CATEGORIES_URL = EnvironmentConfig.API_END_POINT + '/categories';

    var service = {
      indexCategories: indexCategories
    };

    return service;

    // Functions
    function indexCategories(successCallback, errorCallback) {
      $http.get(CATEGORIES_URL).then(successCallback, errorCallback);
    }
  }
})();
