(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('PaymentsService', PaymentsService);

  /* @ngInject */
  function PaymentsService($http, EnvironmentConfig) {
    var PAYMENTS_URL = EnvironmentConfig.API_END_POINT + '/advertisers/me/payments';

    var service = {
      getPaymentToken: getPaymentToken,
      payment: payment
    };

    return service;

    function getPaymentToken(successCallback, errorCallback) {
      $http.get(PAYMENTS_URL + '/token').then(successCallback, errorCallback);
    }

    function payment(paymentNonce, token, ordersIds, lastFour, storePaymentMethod, successCallback, errorCallback) {
      $http.post(PAYMENTS_URL + '/pay', {
        orders_ids: ordersIds,
        payment_nonce: paymentNonce,
        payment_token: token,
        last_four: lastFour,
        store_payment_method: storePaymentMethod
      }).then(successCallback, errorCallback);
    }
  }
})();
