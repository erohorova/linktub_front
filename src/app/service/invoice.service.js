(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('InvoiceService', InvoiceService);

  /* @ngInject */
  function InvoiceService($http, EnvironmentConfig) {
    var INVOICE_URL = EnvironmentConfig.API_END_POINT + '/advertisers/me/invoices';

    var service = {
      indexInvoice: indexInvoice
    };

    return service;

    function indexInvoice(query, successCallback, errorCallback) {
      $http.get(INVOICE_URL, {
        params: {
          page: query.page,
          limit: query.limit
        }
      }).then(successCallback, errorCallback);
    }
  }
})();
