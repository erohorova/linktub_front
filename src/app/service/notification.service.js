(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('NotificationService', NotificationService);

  /** @ngInject */
  function NotificationService($http, $state, $rootScope, $timeout, EnvironmentConfig, lodash,
    POLL_TIME, TYPE_STATE, ROLE) {
    var ADV_NOTIFICATION_URL = EnvironmentConfig.API_END_POINT + '/advertisers/me/notifications';
    var PUB_NOTIFICATION_URL = EnvironmentConfig.API_END_POINT + '/publishers/me/notifications';

    var service = {
      deleteNotification: deleteNotification
    };

    return service;

    function deleteNotification(type, role) {
      var URL = role === ROLE.ADV ? ADV_NOTIFICATION_URL : PUB_NOTIFICATION_URL;
      $http.delete(URL + '/destroy_type?type=' + type).then(function () {
        switch (type) {
        case TYPE_STATE.ACTIVE_CONTENT:
          $rootScope.badgeActive = 0;
          break;
        case TYPE_STATE.AWAITING_PUBLICATION:
          $rootScope.badgeAwaiting = 0;
          break;
        case TYPE_STATE.APPROVED_URL:
          $rootScope.badgeApproved = 0;
          break;
        case TYPE_STATE.REJECTED_CONTENT:
          $rootScope.badgeReused = 0;
          break;
        case TYPE_STATE.ACTIVE_ADS:
          $rootScope.badgeActiveAds = 0;
          break;
        case TYPE_STATE.NEW_ORDERS:
          $rootScope.badgeNewOrders = 0;
          break;
        case TYPE_STATE.PRICING_AND_PAUSING:
          $rootScope.badgePricingAndPausing = 0;
          break;
        case TYPE_STATE.RESUBMITTED_CONTENT:
          $rootScope.badgeReSubmitted = 0;
          break
        case TYPE_STATE.QUOTES:
          $rootScope.counterQuotes = 0;
          break;
        case TYPE_STATE.ARTICLES:
          $rootScope.counterArticles = 0;
          break;
        case TYPE_STATE.MESSAGES:
          $rootScope.counterMessages = 0;
        }
      });
    }
  }
})();
