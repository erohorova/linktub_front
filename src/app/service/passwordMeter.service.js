(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('PasswordMeter', PasswordMeter);

  /* @ngInject */
  function PasswordMeter() {

    var service = {
      evaluatePassword: evaluatePassword
    };

    return service;

    function evaluatePassword(password) {
      return zxcvbn(password).score;
    }
  }
})();
