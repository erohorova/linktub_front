(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('ArticlesService', ArticlesService);

  /** @ngInject */
  function ArticlesService($http, EnvironmentConfig, Upload, lodash) {
    var ARTICLES_URL = EnvironmentConfig.API_END_POINT + '/advertisers/me/articles';

    var service = {
      indexArticles: indexArticles,
      createArticles: createArticles,
      updateArticle: updateArticle,
      createNote: createNote,
      createOrders: createOrders
    };

    return service;

    // Functions
    function indexArticles(successCallback, errorCallback) {
      $http.get(ARTICLES_URL).then(successCallback, errorCallback);
    }

    function createArticles(articles, successCallback, errorCallback) {
      $http.post(ARTICLES_URL + '/bulk_create', {
        articles: sanitizeArticles(articles)
      }).then(successCallback, errorCallback);
    }

    function sanitizeArticles(articles) {
      var santizedArticles = [];
      santizedArticles = lodash.map(articles, function (article) {
        return {
          ad_attributes: {
            campaign_id: article.campaign,
            blog_post_title: article.quote.blog_post_title,
            link_text: article.quote.link_text,
            url_id: article.url_id
          },
          href: article.quote.href,
          notes: article.quote.notes,
          url_id: article.url_id,
          content_id: article.content_id
        }
      });
      return santizedArticles;
    }

    function updateArticle(id, article, successCallback, errorCallback) {
      Upload.upload({
        url: ARTICLES_URL + '/' + id,
        method: 'PUT',
        fields: {
          'article[ad_attributes][id]': article.ad_id,
          'article[ad_attributes][blog_post_title]': article.title,
          'article[ad_attributes][link_text]': article.anchor,
          'article[full_pub_url]': article.destination_url,
          'article[ad_attributes][campaign_id]': article.campaign_id,
          'article[ad_attributes][blog_post_doc]': article.blog_post_doc
        }
      }).then(successCallback, errorCallback);
    }

    function createNote(id, note, addedNote, successCallback, errorCallback) {
      $http.put(ARTICLES_URL + '/' + id, {
        article: {
          notes: note
        },
        rewrite: addedNote
      }).then(successCallback, errorCallback);
    }

    function createOrders(articleIds, ordersOrigin, successCallback, errorCallback) {
      $http.post(ARTICLES_URL + '/create_orders', {
        articles_ids: articleIds,
        created_from: ordersOrigin
      }).then(successCallback, errorCallback);
    }
  }
})();
