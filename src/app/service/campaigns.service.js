(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('CampaignsService', CampaignsService);

  /** @ngInject */
  function CampaignsService($http, EnvironmentConfig, Upload, lodash) {
    var CAMPAIGNS_URL = EnvironmentConfig.API_END_POINT + '/advertisers/me/campaigns';

    var service = {
      indexCampaigns: indexCampaigns,
      indexScopedCampaigns: indexScopedCampaigns,
      showCampaign: showCampaign,
      getCampaignNames: getCampaignNames,
      createCampaign: createCampaign,
      updateCampaign: updateCampaign,
      deleteCampaign: deleteCampaign,
      updateSpreadsheetCampaign: updateSpreadsheetCampaign,
      updateCampaignCategories: updateCampaignCategories,
      addUrl: addUrl,
      indexLinks: indexLinks,
      updateCampaignHeader: updateCampaignHeader
    };

    return service;

    // Functions
    function indexCampaigns(successCallback, errorCallback) {
      $http.get(CAMPAIGNS_URL).then(successCallback, errorCallback);
    }

    function indexScopedCampaigns(scope, successCallback, errorCallback) {
      $http.get(CAMPAIGNS_URL, {
        params: {
          scope: scope
        }
      }).then(successCallback, errorCallback);
    }

    function getCampaignNames(successCallback, errorCallback) {
      $http.get(CAMPAIGNS_URL + '/names').then(successCallback, errorCallback);
    }

    function showCampaign(id, page, successCallback, errorCallback) {
      $http.get(CAMPAIGNS_URL + '/' + id + '?page=' + page).then(successCallback, errorCallback);
    }

    function createCampaign(campaign, successCallback, errorCallback) {
      Upload.upload({
        url: CAMPAIGNS_URL,
        method: 'POST',
        fields: {
          'campaign[name]': campaign.name,
          'campaign[image]': campaign.image,
          'campaign[links_attributes]': [{
            'href': campaign.url
          }],
          'spreadsheet': campaign.csv,
          'categories_ids': lodash.map(campaign.categories, 'id')
        }
      }).then(successCallback, errorCallback);
    }

    function updateCampaign(id, campaign, successCallback, errorCallback) {
      Upload.upload({
        url: CAMPAIGNS_URL + '/' + id,
        method: 'PUT',
        fields: {
          'campaign[name]': campaign.name,
          'campaign[image]': campaign.image,
          'campaign[links_attributes]': [{
            id: campaign.url.id,
            href: campaign.url
          }],
          'categories_ids': lodash.map(campaign.categories, 'id')
        }
      }).then(successCallback, errorCallback);
    }

    function updateCampaignCategories(id, campaign, successCallback, errorCallback) {
      Upload.upload({
        url: CAMPAIGNS_URL + '/' + id,
        method: 'PUT',
        fields: {
          'campaign[categories_ids]': campaign.categories_ids,
        }
      }).then(successCallback, errorCallback);
    }

    function deleteCampaign(id, successCallback, errorCallback) {
      $http.delete(CAMPAIGNS_URL + '/' + id).then(successCallback, errorCallback);
    }

    function updateSpreadsheetCampaign(id, spreadsheet, successCallback, errorCallback) {
      Upload.upload({
        url: CAMPAIGNS_URL + '/' + id + '/update_spreadsheet',
        method: 'PUT',
        fields: {
          'spreadsheet': spreadsheet
        }
      }).then(successCallback, errorCallback);
    }

    function addUrl(id, url, successCallback, errorCallback) {
      Upload.upload({
        url: CAMPAIGNS_URL + '/' + id + '/links',
        method: 'POST',
        fields: {
          'link[href]': url.href
        }
      }).then(successCallback, errorCallback);
    }

    function indexLinks(id, successCallback, errorCallback) {
      $http.get(CAMPAIGNS_URL + '/' + id).then(successCallback, errorCallback);
    }

    function updateCampaignHeader(id, campaign, successCallback, errorCallback) {
      Upload.upload({
        url: CAMPAIGNS_URL + '/' + id,
        method: 'PUT',
        fields: {
          'campaign[name]': campaign.name,
          'campaign[image]': campaign.image,
          'categories_ids': lodash.map(campaign.categories, 'id')
        }
      }).then(successCallback, errorCallback);
    }
  }
})();
