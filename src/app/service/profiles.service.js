(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('ProfilesService', ProfilesService);

  /** @ngInject */
  function ProfilesService($http, EnvironmentConfig, Upload, lodash) {
    var PROFILES_URL = EnvironmentConfig.API_END_POINT + '/publishers/me/profiles';

    var service = {
      createProfile: createProfile,
      updateProfile: updateProfile,
      indexProfiles: indexProfiles,
      showProfile: showProfile,
      getMetaDescription: getMetaDescription,
      getAnalyticsMUV: getAnalyticsMUV,
      getSocialHandleFollowers: getSocialHandleFollowers,
      updateSpreadsheetProfile: updateSpreadsheetProfile,
      getDomainInformation: getDomainInformation,
      getProfileNames: getProfileNames,
      indexUrls: indexUrls,
      addUrl: addUrl,
      updateUrl: updateUrl,
      getTwitterFollowersCount: getTwitterFollowersCount
    };

    return service;

    // Functions
    function indexProfiles(successCallback, errorCallback) {
      $http.get(PROFILES_URL).then(successCallback, errorCallback);
    }

    function indexUrls(id, successCallback, errorCallback) {
      $http.get(PROFILES_URL + '/' + id).then(successCallback, errorCallback);
    }

    function showProfile(id, successCallback, errorCallback) {
      $http.get(PROFILES_URL + '/' + id).then(successCallback, errorCallback);
    }

    function createProfile(profile, successCallback, errorCallback) {
      Upload.upload({
        url: PROFILES_URL,
        method: 'POST',
        fields: {
          'profile[name]': profile.name,
          'profile[image]': profile.image,
          'profile[profile_type]': profile.profileType,
          'profile[content_type]': profile.contentType,
          'profile[categories_ids]': profile.categories_ids,
          'profile[urls_attributes]': profile.urls,
          'profile[web_content_attributes]': profile.web_content,
          'profile[facebook_handles_attributes]': profile.facebook_handles,
          'profile[twitter_handles_attributes]': profile.twitter_handles,
          'profile[youtube_handles_attributes]': profile.youtube_handles,
          'profile[instagram_handles_attributes]': profile.instagram_handles,
          'profile[pinterest_handles_attributes]': profile.pinterest_handles
        }
      }).then(successCallback, errorCallback);
    }

    function updateProfile(id, profile, successCallback, errorCallback) {
      Upload.upload({
        url: PROFILES_URL + '/' + id,
        method: 'PUT',
        fields: {
          'profile[name]': profile.name,
          'profile[image]': profile.image,
          'profile[profile_type]': profile.profile_type,
          'profile[categories_ids]': profile.categories_ids,
          'profile[urls_attributes]': profile.urls,
          'profile[web_content_attributes]': profile.web_content,
          'profile[facebook_handles_attributes]': profile.facebook_handles,
          'profile[twitter_handles_attributes]': profile.twitter_handles,
          'profile[youtube_handles_attributes]': profile.youtube_handles,
          'profile[instagram_handles_attributes]': profile.instagram_handles,
          'profile[pinterest_handles_attributes]': profile.pinterest_handles,
          'spreadsheet': profile.csv
        }
      }).then(successCallback, errorCallback);
    }

    function updateSpreadsheetProfile(id, profileType, spreadsheet, successCallback, errorCallback) {
      Upload.upload({
        url: PROFILES_URL + '/' + id + '/update_spreadsheet',
        method: 'PUT',
        fields: {
          'spreadsheet': spreadsheet,
          'profile_type': profileType
        }
      }).then(successCallback, errorCallback);
    }

    function getMetaDescription(url, successCallback, errorCallback) {
      var META_URL = '/meta_description?url=';
      $http.get(PROFILES_URL + META_URL + url).then(successCallback, errorCallback);
    }

    function getAnalyticsMUV(url, successCallback, errorCallback) {
      var GA_URL = '/ga_muv?url=';
      $http.get(PROFILES_URL + GA_URL + url).then(successCallback, errorCallback);
    }

    function getSocialHandleFollowers(url, name, successCallback, errorCallback) {
      var URL = '/' + name + '_followers?url=';
      $http.get(PROFILES_URL + URL + url).then(successCallback, errorCallback);
    }

    function getDomainInformation(url, successCallback, errorCallback) {
      var DOMAIN_URL = '/domain_information?url=';
      $http.get(PROFILES_URL + DOMAIN_URL + url).then(successCallback, errorCallback);
    }

    function getProfileNames(successCallback) {
      $http.get(PROFILES_URL + '/names').then(successCallback);
    }

    function addUrl(id, url, successCallback, errorCallback) {
      Upload.upload({
        url: PROFILES_URL + '/' + id + '/urls',
        method: 'POST',
        fields: {
          'url[href]': url.href,
          'url[price]': url.price,
          'url[description]': url.description,
          'url[author_page]': url.author_page,
          'url[follow_link]': url.follow_link
        }
      }).then(successCallback, errorCallback);
    }

    function updateUrl(id, url, successCallback, errorCallback) {
      Upload.upload({
        url: PROFILES_URL + '/' + id + '/urls/' + url.id,
        method: 'PUT',
        fields: {
          'url[href]': url.href,
          'url[price]': url.price,
          'url[description]': url.description,
          'url[author_page]': url.author_page,
          'url[follow_link]': url.follow_link,
          'url[pause]': url.pause
        }
      }).then(successCallback, errorCallback);
    }

    function getTwitterFollowersCount(handle, successCallback, errorCallback) {
      $http.get(PROFILES_URL + '/twitter_followers', {
        params: {
          'handle': handle
        }
      }).then(successCallback, errorCallback);
    }
  }
})();
