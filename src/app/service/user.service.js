(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('UserService', UserService);

  /** @ngInject */
  function UserService($http, EnvironmentConfig) {
    var USER_URL = EnvironmentConfig.API_END_POINT + '/users/me';
    var USERS_URL = EnvironmentConfig.API_END_POINT + '/users';

    var service = {
      getUser: getUser,
      updateUser: update,
      signUp: signUp,
      checkDuplicatedEmail: checkDuplicatedEmail
    };

    return service;

    // Functions
    function getUser(successCallback, errorCallback) {
      $http.get(USER_URL).then(successCallback, errorCallback);
    }

    function update(user, successCallback, errorCallback) {
      $http.put(USER_URL, {
        user: user
      }).then(successCallback, errorCallback);
    }

    function signUp(user, advertiser, publisher, successCallback, errorCallback) {
      $http.post(USERS_URL, {
        user: user,
        advertiser: advertiser,
        publisher: publisher
      }).then(successCallback, errorCallback);
    }

    function checkDuplicatedEmail(email, successCallback, errorCallback) {
      $http.get(USERS_URL + '/check_email', {
        params: {
          email: email
        }
      }).then(successCallback, errorCallback);
    }
  }
})();
