(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('SearchService', SearchService);

  /** @ngInject */
  function SearchService($http, EnvironmentConfig) {
    var SEARCH_URL = EnvironmentConfig.API_END_POINT + '/urls';

    var service = {
      indexUrls: indexUrls
    };

    return service;

    // Functions
    function indexUrls(values, categories_names, categories_keywords, successCallback, errorCallback) {
      $http.get(SEARCH_URL, {
        params: {
          domain_authority_min: values.domain_authority_start,
          domain_authority_max: values.domain_authority_end,
          trust_flow_min: values.trust_flow_start,
          trust_flow_max: values.trust_flow_end,
          citation_flow_min: values.citation_flow_start,
          citation_flow_max: values.citation_flow_end,
          shown_price_min: values.price_start,
          shown_price_max: values.price_end,
          'words[]': categories_names && categories_names.concat(values.text) || [],
          'keywords[]': categories_keywords && categories_keywords.concat(values.text) || [],
          auto_approved: values.auto_approved,
          sort_by: values.sort_by,
          page: values.page
        }
      }).then(successCallback, errorCallback);
    }
  }
})();
