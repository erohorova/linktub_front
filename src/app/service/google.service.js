(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('GoogleService', GoogleService);

  /* @ngInject */
  function GoogleService(EnvironmentConfig, SCOPE) {
    var service = {
      authorize: authorize,
      queryAccounts: queryAccounts,
      queryProperties: queryProperties,
      queryProfiles: queryProfiles,
      queryCoreReportingApi: queryCoreReportingApi,
      queryYouTubeStatistics: queryYouTubeStatistics
    };

    return service;

    function authorize(event, successCallback) {
      // Handles the authorization flow. `immediate` should be false when invoked from the button click.
      var useImmdiate = !event;
      var authData = {
        client_id: EnvironmentConfig.CLIENT_ID,
        scope: SCOPE.ANALYTICS + ' ' + SCOPE.YOUTUBE,
        immediate: useImmdiate
      };

      gapi.auth.authorize(authData, successCallback);
    }

    function queryAccounts(successCallback, errorCallback) {
      gapi.client.load('analytics', 'v3').then(function () {
        // Get a list of all Google Analytics accounts for this user
        gapi.client.analytics.management.accounts.list().then(successCallback, errorCallback);
      });
    }

    function queryProperties(account, successCallback, errorCallback) {
      gapi.client.analytics.management.webproperties.list({
          accountId: account.id
        })
        .then(successCallback)
        .then(null, errorCallback);
    }

    function queryProfiles(property, successCallback, errorCallback) {
      property = JSON.parse(property);
      gapi.client.analytics.management.profiles.list({
          accountId: property.accountId,
          webPropertyId: property.id
        })
        .then(successCallback)
        .then(null, errorCallback);
    }

    function queryCoreReportingApi(profileId, successCallback, errorCallback) {
      gapi.client.analytics.data.ga.get({
          'ids': 'ga:' + profileId,
          'start-date': '30daysAgo',
          'end-date': 'today',
          'metrics': 'ga:users'
        })
        .then(successCallback)
        .then(null, errorCallback)
    }

    function queryYouTubeStatistics(successCallback) {
      gapi.client.request({
        'method': 'GET',
        'path': '/youtube/v3/channels',
        'params': {'part': 'statistics', 'mine': 'true'}
      }).then(successCallback);
    }
  }
})();
