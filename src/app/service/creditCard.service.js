(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('CreditCardService', CreditCardService);

  /* @ngInject */
  function CreditCardService($http, EnvironmentConfig) {
    var CREDIT_CARDS_URL = EnvironmentConfig.API_END_POINT + '/advertisers/me/credit_cards';

    var service = {
      indexCreditCards: indexCreditCards,
      createCreditCard: createCreditCard,
      updateCreditCard: updateCreditCard,
      deleteCreditCard: deleteCreditCard,
      getCreditCard: getCreditCard
    };

    return service;

    function indexCreditCards(successCallback, errorCallback) {
      $http.get(CREDIT_CARDS_URL).then(successCallback, errorCallback);
    }

    function getCreditCard(cardId, successCallback, errorCallback) {
      $http.get(CREDIT_CARDS_URL + '/' + cardId).then(successCallback, errorCallback);
    }

    function createCreditCard(nonce, successCallback, errorCallback) {
      $http.post(CREDIT_CARDS_URL, {
        credit_card: {
          payment_nonce: nonce,
        }
      }).then(successCallback, errorCallback);
    }

    function updateCreditCard(id, nonce, token, successCallback, errorCallback) {
      $http.put(CREDIT_CARDS_URL + '/' + id, {
        credit_card: {
          payment_nonce: nonce,
          payment_token: token,
        }
      }).then(successCallback, errorCallback);
    }

    function deleteCreditCard(cardId, successCallback, errorCallback) {
      $http.delete(CREDIT_CARDS_URL + '/' + cardId).then(successCallback, errorCallback);
    }
  }
})();
