(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('UrlsService', UrlsService);

  /** @ngInject */
  function UrlsService($http, EnvironmentConfig, Upload, lodash) {
    var URLS = EnvironmentConfig.API_END_POINT + '/publishers/me/urls';

    var service = {
      updateUrl: updateUrl,
      indexOnlyUrls: indexOnlyUrls
    };

    return service;

    // Functions
    function updateUrl(url, successCallback, errorCallback) {
      Upload.upload({
        url: URLS + '/' + url.id,
        method: 'PUT',
        fields: {
          'url[href]': url.href,
          'url[price]': url.price,
          'url[description]': url.description,
          'url[follow_link]': url.follow_link,
          'url[paused]': url.paused
        }
      }).then(successCallback, errorCallback);
    }

    function indexOnlyUrls(states, successCallback, errorCallback) {
      $http.get(URLS, {
        params: {
          'states[]': states
        }
      }).then(successCallback, errorCallback);
    }
  }
})();