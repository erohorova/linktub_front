
(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('BlogPostsService', BlogPostsService);

  /** @ngInject */
  function BlogPostsService($http, EnvironmentConfig, Upload) {
    var BLOGS_URL = EnvironmentConfig.API_END_POINT + '/advertisers/me/ads';
    var PUB_BLOGS_URL = EnvironmentConfig.API_END_POINT + '/publishers/me/ads';

    var service = {
      showBlogPost: showBlogPost,
      indexBlogPosts: indexBlogPosts,
      updateBlogPost: updateBlogPost,
      deleteBlogPost: deleteBlogPost,
      indexPublisherBlogPosts: indexPublisherBlogPosts,
      approveAd: approveAd,
      rejectAd: rejectAd,
      updateLiveUrl: updateLiveUrl
    };

    return service;

    // Functions
    function showBlogPost(id, successCallback, errorCallback) {
      $http.get(BLOGS_URL + '/' + id).then(successCallback, errorCallback);
    }

    function indexBlogPosts(params, successCallback, errorCallback) {
      $http.get(BLOGS_URL, {
        params: params
      }).then(successCallback, errorCallback);
    }

    function updateBlogPost(id, blogPost, successCallback, errorCallback) {
      var body = {
        ad: {
          id: blogPost.id,
          link_id: blogPost.link.id,
          url_id: blogPost.url.id,
          campaign_id: blogPost.campaign.id,
          blog_post_title: blogPost.blog_post_title,
          blog_post_body: blogPost.blog_post_body,
          blog_post_image_1: blogPost.image_1,
          blog_post_image_2: blogPost.image_2,
          blog_post_doc: blogPost.word,
          link_text: blogPost.link_text
        }
      };

      if (!blogPost.link_text) {
        delete body.ad.link_text;
      }
      if (!blogPost.blog_post_body) {
        delete body.ad.blog_post_body;
      }

      Upload.upload({
        url: BLOGS_URL + '/' + id,
        method: 'PUT',
        fields: body
      }).then(successCallback, errorCallback);
    }

    function deleteBlogPost(id, successCallback, errorCallback) {
      $http.delete(BLOGS_URL + '/' + id).then(successCallback, errorCallback);
    }

    function indexPublisherBlogPosts(state, successCallback, errorCallback) {
      $http.get(PUB_BLOGS_URL, {
        params: {
          state: state
        }
      }).then(successCallback, errorCallback);
    }

    function updateLiveUrl(id, live_url, note, successCallback, errorCallback) {
      $http.put(PUB_BLOGS_URL + '/' +id, {
        ad: {
          full_pub_url: live_url,
          notes: note
        }
      }).then(successCallback, errorCallback);
    }

    function approveAd(id, successCallback, errorCallback) {
      $http.put(PUB_BLOGS_URL + '/' + id + '/approve').then(successCallback, errorCallback);
    }

    function rejectAd(id, ad, successCallback, errorCallback) {
      $http.put(PUB_BLOGS_URL + '/' + id + '/reject', {
        ad: ad
      }).then(successCallback, errorCallback);
    }
  }
})();
