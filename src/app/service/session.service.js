(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('SessionService', SessionService);

  /* @ngInject */
  function SessionService(lodash, StorageService, ROLE) {

    ////////// Constants //////////
    const INDEX = 'index';
    const TOKEN = 'token';
    const USER = 'user';
    const ROLES = 'roles';
    const CURRENT_ROLE = 'currentRole';

    var service = {
      sessionToken: sessionToken,
      sessionUser: sessionUser,
      createSession: createSession,
      isloggedIn: isloggedIn,
      clearSession: clearSession,
      currentRole: currentRole,
      isAdvertiser: isAdvertiser,
      isPublisher: isPublisher,
      switchAccount: switchAccount,
      canSwithAccount: canSwithAccount,
      setRole: setRole,
      updateSession: updateSession,
      forceSessionFromAdmin: forceSessionFromAdmin
    };

    return service;

    ////////// Functions //////////

    function sessionToken() {
      return StorageService.get(TOKEN);
    }

    function sessionUser() {
      return StorageService.get(USER);
    }

    function createSession(data) {
      StorageService.set(INDEX, 0);
      StorageService.set(TOKEN, data.token);
      StorageService.set(USER, data.user);
      StorageService.set(ROLES, data.user.roles);
      StorageService.set(CURRENT_ROLE, data.user.roles[StorageService.get(INDEX)]);
    }

    function updateSession(data) {
      var index = StorageService.get(INDEX);
      var total = StorageService.get(ROLES).length;
      var res = (++index % total);
      StorageService.set(INDEX, res);
      StorageService.set(USER, data.user);
      StorageService.set(ROLES, data.user.roles);
      StorageService.set(CURRENT_ROLE, data.user.roles[index]);
    }

    function isloggedIn() {
      return StorageService.get(TOKEN) != null;
    }

    function clearSession() {
      return StorageService.clear();
    }

    function currentRole() {
      return StorageService.get(CURRENT_ROLE);
    }

    function isAdvertiser() {
      return StorageService.get(CURRENT_ROLE) == ROLE.ADV;
    }

    function isPublisher() {
      return StorageService.get(CURRENT_ROLE) == ROLE.PUB;
    }

    function switchAccount() {
      if (canSwithAccount()) {
        var index = StorageService.get(INDEX);
        var total = StorageService.get(ROLES).length;
        var res = (++index % total);
        var newRole = StorageService.get(ROLES)[res];
        StorageService.set(INDEX, res);
        return StorageService.set(CURRENT_ROLE, newRole);
      }
      return false;
    }

    function canSwithAccount() {
      var roles = StorageService.get(ROLES);
      return roles && roles.length > 1;
    }

    function setRole(role) {
      StorageService.set(role, true);
    }

    function forceSessionFromAdmin(token, role) {
      createSession({
        token: token,
        user: {
          roles: [role]
        }
      })
    }
  }
})();
