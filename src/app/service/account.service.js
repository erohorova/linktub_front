(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('AccountService', AccountService);

  /** @ngInject */
  function AccountService($http, EnvironmentConfig) {
    var ADVERTISER_URL = EnvironmentConfig.API_END_POINT + '/advertisers';
    var PUBLISHER_URL = EnvironmentConfig.API_END_POINT + '/publishers';

    var service = {
      getAdvertiser: getAdvertiser,
      getPublisher: getPublisher,
      createAdvertiser: createAdv,
      createPublisher: createPub
    };

    return service;

    // Functions
    function getAdvertiser(successCallback, errorCallback) {
      $http.get(ADVERTISER_URL + '/me').then(successCallback, errorCallback);
    }

    function getPublisher(successCallback, errorCallback) {
      $http.get(PUBLISHER_URL + '/me').then(successCallback, errorCallback);
    }

    function createAdv(successCallback, errorCallback) {
      $http.post(ADVERTISER_URL).then(successCallback, errorCallback);
    }

    function createPub(successCallback, errorCallback) {
      $http.post(PUBLISHER_URL).then(successCallback, errorCallback);
    }
  }
})();
