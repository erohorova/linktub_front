(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('PayoutService', PayoutService);

  /* @ngInject */
  function PayoutService($timeout, $http, EnvironmentConfig) {
    var PAYOUT_URL = EnvironmentConfig.API_END_POINT + '/publishers/me/payouts';

    var service = {
      indexPayoutItems: indexPayoutItems
    };

    return service;

    function indexPayoutItems(date_from, date_to, successCallback, errorCallback) {
      $http.get(PAYOUT_URL, {
        params: {
          date_from: date_from,
          date_to: date_to
        }
      }).then(successCallback, errorCallback);
    }
  }
})();
