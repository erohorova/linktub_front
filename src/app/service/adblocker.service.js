(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('AdBlockerService', AdBlockerService);

  /** @ngInject */
  function AdBlockerService($http) {

    var service = {
      adBlockerEnabled: adBlockerEnabled
    };

    return service;

    // Functions
    function adBlockerEnabled(callback) {
      var testURL = 'https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js'

      var myInit = {
        method: 'HEAD',
        mode: 'no-cors'
      };

      var myRequest = new Request(testURL, myInit);

      fetch(myRequest).then(function(response) {
        return response;
      }).then(function(response) {
        callback(false)
      }).catch(function(e){
        callback(true)
      });
    }
  }
})();
