(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('CounterService', CounterService);

  /** @ngInject */
  function CounterService($http, $state, $rootScope, toastr, $timeout, EnvironmentConfig,
    POLL_TIME, ADV_STATES, PUB_STATES) {
    var ADV_COUNTER_URL = EnvironmentConfig.API_END_POINT + '/advertisers/me/counters';
    var PUB_COUNTER_URL = EnvironmentConfig.API_END_POINT + '/publishers/me/counters';

    var service = {
      poller: poller,
      getAdvCardValues: getAdvCardValues,
      getPubCardValues: getPubCardValues,
    };

    return service;

    //used to poll server to get notifications and counters
    function poller() {
      if (ADV_STATES.indexOf($state.current.name) !== -1) {
        getAdvCardValues();
      } else if (PUB_STATES.indexOf($state.current.name) !== -1) {
        getPubCardValues();
      }
      $timeout(poller, POLL_TIME);
    }

    function getPubCardValues() {
      //used for badges notification counter
      $rootScope.badgeActiveAds = $rootScope.badgeActiveAds || 0;
      $rootScope.badgeNewOrders = $rootScope.badgeNewOrders || 0;
      $rootScope.badgePricingAndPausing = $rootScope.badgePricingAndPausing || 0;
      $rootScope.badgeReSubmitted = $rootScope.badgeReSubmitted || 0;

      //used for card items counter
      $rootScope.counterActiveAds = $rootScope.counterActiveAds || 0;
      $rootScope.counterNewOrders = $rootScope.counterNewOrders || 0;
      $rootScope.counterPricingAndPausing = $rootScope.counterPricingAndPausing || 0;
      $rootScope.counterReSubmitted = $rootScope.counterReSubmitted || 0;
      //used for contact/help
      $rootScope.counterMessages = $rootScope.counterMessages || 0;


      $http.get(PUB_COUNTER_URL, {
        ignoreLoading: true
      }).then(function (response) {
        var data = response.data;
        $rootScope.counterActiveAds = data.active_ads;
        $rootScope.counterNewOrders = data.new_orders;
        $rootScope.counterPricingAndPausing = data.pricing_and_pausing;
        $rootScope.counterReSubmitted = data.resubmitted_content;

        $rootScope.badgeActiveAds = data.active_ads_badge;
        $rootScope.badgeNewOrders = data.new_orders_badge;
        $rootScope.badgePricingAndPausing = data.pricing_and_pausing_badge;
        $rootScope.badgeReSubmitted = data.resubmitted_content_badge;
        $rootScope.counterMessages = data.new_message;
      });
    }

    function getAdvCardValues() {
      //used for badges notification counter
      $rootScope.badgeActive = $rootScope.badgeActive || 0;
      $rootScope.badgeAwaiting = $rootScope.badgeAwaiting || 0;
      $rootScope.badgeApproved = $rootScope.badgeApproved || 0;
      $rootScope.badgeReused = $rootScope.badgeReused || 0;
      //used for card items counter
      $rootScope.counterActive = $rootScope.counterActive || 0;
      $rootScope.counterAwaiting = $rootScope.counterAwaiting || 0;
      $rootScope.counterApproved = $rootScope.counterApproved || 0;
      $rootScope.counterReused = $rootScope.counterReused || 0;
      // used for quotes & articles
      $rootScope.counterQuotes = $rootScope.counterQuotes || 0;
      $rootScope.counterArticles = $rootScope.counterArticles || 0;
      //used for contact/help
      $rootScope.counterMessages = $rootScope.counterMessages || 0;


      $http.get(ADV_COUNTER_URL, {
        ignoreLoading: true
      }).then(function (response) {
        var data = response.data;
        $rootScope.badgeActive = data.active_content_badge;
        $rootScope.badgeAwaiting = data.awaiting_pubs_badge;
        $rootScope.badgeApproved = data.urls_approved_badge;
        $rootScope.badgeReused = data.reuse_content_badge;
        $rootScope.counterActive = data.active_content;
        $rootScope.counterAwaiting = data.awaiting_pubs;
        $rootScope.counterApproved = data.urls_approved;
        $rootScope.counterReused = data.reuse_content;
        $rootScope.counterQuotes = data.new_quotes;
        $rootScope.counterArticles = data.new_articles;
        $rootScope.counterMessages = data.new_message;
      });
    }
  }
})();
