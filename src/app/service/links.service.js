(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('LinksService', LinksService);

  /** @ngInject */
  function LinksService($http, EnvironmentConfig, Upload, lodash) {
    var LINKS_URL = EnvironmentConfig.API_END_POINT + '/advertisers/me/links';

    var service = {
      deleteLink: deleteLink,
      getLinks: getLinks
    };

    return service;

    // Functions
    function deleteLink(id, successCallback, errorCallback) {
      $http.delete(LINKS_URL + '/' + id).then(successCallback, errorCallback);
    }

    function getLinks(id, successCallback, errorCallback) {
      $http.get(LINKS_URL + '/' + id + '/orders_built').then(successCallback, errorCallback);
    }
  }
})();
