(function () {
  'use strict';

  angular
    .module('linktub.pages', [
      'linktub.pages.articles',
      'linktub.pages.campaigns',
      'linktub.pages.cart',
      'linktub.pages.payouts',
      'linktub.pages.dashboard',
      'linktub.pages.landing',
      'linktub.pages.quotes',
      'linktub.pages.search',
      'linktub.pages.reports',
      'linktub.pages.user',
      'linktub.pages.billing',
      'linktub.pages.profiles',
      'linktub.pages.creditCards',
      'linktub.pages.faq',
      'linktub.pages.contactUs'
    ]);
})();
