(function () {
  'use strict';

  angular
    .module('linktub.pages.reports.resubmitted', [])
    .controller('ResubmittedController', ResubmittedController);

  /* @ngInject */
  function ResubmittedController($scope, $mdDialog, toastr, BlogPostsService,
    NotificationService, PUB_URL_STATE, TYPE_STATE, ROLE) {
    var vm = this;

    //Accesible variables
    vm.resubmitted = [];
    vm.filtered = [];
    vm.query = {
      filter: '',
      order: '-url.profile_name',
      limit: 20,
      page: 1
    };

    //Accesible functions
    vm.showNotesIcon = showNotesIcon;
    vm.approve = approve;
    vm.decline = decline;
    vm.showNotes = showNotes;

    initialize();

    function initialize() {
      NotificationService.deleteNotification(TYPE_STATE.RESUBMITTED_CONTENT, ROLE.PUB);
      BlogPostsService.indexPublisherBlogPosts(PUB_URL_STATE.RESUBMITTED, function (response) {
        vm.resubmitted = response.data.ads || [];
        vm.filtered = vm.resubmitted;
      }, function (error) {
        toastr.error('Couldn\'t retrieve ads. Try again please.');
      });
    }

    function showNotesIcon() {
      return 'assets/images/note.svg';
    }

    function showNotes(ev, ad) {
      $mdDialog.show({
        controller: 'ReportNotesController',
        controllerAs: 'noteCtrl',
        templateUrl: 'app/pages/reports/pub/resubmittedContent/partials/notes.html',
        hasBackdrop: true,
        targetEvent: ev,
        clickOutsideToClose: true,
        escapeToClose: true,
        focusOnOpen: true,
        locals: {
          ad: ad
        }
      });
    }

    function downloadContent(ev, ad) {
      if (ad.blog_post_body) {
        blogPostEditDialog(ev, ad, true);
      } else if (ad.word) {
        return ad.word.url;
      }
    }

    function blogPostEditDialog(ev, ad, download) {
      $mdDialog.show({
        controller: 'BlogPostEditController',
        controllerAs: 'blgEditCtrl',
        templateUrl: 'app/pages/reports/adv/rejected/blogPostsEdit/blogPostsEdit.html',
        hasBackdrop: true,
        targetEvent: ev,
        clickOutsideToClose: true,
        escapeToClose: true,
        focusOnOpen: true,
        locals: {
          ad: ad,
          download: download
        }
      });
    }

    function approve(ad) {
      var confirm = $mdDialog.confirm()
        .title('Are you sure?')
        .textContent('')
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function (result) {
        BlogPostsService.approveAd(ad.id, function (response) {
          initialize();
        }, function (error) {
          toastr.error('Couldn\'t approve order. Try again please.');
        });
      });
    }

    function decline(ad) {
      if (ad.rejected_counter >= 1) {
        // Reject the ad again will cancel the ad for ever
        var confirm = $mdDialog.confirm()
          .title('Are You Sure?')
          .textContent('If you reject this piece a 2nd time, you will be permanently cancelling this order.')
          .ok('I Understand')
          .cancel('Cancel');

        $mdDialog.show(confirm).then(function () {
          BlogPostsService.rejectAd(ad.id, ad, function (response) {
            initialize();
          }, function (error) {
            toastr.error('Couldn\'t decline order. Try again please.');
          });
        });
      } else {
        $mdDialog.show({
          templateUrl: 'app/pages/reports/pub/newOrders/partials/rejectOrder.html',
          controller: 'RejectOrderController',
          controllerAs: 'rejCtrl',
          preserveScope: true,
          clickOutsideToClose: false,
          fullscreen: true
        }).then(function (ads) {
          BlogPostsService.rejectAd(ad.id, ad, function (response) {
            initialize();
          }, function (error) {
            toastr.error('Couldn\'t decline order. Try again please.');
          });
        });
      }
    }

    $scope.$watch('resCtrl.query.filter', function (newValue, oldValue) {
      var bookmark = '';

      if (!oldValue) {
        bookmark = vm.query.page;
      } else if (newValue !== oldValue) {
        vm.query.page = 1;
      }

      if (!newValue) {
        vm.query.page = bookmark;
      } else if (newValue === '') {
        vm.query.page = 1;
      }
    });
  }
})();
