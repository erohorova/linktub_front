(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('ReportNotesController', ReportNotesController);

  /* @ngInject */
  function ReportNotesController($scope, $mdDialog, lodash, ad) {
    var vm = this;

    //Accesible variables
    vm.notes = [];
    vm.ad = {};

    //Accesible functions
    vm.ok = ok;

    initialize();

    function initialize() {
      vm.ad = ad;
      vm.notes = ad.notes;
    }

    function ok() {
      $mdDialog.hide();
    }
  }
})();
