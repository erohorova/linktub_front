(function () {
  'use strict';

  angular
    .module('linktub.pages.reports.newOrders', [])
    .controller('NewOrdersController', NewOrdersController);

  /* @ngInject */
  function NewOrdersController($scope, $window, $mdDialog, toastr, BlogPostsService,
    NotificationService, PUB_URL_STATE, ROLE, TYPE_STATE) {
    var vm = this;

    //Accesible variables
    vm.newOrders = [];
    vm.filtered = [];
    vm.query = {
      filter: '',
      order: '-ordered_date',
      limit: 20,
      page: 1
    };

    //Accesible functions
    vm.approve = approve;
    vm.decline = decline;
    vm.downloadContent = downloadContent;
    vm.isRejected = isRejected;

    initialize();

    function initialize() {
      NotificationService.deleteNotification(TYPE_STATE.NEW_ORDERS, ROLE.PUB);
      BlogPostsService.indexPublisherBlogPosts(PUB_URL_STATE.PENDING, function (response) {
        vm.newOrders = response.data.ads || [];
      }, function (error) {
        toastr.error('Couldn\'t retrieve orders. Try again please.');
      });
    }

    function approve(ad) {
      if (!isRejected(ad)) {
        var confirm = $mdDialog.confirm()
          .title('Are you sure?')
          .textContent('')
          .ok('Yes')
          .cancel('No');

        $mdDialog.show(confirm).then(function (result) {
          BlogPostsService.approveAd(ad.id, function (response) {
            refreshOrRedirect();
          }, function (error) {
            toastr.error('Couldn\'t approve order. Try again please.');
          });
        });
      }
    }

    function decline(ad) {
      if (!isRejected(ad)) {
        $mdDialog.show({
          templateUrl: 'app/pages/reports/pub/newOrders/partials/rejectOrder.html',
          controller: 'RejectOrderController',
          controllerAs: 'rejCtrl',
          preserveScope: true,
          clickOutsideToClose: false,
          fullscreen: true
        }).then(function (ads) {
          BlogPostsService.rejectAd(ad.id, ads, function (response) {
            refreshOrRedirect();
          }, function (error) {
            toastr.error('Couldn\'t decline order. Try again please.');
          });
        });
      }
    }

    function refreshOrRedirect() {
      if (vm.newOrders.length > 1) {
        initialize();
      } else {
        $window.location.href = '/#/pub/dashboard';
      }
    }

    $scope.$watch('newOrdersCtrl.query.filter', function (newValue, oldValue) {
      var bookmark = '';

      if (!oldValue) {
        bookmark = vm.query.page;
      } else if (newValue !== oldValue) {
        vm.query.page = 1;
      }

      if (!newValue) {
        vm.query.page = bookmark;
      } else if (newValue === '') {
        vm.query.page = 1;
      }
    });

    function downloadContent(ev, ad) {
      if (ad.blog_post_body) {
        blogPostEditDialog(ev, ad, true);
      } else if (ad.word) {
        return ad.word.url;
      }
    }

    function blogPostEditDialog(ev, ad, download) {
      $mdDialog.show({
        controller: 'BlogPostEditController',
        controllerAs: 'blgEditCtrl',
        templateUrl: 'app/pages/reports/adv/rejected/blogPostsEdit/blogPostsEdit.html',
        hasBackdrop: true,
        targetEvent: ev,
        clickOutsideToClose: true,
        escapeToClose: true,
        focusOnOpen: true,
        locals: {
          ad: ad,
          download: download
        }
      });
    }

    function isRejected(ad) {
      return ad.state === PUB_URL_STATE.REJECTED;
    }
  }
})();
