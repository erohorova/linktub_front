(function () {
  'use strict';

  angular
    .module('linktub.pages.reports.newOrders')
    .controller('RejectOrderController', RejectOrderController);

  /* @ngInject */
  function RejectOrderController($scope, $mdDialog, lodash, RejectedReasonsService) {
    var vm = this;

    //Accesible variables
    vm.rejectReasons = [];
    vm.notes = '';
    vm.allowAgency;
    vm.selected = [];

    //Accesible functions
    vm.submit = submit;
    vm.cancel = cancel;

    initialize();

    function initialize() {
      vm.allowAgency = false;
      vm.rejectedReasons = RejectedReasonsService.getRejectedReasons();
    }

    function submit() {
      var response = {
        rejected_reason: vm.selected,
        notes: vm.notes,
        editable: vm.allowAgency
      };

      $mdDialog.hide(response);
    }

    function cancel() {
      $mdDialog.cancel();
    }
  }
})();
