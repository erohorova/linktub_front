(function () {
  'use strict';

  angular
    .module('linktub.pages.reports.activeAdsContent', [])
    .controller('ActiveAdsContentController', ActiveAdsContentController);

  /* @ngInject */
  function ActiveAdsContentController($scope, $mdEditDialog, $mdDialog, toastr, lodash, BlogPostsService,
    OrdersService, PUB_URL_STATE, ROLE, TYPE_STATE, NotificationService) {
    var vm = this;

    //Accesible variables
    vm.adsNotSubmitted = [];
    vm.filteredNotSubmitted = [];
    vm.queryNotSubmitted = {
      filter: '',
      order: 'order_date',
      limit: 20,
      page: 1
    };

    vm.search = false;
    vm.activeAds = [];
    vm.filtered = [];
    vm.query = {
      filter: '',
      order: '-order_date',
      limit: 20,
      page: 1
    };

    //Accessible functions
    vm.removeFilter = removeFilter;
    vm.removeFilterAds = removeFilterAds;
    vm.noAds = noAds;
    vm.noUnsubmittedAds = noUnsubmittedAds;
    vm.noSubmittedAds = noSubmittedAds;
    vm.showSearchIcon = showSearchIcon;
    vm.submitUrl = submitUrl;
    vm.changeIcon = changeIcon;
    vm.isSubmitted = isSubmitted;
    vm.getIconTooltip = getIconTooltip;
    vm.adRejected = adRejected;
    vm.submitOrNotes = submitOrNotes;
    vm.searchOn = searchOn;

    initialize();

    function initialize() {
      loadUnsubmitted();
      loadActiveAds();
    }

    function loadUnsubmitted() {
      NotificationService.deleteNotification(TYPE_STATE.ACTIVE_ADS, ROLE.PUB);
      BlogPostsService.indexPublisherBlogPosts(PUB_URL_STATE.APPROVE_PUB, function (response) {
        var total_ads = response.data.ads;
        lodash.map(total_ads, function (item) {
          item.submitted = item.full_pub_url;
        });
        vm.adsNotSubmitted = total_ads;
        vm.filterNotSubmitted = vm.adsNotSubmitted;
        loadRejectedNonSubmittedURLs();
      }, function (error) {
        toastr.error('Error');
      });
    }

    function loadRejectedNonSubmittedURLs() {
      BlogPostsService.indexPublisherBlogPosts(PUB_URL_STATE.REJECTED_ADMIN, function (response) {
        var rejected_ads = response.data.ads;
        lodash.map(rejected_ads, function (item) {
          item.submitted = item.full_pub_url;
        });

        var tempAds = lodash.concat(vm.adsNotSubmitted, rejected_ads);
        vm.adsNotSubmitted = tempAds;
        vm.filterNotSubmitted = vm.adsNotSubmitted;
      }, function (error) {
        toastr.error('Error');
      });
    }

    function loadActiveAds() {
      OrdersService.indexCompletedOrders(function (response) {
        vm.activeAds = response.data.orders;
        vm.filtered = vm.activeAds;
      }, function (error) {
        toastr.error('Error');
      });
    }

    $scope.$watch('actAdsCtrl.query.filterNotSubmitted', function (newValue, oldValue) {
      var bookmark = '';

      if (!oldValue) {
        bookmark = vm.query.page;
      } else if (newValue !== oldValue) {
        vm.query.page = 1;
      }

      if (!newValue) {
        vm.query.page = bookmark;
      } else if (newValue === '') {
        vm.query.page = 1;
      }
    });

    function removeFilter() {
      vm.search = false;
      vm.queryNotSubmitted.filter = '';
    }

    function removeFilterAds() {
      vm.search = false;
      vm.query.filter = '';
    }

    $scope.editComment = function (event, ad) {
      event.stopPropagation(); // in case autoselect is enabled
      if (!ad.submitted && (!adRejected(ad))) {
        var editDialog = {
          modelValue: ad.full_pub_url,
          placeholder: 'http://MySite.com/new-post',
          save: function (input) {
            ad.full_pub_url = input.$modelValue;
          },
          targetEvent: event,
          title: 'Live Post URL'
        };
        var promise;
        promise = $mdEditDialog.large(editDialog).then(function (response) {
          ad.submitted = false;
        });
      }
    };
    $scope.editCommentBlur = function (event, ad) {
      event.stopPropagation(); // in case autoselect is enabled
      if (!ad.submitted && (!adRejected(ad))) {
        ad.full_pub_url = event.target.value;
        ad.submitted = false;
      }
    };

    function noAds() {
      return noUnsubmittedAds() && noSubmittedAds();
    }

    function noUnsubmittedAds() {
      return vm.adsNotSubmitted.length === 0;
    }

    function noSubmittedAds() {
      return vm.activeAds.length === 0;
    }

    function showSearchIcon() {
      return 'assets/images/search_icon.svg';
    }

    function submitUrl(ad) {
      if (ad.full_pub_url && !isSubmitted(ad)) {
        BlogPostsService.updateLiveUrl(ad.id, ad.full_pub_url, null, function (response) {
          ad.submitted = true;
          toastr.success('Submitted Ad!');
        }, function (response) {
          toastr.error('Couldn\'t submit live url. Try again please.');
        });
      } else if (!ad.full_pub_url && !isSubmitted(ad)) {
        toastr.error('Must select the live post URL');
      }
    }

    function changeIcon(ad) {
      if (ad.full_pub_url && ad.submitted) {
        return 'hourglass_empty';
      } else if (ad.state === PUB_URL_STATE.REJECTED_ADMIN) {
        return 'assets/images/alert.svg';
      } else {
        return 'check_circle';
      }
      return ad.submitted ? 'hourglass_empty' : 'check_circle';
    }

    function isSubmitted(ad) {
      return ad.state === PUB_URL_STATE.APPROVE_PUB && ad.full_pub_url && ad.submitted;
    }

    function getIconTooltip(ad) {
      if (ad.full_pub_url && ad.submitted && !adRejected(ad)) {
        return 'Waiting on admin approval';
      } else if (adRejected(ad)) {
        return 'Click here to resubmit';
      } else {
        return 'Ready to submit';
      }
    }

    function adRejected(ad) {
      return ad.state === PUB_URL_STATE.REJECTED_ADMIN;
    }

    function showNotes(ad) {
      $mdDialog.show({
        controller: 'NotesControllerActiveContent',
        controllerAs: 'noteCtrl',
        templateUrl: 'app/pages/reports/pub/activeAds/partials/notes.html',
        clickOutsideToClose: false,
        escapeToClose: true,
        focusOnOpen: true,
        locals: {
          ad: ad
        }
      }).then(function (response) {
        if (response.ad.full_pub_url) {
          BlogPostsService.updateLiveUrl(response.ad.id, response.ad.full_pub_url, response.note, function (response) {
            loadUnsubmitted();
            toastr.success('Submitted Ad!');
          }, function (response) {
            toastr.error('Couldn\'t submit live url. Try again please.');
          });
        }
      });
    }

    function submitOrNotes(ad) {
      switch (ad.state) {
      case PUB_URL_STATE.REJECTED_ADMIN:
        showNotes(ad);
        break;
      case PUB_URL_STATE.APPROVE_PUB:
        submitUrl(ad);
        break;
      case PUB_URL_STATE.APPROVE:
        submitUrl(ad);
        break;
      }
    }

    function searchOn() {
      vm.search = true;
    }
  }
})();
