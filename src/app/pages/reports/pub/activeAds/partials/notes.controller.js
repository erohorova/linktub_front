(function () {
  'use strict';

  angular
    .module('linktub.pages.reports.resubmitted')
    .controller('NotesControllerActiveContent', NotesControllerActiveContent);

  /* @ngInject */
  function NotesControllerActiveContent($scope, $mdDialog, lodash, ad) {
    var vm = this;

    //Accesible variables
    vm.notes = [];
    vm.ad = {};
    vm.pubNote;
    vm.pubUrl = '';

    //Accesible functions
    vm.ok = ok;

    initialize();

    function initialize() {
      vm.ad = ad;
      vm.notes = ad.notes;
    }

    function ok() {
      vm.ad.full_pub_url = vm.pubUrl;
      var response = {
        ad: vm.ad,
        note: vm.pubNote
      };
      $mdDialog.hide(response);
    }
  }
})();
