(function () {
  'use strict';

  angular
    .module('linktub.pages.reports.pricingPausing', [])
    .controller('PricingPausingController', PricingPausingController);

  /* @ngInject */
  function PricingPausingController($scope, $mdDialog, $mdEditDialog, toastr, lodash, UrlsService,
    NotificationService, PUB_URL_STATE, TYPE_STATE, ROLE) {
    var vm = this;

    //Accesible variables
    vm.pricingPausing = [];
    vm.filtered = [];
    vm.query = {
      filter: '',
      order: 'submitted',
      page: 1,
      limit: 20
    };

    vm.changeIcon = changeIcon;
    vm.isSubmitted = isSubmitted;
    vm.submitUrl = submitUrl;
    vm.canSubmit = canSubmit;
    vm.showSearchIcon = showSearchIcon;
    vm.removeFilter = removeFilter;
    vm.showInfo = showInfo;
    vm.getRowClass = getRowClass;
    vm.getEditPriceTooltipText = getEditPriceTooltipText;
    vm.toggleOrderPaused = toggleOrderPaused;

    initialize();

    function initialize() {
      getActiveURLs();
    }

    function getActiveURLs() {
      NotificationService.deleteNotification(TYPE_STATE.PRICING_AND_PAUSING, ROLE.PUB);
      UrlsService.indexOnlyUrls([PUB_URL_STATE.ACTIVE, PUB_URL_STATE.PAUSED, PUB_URL_STATE.PENDING, PUB_URL_STATE.REJECTED], function (response) {
        vm.pricingPausing = response.data.urls;
        lodash.map(vm.pricingPausing, function (item) {
          item.submitted = lodash.includes([PUB_URL_STATE.PENDING, PUB_URL_STATE.REJECTED], item.state);
          item.oldPrice = item.price;
          item.paused = item.state === PUB_URL_STATE.PAUSED ? true : false;
          item.oldPaused = item.paused;
        });
        vm.filtered = vm.pricingPausing;
      }, function (response) {
        toastr.error("Couldn't retrieve URLs. Try again please.");
      });
    }

    function changeIcon(ad) {
      return ad.submitted ? 'hourglass_empty' : 'check_circle';
    }

    $scope.editPrice = function (event, url) {
      event.stopPropagation(); // in case autoselect is enabled
      if (!url.submitted || isRejected(url)) {
        var editDialog = {
          modelValue: url.price,
          placeholder: 'Payout',
          save: function (input) {
            url.price = input.$modelValue;
            if (priceChanged(url)) {
              vm.submitUrl(url);
            }
          },
          targetEvent: event,
          title: 'Change Your Price',
          type: 'number',
          validators: {
            'step': .01
          }
        };
        var promise = $mdEditDialog.large(editDialog);
      }
    };
    $scope.editPriceBlur = function (event, url) {
      event.stopPropagation(); // in case autoselect is enabled
      if (!url.submitted || isRejected(url)) {
        url.price = event.target.value;
        if (priceChanged(url)) {
          vm.submitUrl(url);
        }
      }
    };

    function priceChanged(url) {
      return url.price != url.oldPrice;
    }

    function pausedChanged(url) {
      return url.paused != url.oldPaused;
    }

    function isSubmitted(url) {
      return url.state === PUB_URL_STATE.PENDING && url.submitted;
    }

    function submitUrl(url) {
      if (canSubmit(url) && (!url.submitted || isRejected(url))) {
        $mdDialog.show({
          controller: 'PauseController',
          controllerAs: 'pauseCtrl',
          templateUrl: 'app/pages/reports/pub/pricingAndPausing/partials/pause.html',
          hasBackdrop: true,
          clickOutsideToClose: true,
          escapeToClose: true,
          focusOnOpen: true,
          locals: {
            url: url
          }
        }).then(function () {
          initialize();
        }).catch(function () {
          if (pausedChanged(url)) {
            url.paused = !url.paused;
          }

          if (priceChanged(url)) {
            url.price = url.oldPrice;
          }
        });
      }
    }

    function canSubmit(url) {
      return (!url.paused && priceChanged(url)) || pausedChanged(url);
    }

    function removeFilter() {
      vm.search = false;
      vm.query.filter = '';
      vm.query.page = 1;
    }

    function showSearchIcon() {
      return 'assets/images/search_icon.svg';
    }

    $scope.$watch('ppCtrl.query.filter', function (newValue, oldValue) {
      var bookmark = null;

      if (!oldValue) {
        bookmark = vm.query.page;
      } else if (newValue !== oldValue) {
        vm.query.page = 1;
      }

      if (!newValue) {
        vm.query.page = bookmark;
      } else if (newValue === '') {
        vm.query.page = 1;
      }

      if(vm.query.page === null) {
        vm.query.page = 1;
      }
    });

    function showInfo() {
      return $mdDialog.show(
        $mdDialog.alert()
        .title('Pricing & Pausing')
        .htmlContent('Here is where you change your price and remove your site ' +
          'temporarily from receiving new orders.<br> You can change these things at any time.')
        .escapeToClose(false)
        .ok('Ok')
      );
    }

    function getRowClass(url) {
      switch (url.state) {
        case PUB_URL_STATE.PENDING:
          return 'submitted-url';
        case PUB_URL_STATE.REJECTED:
          return 'rejected-url'
      }
    }

    function getEditPriceTooltipText(url) {
      return isRejected(url) ? 'Price change needed for approval (see notes in email). Click here to edit.' : 'Click Here to Edit Price';
    }

    function isRejected(url) {
      return url.submitted && url.state === PUB_URL_STATE.REJECTED;
    }

    function toggleOrderPaused(order) {
      order.paused = !order.paused;
    }
  }
})();
