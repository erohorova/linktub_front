(function () {
  'use strict';

  angular
    .module('linktub.pages.reports.pricingPausing')
    .controller('PauseController', PauseController);

  /* @ngInject */
  function PauseController($scope, $mdDialog, lodash, toastr, UrlsService, url) {
    var vm = this;

    //Accesible variables
    vm.url = {};

    //Accesible functions
    vm.ok = ok;
    vm.cancel = cancel;
    vm.priceChanged = priceChanged;
    vm.pausedChanged = pausedChanged;
    vm.isPausing = isPausing;
    vm.isUnpausing = isUnpausing;

    initialize();

    function initialize() {
      vm.url = url;
    }

    function priceChanged() {
      return vm.url.price != vm.url.oldPrice;
    }

    function pausedChanged() {
      return vm.url.oldPaused != vm.url.paused;
    }

    function isPausing() {
      return pausedChanged() && vm.url.paused;
    }

    function isUnpausing() {
      return pausedChanged() && !vm.url.paused;
    }

    function ok() {
      UrlsService.updateUrl(vm.url, function() {
        toastr.success('URL was updated!');
        $mdDialog.hide();
      }, function() {
        toastr.error("Couldn't update URL. Try again please.");
      });
    }

    function cancel() {
      $mdDialog.cancel();
    }
  }
})();
