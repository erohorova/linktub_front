(function () {
  'use strict';

  angular
    .module('linktub.pages.reports', [
      'linktub.pages.reports.rejected',
      'linktub.pages.reports.activeContent',
      'linktub.pages.reports.awaitingContent',
      'linktub.pages.reports.activeAdsContent',
      'linktub.pages.reports.newOrders',
      'linktub.pages.reports.pricingPausing',
      'linktub.pages.reports.resubmitted'
    ])
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('rejected-reports', {
        parent: 'dashboard',
        url: '/reports/rejected',
        templateUrl: 'app/pages/reports/adv/rejected/reportRejected.html',
        controller: 'ReportRejectedController',
        controllerAs: 'repRejCtrl',
        authentication: {
          required: true,
          permittedRoles: ['advertiser']
        }
      })
      .state('active-content', {
        parent: 'dashboard',
        url: '/reports/activeContent',
        templateUrl: 'app/pages/reports/adv/activeContent/reportActiveContent.html',
        controller: 'ReportActiveContentController',
        controllerAs: 'repActCntCtrl',
        authentication: {
          required: true,
          permittedRoles: ['advertiser']
        }
      })
      .state('awaiting-content', {
        parent: 'dashboard',
        url: '/reports/awaitingContent',
        templateUrl: 'app/pages/reports/adv/awaitingContent/reportAwaitingContent.html',
        controller: 'ReportAwaitingContentController',
        controllerAs: 'repAwaitingCtrl',
        authentication: {
          required: true,
          permittedRoles: ['advertiser']
        }
      })
      .state('new-orders',  {
        parent: 'dashboard',
        url: '/reports/newOrders',
        templateUrl: 'app/pages/reports/pub/newOrders/newOrders.html',
        controller: 'NewOrdersController',
        controllerAs: 'newOrdersCtrl',
        authentication: {
          required: true,
          permittedRoles: ['publisher']
        }
      })
      .state('active-ads',  {
        parent: 'dashboard',
        url: '/reports/activeAds',
        templateUrl: 'app/pages/reports/pub/activeAds/activeAdsContent.html',
        controller: 'ActiveAdsContentController',
        controllerAs: 'actAdsCtrl',
        authentication: {
          required: true,
          permittedRoles: ['publisher']
        }
      })
      .state('pricing-pausing',  {
        parent: 'dashboard',
        url: '/reports/pricingPausing',
        templateUrl: 'app/pages/reports/pub/pricingAndPausing/pricingPausing.html',
        controller: 'PricingPausingController',
        controllerAs: 'ppCtrl',
        authentication: {
          required: true,
          permittedRoles: ['publisher']
        }
      })
      .state('resubmitted',  {
        parent: 'dashboard',
        url: '/reports/resubmitted',
        templateUrl: 'app/pages/reports/pub/resubmittedContent/resubmitted.html',
        controller: 'ResubmittedController',
        controllerAs: 'resCtrl',
        authentication: {
          required: true,
          permittedRoles: ['publisher']
        }
      });
  }
})();
