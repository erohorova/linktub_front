(function () {
  'use strict';

  angular
    .module('linktub.pages.reports.awaitingContent', [])
    .controller('ReportAwaitingContentController', ReportAwaitingContentController);

  /* @ngInject */
  function ReportAwaitingContentController($log, $mdDialog, $anchorScroll, $state, $scope, toastr, lodash,
    TYPE_STATE, ROLE, BlogPostsService, NotificationService) {
    var vm = this;

    //Accesible variables
    vm.awaitingAds = [];
    vm.filtered = [];
    vm.query = {
      filter: '',
      order: '-requested_date',
      limit: 20,
      page: 1
    };

    //Accesible functions
    vm.downloadContent = downloadContent;
    vm.cancel = cancel;

    initialize();

    function initialize() {
      NotificationService.deleteNotification(TYPE_STATE.AWAITING_PUBLICATION, ROLE.ADV);
      BlogPostsService.indexBlogPosts({ state: TYPE_STATE.AWAITING_PUBLICATION }, function (response) {
        vm.awaitingAds = response.data.ads;
        vm.filtered = vm.activeAds;
      }, function () {
        $log.error('Error retrieving blog posts');
      });
    }

    $scope.$watch('repAwaitingCtrl.query.filter', function (newValue, oldValue) {
      var bookmark = '';

      if (!oldValue) {
        bookmark = vm.query.page;
      }

      if (newValue !== oldValue) {
        vm.query.page = 1;
      }

      if (!newValue) {
        vm.query.page = bookmark;
      }

      if (newValue == '') {
        vm.query.page = 1;
      }
    });

    function downloadContent(ev, ad) {
      if (ad.blog_post_body) {
        blogPostEditDialog(ev, ad, true);
      } else if (ad.word) {
        return ad.word.url;
      }
    }

    function blogPostEditDialog(ev, ad, download) {
      $mdDialog.show({
        controller: 'BlogPostEditController',
        controllerAs: 'blgEditCtrl',
        templateUrl: 'app/pages/reports/adv/rejected/blogPostsEdit/blogPostsEdit.html',
        hasBackdrop: true,
        targetEvent: ev,
        clickOutsideToClose: true,
        escapeToClose: true,
        focusOnOpen: true,
        locals: {
          ad: ad,
          download: download
        }
      });
    }

    function cancel() {
      $mdDialog.hide();
    }
  }
})();
