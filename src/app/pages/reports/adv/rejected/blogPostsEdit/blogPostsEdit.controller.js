(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('BlogPostEditController', BlogPostEditController);

  /** @ngInject */
  function BlogPostEditController($log, $mdDialog, toastr, lodash, CampaignsService, BlogPostsService, ad, download) {
    var vm = this;

    // Accessible variables
    vm.blogPost = {};
    vm.infographic = 'no';
    vm.campaigns = [];
    vm.download = download;
    vm.word = {};

    // Accessible functions
    vm.save = save;
    vm.closeDialog = closeDialog;
    vm.setTitle = setTitle;
    vm.isDocRequired = isDocRequired;
    vm.noUrl = noUrl;
    vm.hasContent = hasContent;

    initialize();

    // Functions
    function initialize() {
      CampaignsService.indexCampaigns(function (response) {
        vm.campaigns = response.data.campaigns;
        if (ad) {
          vm.campaign = ad.campaign;
          setupForm(ad);
        } else {
          vm.blogPost = {}
        }
      }, function () {
        $log.error('There was an error retrieving campaigns.');
        toastr.error('Error retrieving campaigns.');
      });
    }

    function setupForm(ad) {
      var campaign = lodash.find(vm.campaigns, function (campaign) {
        return campaign.id == ad.campaign.id;
      });
      var link = lodash.find(campaign.links, function (link) {
        return link.id == ad.link.id;
      });

      vm.newURL = ad.link.href;
      vm.blogPost.campaign = campaign;
      vm.infographic = ad.blog_post_body ? 'yes' : 'no';
      vm.blogPost.blog_post_body = ad.blog_post_body || null;
      vm.word = ad.word || null;
      vm.blogPost.word = vm.word;
      vm.blogPost.id = ad.id;
      vm.blogPost.url = ad.url;
      vm.blogPost.link = link;
      vm.blogPost.blog_post_title = ad.blog_post_title;
      vm.blogPost.link_text = ad.link_text;
      vm.blogPost.editable = ad.editable;
    }

    function save() {
      if (isValidURL(vm.newURL)) {
        vm.blogPost.link = vm.newURL;
        vm.blogPost.campaign.url = vm.newURL;
        CampaignsService.updateCampaign(vm.blogPost.campaign.id, vm.blogPost.campaign,
          function (response) {
            var addedLink = response.data.link;
            createBlogPost(addedLink);
          },
          function (response) {
            var message = response.data.errors['links.href'][0];
            $log.error('Error submiting new site.');
            toastr.error('The site ' + message + '. Please retry.');
          });
      }
    }

    function createBlogPost(link) {
      if (angular.isDefined(link)) {
        vm.blogPost.link = link;
      }

      if (!vm.blogPost.blog_post_body || vm.blogPost.blog_post_body === 'null') {
        vm.blogPost.blog_post_body = null;
      }

      BlogPostsService.updateBlogPost(vm.blogPost.id, vm.blogPost, function () {
        $mdDialog.hide(vm.blogPost.id);
        toastr.success('Updated blog post.');
      }, function () {
        $log.error('There was an error submiting blog post.');
        toastr.error('Error submiting blog post.');
      });
    }

    function closeDialog() {
      $mdDialog.cancel();
    }

    function setTitle() {
      return vm.blogPost.editable ? 'Edit Blog Post' : 'Download infographic';
    }

    function isDocRequired() {
      if (vm.word) {
        return vm.word && !vm.blogPost.word;
      }

      return false;
    }

    function noUrl() {
      return !isValidURL(vm.blogPost.link) && !isValidURL(vm.newURL);
    }

    function hasContent() {
      return vm.blogPost.word || vm.blogPost.blog_post_body;
    }

    function isValidURL(url) {
      return angular.isDefined(url) && url !== null && url !== '';
    }
  }
})();
