(function () {
  'use strict';

  angular
    .module('linktub.pages.reports.rejected')
    .controller('PubNotesController', PubNotesController);

  /* @ngInject */
  function PubNotesController($scope, $mdDialog, lodash, order) {
    var vm = this;

    //Accesible variables
    vm.rejectedReason = '';
    vm.note = '';
    vm.allowAgency;

    //Accesible functions
    vm.ok = ok;

    initialize();

    function initialize() {
      vm.allowAgency = true;
      vm.rejectedReason = order.rejected_reason;
      vm.note = angular.isDefined(order.notes[0]) ? order.notes[0] : '';
    }

    function ok() {
      $mdDialog.hide();
    }
  }
})();
