(function () {
  'use strict';

  angular
    .module('linktub.pages.reports.rejected', [])
    .controller('ReportRejectedController', ReportRejectedController);

  /* @ngInject */
  function ReportRejectedController($log, $mdDialog, $mdMedia, lodash, TYPE_STATE, ROLE, RoutesService,
    BlogPostsService, NotificationService) {
    var vm = this;

    //Accesible variables
    vm.rejectedAds = [];
    vm.perPage = 10;
    vm.filtered = [];
    vm.query = {
      filter: '',
      order: '-requested_date',
      limit: 20,
      page: 1
    };

    //Accessible functions
    vm.deleteUrl = deleteUrl;
    vm.downloadContent = downloadContent;
    vm.showInstructions = showInstructions;
    vm.blogPostEditDialog = blogPostEditDialog;
    vm.showMoreNotes = showMoreNotes;
    vm.cancel = cancel;
    vm.canEdit = canEdit;
    vm.canDelete = canDelete;
    vm.$mdMedia = $mdMedia;
    vm.showNotesIcon = showNotesIcon;
    vm.showSearchIcon = showSearchIcon;
    vm.removeFilter = removeFilter;
    vm.getTooltipText = getTooltipText;
    vm.notesSeen = notesSeen;

    function getTooltipText(input, ad) {
      switch (input) {
      case 'notes':
        if (!ad.downloaded) {
          return 'Please download content first';
        } else {
          return 'Read notes';
        }
      case 'delete':
        if (ad.editable && !ad.downloaded) {
          return 'Please download content first';
        } else if (!ad.sawNote && ad.downloaded) {
          return 'Read notes first please';
        } else {
          return 'Delete';
        }
      case 'edit':
        if (ad.editable && !ad.downloaded) {
          return 'Please download content first';
        } else if (!ad.sawNote && ad.downloaded && ad.editable) {
          return 'Read notes first please';
        } else {
          return 'Edit';
        }
      }
    }

    initialize();

    function initialize() {
      NotificationService.deleteNotification(TYPE_STATE.REJECTED_CONTENT, ROLE.ADV);
      BlogPostsService.indexBlogPosts({ state: TYPE_STATE.REJECTED_OR_CANCELLED_CONTENT }, function (response) {
        var aux = lodash.map(response.data.ads, function (ad) {
          ad.sawNote = false;
          ad.downloaded = false;
          return ad;
        });
        vm.rejectedAds = aux;
        vm.filtered = vm.rejectedAds;
      }, function () {
        $log.error('Error retrieving blog posts');
      });
    }

    function deleteUrl(id) {
      var confirm = $mdDialog.confirm()
        .title('Would you like to delete this content?')
        .textContent('This content will be removed permanently.')
        .ariaLabel('Removing content')
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function () {
        BlogPostsService.deleteBlogPost(id, function () {
          lodash.remove(vm.rejectedAds, function (item) {
            return item.id == id;
          });
        }, function () {
          $log.error('Error deleting blog posts');
        });
      });
    }

    function showInstructions() {
      $mdDialog.show({
        controller: 'ReportRejectedController',
        controllerAs: 'repRejCtrl',
        templateUrl: 'app/pages/reports/adv/rejected/instructions.html',
        hasBackdrop: true,
        clickOutsideToClose: true,
        escapeToClose: true,
        focusOnOpen: true
      });
    }

    function showMoreNotes(ad) {
      ad.sawNote = true;
      $mdDialog.show({
        templateUrl: 'app/pages/reports/adv/rejected/pubNotes/pubNotes.html',
        controller: 'PubNotesController',
        controllerAs: 'pubNotesCtrl',
        preserveScope: true,
        clickOutsideToClose: false,
        fullscreen: true,
        locals: {
          order: ad
        }
      });
    }

    function downloadContent(ev, ad) {
      ad.downloaded = true;
      if (ad.blog_post_body) {
        blogPostEditDialog(ev, ad, true);
      } else if (ad.word) {
        return ad.word.url;
      }
    }

    function blogPostEditDialog(ev, ad, download) {
      $mdDialog.show({
        controller: 'BlogPostEditController',
        controllerAs: 'blgEditCtrl',
        templateUrl: 'app/pages/reports/adv/rejected/blogPostsEdit/blogPostsEdit.html',
        hasBackdrop: true,
        targetEvent: ev,
        clickOutsideToClose: true,
        escapeToClose: true,
        focusOnOpen: true,
        locals: {
          ad: ad,
          download: download
        }
      }).then(function (id) {
        lodash.remove(vm.rejectedAds, function (item) {
          return item.id == id;
        });

        if (vm.rejectedAds.length == 0) {
          return RoutesService.dashboard();
        }
      }, function () {
        $log.log('Dialog was canceled');
      });
    }

    function removeFilter() {
      vm.search = false;
      vm.query.filter = '';
    }

    function showSearchIcon() {
      return 'assets/images/search_icon.svg';
    }

    function cancel() {
      $mdDialog.hide();
    }

    function canEdit(ad) {
      return (ad.editable && ad.downloaded && ad.sawNote);
    }

    function canDelete(ad) {
      return (ad.downloaded && ad.sawNote);
    }

    function showNotesIcon() {
      return 'assets/images/note.svg';
    }

    function notesSeen(ad) {
      ad.sawNote = true;
    }
  }
})();
