(function () {
  'use strict';

  angular
    .module('linktub.pages.reports.activeContent', [])
    .controller('ReportActiveContentController', ReportActiveContentController);

  /* @ngInject */
  function ReportActiveContentController($log, $mdDialog, $anchorScroll, $state, $scope, toastr, lodash,
    TYPE_STATE, ROLE, BLOG_POST_STATES, BlogPostsService, NotificationService) {
    var vm = this;

    //Accesible variables
    vm.activeAds = [];
    vm.filtered = [];
    vm.active_csv_url = '';
    vm.query = {
      filter: '',
      order: '-requested_date',
      limit: 20,
      page: 1
    };

    vm.cancelledBlogPost = cancelledBlogPost;

    initialize();

    function initialize() {
      NotificationService.deleteNotification(TYPE_STATE.ACTIVE_CONTENT, ROLE.ADV);
      BlogPostsService.indexBlogPosts({ state: TYPE_STATE.ACTIVE_CONTENT }, function (response) {
        vm.activeAds = response.data.ads;
        vm.filtered = vm.activeAds;
        vm.active_csv_url = response.data.spreadsheet_file;
      }, function () {
        $log.error('Error retrieving blog posts');
      });
    }

    $scope.$watch('repActCntCtrl.query.filter', function (newValue, oldValue) {
      var bookmark = '';

      if (!oldValue) {
        bookmark = vm.query.page;
      }

      if (newValue !== oldValue) {
        vm.query.page = 1;
      }

      if (!newValue) {
        vm.query.page = bookmark;
      }

      if (newValue == '') {
        vm.query.page = 1;
      }
    });

    function cancelledBlogPost(ad) {
      return ad.state === BLOG_POST_STATES.CANCELLED;
    }
  }
})();
