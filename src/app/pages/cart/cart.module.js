(function () {
  'use strict';

  angular
    .module('linktub.pages.cart')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('cart', {
        parent: 'dashboard',
        url: '/cart',
        templateUrl: 'app/pages/cart/cart.html',
        controller: 'CartController',
        controllerAs: 'crtCtrl',
        authentication: {
          required: true,
          permittedRoles: ['advertiser']
        }
      })
      .state('checkout', {
        parent: 'dashboard',
        url: '/cart/checkout',
        templateUrl: 'app/pages/cart/checkout/checkout.html',
        controller: 'CheckoutController',
        controllerAs: 'chkCtrl',
        authentication: {
          required: true,
          permittedRoles: ['advertiser']
        }
      });
  }
})();
