(function () {
  'use strict';

  angular
    .module('linktub.pages.cart', [])
    .controller('CartController', CartController);

  /** @ngInject */
  function CartController($log, $mdDialog, $anchorScroll, $state, lodash, toastr, OrdersService, SelectedOrdersService) {
    var vm = this;

    // Accessible attributes
    vm.orders = [];
    vm.perPage = 10;
    vm.selectedOrders = [];

    // Accessible functions
    vm.goTop = goTop;
    vm.toggleAll = toggleAll;
    vm.allSelected = allSelected;
    vm.selectedItems = selectedItems;
    vm.deleteSelected = deleteSelected;
    vm.cannotCheckout = cannotCheckout;
    vm.cannotDeleteOrders = cannotDelete;
    vm.checkout = checkout;

    initialize();

    // Functions
    function initialize() {
      SelectedOrdersService.clean();
      OrdersService.indexOrders(function (orderList) {
        vm.orders = orderList;
      }, function () {
        toastr.error('There was an error retrieving your orders.');
      })
    }

    function deleteSelected(ev) {
      var confirm = $mdDialog.confirm()
        .title('Would you like to delete this ad spot/s?')
        .textContent('This ad spot/s will be removed permanently.')
        .ariaLabel('Removing ad spots')
        .targetEvent(ev)
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function () {
        OrdersService.deleteOrders(lodash.map(vm.selectedOrders, 'id'), function () {
          toastr.success('Removed orders successfuly.');
          OrdersService.indexOrders(function (orderList) {
            vm.orders = orderList;
          }, function () {
            toastr.error('There was an error retrieving your orders.');
          })
          vm.selectedOrders = [];
        }, function () {
          $log.error('Error deleting orders.');
        })
      });
    }

    function goTop() {
      $anchorScroll('top');
    }

    function toggleAll() {
      var all = allSelected();
      lodash.map(vm.orders, function (order) {
        var pos = lodash.indexOf(vm.selectedOrders, order);
        if (all && pos >= 0) {
          vm.selectedOrders.splice(pos, 1);
        } else if (pos < 0) {
          vm.selectedOrders.push(order);
        }
      });
    }

    function allSelected() {
      return vm.selectedOrders.length > 0 &&
        vm.selectedOrders.length == vm.orders.length;
    }

    function selectedItems() {
      return (vm.selectedOrders.length > 1) ? vm.selectedOrders.length + ' Items selected' : '1 Item selected';
    }

    function cannotCheckout() {
      if (vm.selectedOrders.length < 1) {
        return true;
      }
      var pos = lodash.findIndex(vm.selectedOrders, function (order) {
        return checkBlogContent(order);
      });

      return pos != -1;
    }

    function checkBlogContent(order) {
      if (order.ad) {
        if (order.ad.blog_post_body || order.ad.word) {
          return false;
        }
      }
      return true;
    }

    function cannotDelete() {
      return (vm.selectedOrders.length == 0 ||
        (vm.selectedOrders.length > 1 && vm.allSelected()));
    }

    function checkout() {
      SelectedOrdersService.setOrdersCart(vm.selectedOrders);
      $state.go('checkout');
    }
  }
})();
