(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('BlogPostController', BlogPostController);

  /** @ngInject */
  function BlogPostController($log, $mdDialog, $scope, $mdMedia, toastr, lodash, order, CampaignsService, OrdersService) {
    var vm = this;

    // Accessible variables
    vm.blogPost = {};
    vm.infographic = 'no';
    vm.campaigns = [];
    vm.newURL;
    vm.link;
    vm.$mdMedia = $mdMedia;
    vm.order = {};

    // Accessible functions
    vm.save = save;
    vm.closeDialog = closeDialog;
    vm.hasContent = hasContent;
    vm.downloadContent = downloadContent;
    vm.canDownload = canDownload;
    vm.setEmptyDropdownUrl = setEmptyDropdownUrl;
    vm.setEmptyNewUrl = setEmptyNewUrl;
    vm.noUrl = noUrl;
    vm.isValidURL = isValidURL;

    initialize();

    // Functions
    function initialize() {
      CampaignsService.indexScopedCampaigns('active', function (response) {
        vm.campaigns = response.data.campaigns;
        vm.order = order;
        if (order.ad) {
          setupForm(order.ad, order.ad.link);
        } else {
          vm.blogPost = {}
        }
      }, function () {
        $log.error('There was an error retrieving campaigns.');
        toastr.error('Error retrieving campaigns.');
      });
    }

    function setupForm(ad, url) {
      vm.blogPost = ad;

      var campaign = lodash.find(vm.campaigns, function (campaign) {
        return campaign.id == ad.campaign.id;
      });
      vm.blogPost.campaign = campaign;

      if(url) {
        vm.newURL = url.href;
        var link = lodash.find(campaign.links, function (link) {
          return link.id == ad.link.id;
        });
        vm.blogPost.link = link;
      }

      if (checkBlogPostBody()) {
        vm.infographic = 'yes';
      }
    }

    function save() {
      if (isValidURL(vm.newURL)) {
        vm.blogPost.link = vm.newURL;
        vm.blogPost.campaign.url = vm.newURL;
        CampaignsService.updateCampaign(vm.blogPost.campaign.id, vm.blogPost.campaign,
          function (response) {
            var addedLink = response.data.link;
            createBlogPost(addedLink);
          },
          function (response) {
            var message = response.data.errors['links.href'][0];
            $log.error('Error submiting new site.');
            toastr.error('The site ' + message + '. Please retry.');
          });
      }
    }

    function checkBlogPostBody() {
      if (!vm.blogPost.blog_post_body || vm.blogPost.blog_post_body === 'null') {
        vm.blog_post_body = null;
        return false;
      }

      return true;
    }

    function createBlogPost(link) {
      if (angular.isDefined(link)) {
        vm.blogPost.link = link;
      }

      if (!vm.blogPost.blog_post_body || vm.blogPost.blog_post_body === 'null') {
        vm.blogPost.blog_post_body = null;
      }

      vm.blogPost.url = order.url;
      OrdersService.updateOrder(order.id, vm.blogPost, function (response) {
        vm.blogPost = response.data.ad;
        toastr.success('Order successfuly submitted.');
        $mdDialog.hide(vm.blogPost);
      }, function () {
        $log.error('There was an error submiting order.');
        toastr.error('Error submiting order.');
      });
    }

    function closeDialog() {
      $mdDialog.cancel();
    }

    function hasContent() {
      if (vm.blogPost.word || vm.blogPost.blog_post_body) {
        return true;
      }
      return false;
    }

    function downloadContent(blog) {
      return blog.word.url;
    }

    function canDownload(blog) {
      return angular.isDefined(blog.word.url);
    }

    function setEmptyDropdownUrl() {
      if (isValidURL(vm.newURL)) {
        vm.blogPost.link = null;
      }
    }

    function setEmptyNewUrl() {
      if (isValidURL(vm.blogPost.link)) {
        vm.newURL = null;
      }
    }

    function noUrl() {
      return !isValidURL(vm.blogPost.link) && !isValidURL(vm.newURL);
    }

    function isValidURL(url) {
      return angular.isDefined(url) && url != null && url != '';
    }
  }
})();
