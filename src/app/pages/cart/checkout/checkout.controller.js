(function () {
  'use strict';

  angular
    .module('linktub.pages.cart')
    .controller('CheckoutController', CheckoutController);

  /* @ngInject */
  function CheckoutController() {}
})();
