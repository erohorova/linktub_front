(function () {
  'use strict';

  angular
    .module('linktub.pages.dashboard.adv', [])
    .controller('AdvDashboardController', AdvDashboardController);

  /* @ngInject */
  function AdvDashboardController($state, $log, $rootScope, toastr, lodash, AccountService, CounterService,
    OrdersService, KEY_CODES) {
    var vm = this;

    // Accessible attributes
    vm.advertiser = {};
    vm.selectedCategories = [];

    // Accessible functions
    vm.search = search;
    vm.seeReport = seeReport;
    vm.showActiveContent = showActiveContent;
    vm.showAwaitingContent = showAwaitingContent;
    vm.seeCart = seeCart;
    vm.searchEnter = searchEnter;
    vm.getPlaceholder = getPlaceholder;

    initialize();

    // Functions
    function initialize() {
      AccountService.getAdvertiser(function (response) {
        vm.advertiser = response.data;
        CounterService.getAdvCardValues();
      }, function () {
        $log.error('Error retrieving user\'s info');
        toastr.error('There was an error retrieving your dashboard\'s info.')
      });

      OrdersService.indexOrders(function (response) {
        $rootScope.cartItems = response;
      }, function (error) {
        toastr.error('There was an error retrieving orders');
      });
    }

    function search() {
      $rootScope.categories = vm.selectedCategories;
      $state.go('search');
    }

    function seeReport() {
      $state.go('rejected-reports');
    }

    function showActiveContent() {
      $state.go('active-content');
    }

    function showAwaitingContent() {
      $state.go('awaiting-content');
    }

    function seeCart() {
      $state.go('cart');
    }

    function searchEnter(event) {
      if (event.keyCode == KEY_CODES.ENTER) {
        search();
      }
    }

    function getPlaceholder() {
      return (vm.selectedCategories.length > 0) ? ' ' : 'Type & Select or Press Enter';
    }
  }
})();
