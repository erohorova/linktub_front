(function () {
  'use strict';

  angular
    .module('linktub.pages.dashboard', [
      'linktub.pages.dashboard.adv',
      'linktub.pages.dashboard.pub'
    ])
    .config(routesConfig);

  /** @ngInject */
  function routesConfig($stateProvider) {
    $stateProvider
      .state('dashboard', {
        abstract: true,
        templateUrl: 'app/pages/dashboard/dashboard.html'
      })
      .state('adv-dash', {
        parent: 'dashboard',
        url: '/adv/dashboard',
        templateUrl: 'app/pages/dashboard/adv/advDashboard.html',
        controller: 'AdvDashboardController',
        controllerAs: 'advCtrl',
        authentication: {
          required: true,
          permittedRoles: ['advertiser']
        }
      })
      .state('pub-dash', {
        parent: 'dashboard',
        url: '/pub/dashboard',
        templateUrl: 'app/pages/dashboard/pub/pubDashboard.html',
        controller: 'PubDashboardController',
        controllerAs: 'pubCtrl',
        authentication: {
          required: true,
          permittedRoles: ['publisher']
        }
      });
  }
})();
