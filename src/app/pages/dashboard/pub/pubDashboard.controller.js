(function () {
  'use strict';

  angular
    .module('linktub.pages.dashboard.pub', [])
    .controller('PubDashboardController', PubDashboardController);

  /* @ngInject */
  function PubDashboardController($log, toastr, AccountService, RoutesService, CounterService) {
    var vm = this;

    //Accessible variables

    //Accessible functions
    vm.showActiveContent = showActiveContent;
    vm.newOrders = newOrders;
    vm.pricingPausing = pricingPausing;
    vm.resubmittedContent = resubmittedContent;

    initialize();

    // Functions
    function initialize() {
      AccountService.getPublisher(function (response) {
        vm.publisher = response.data;
        CounterService.getPubCardValues();
      }, function () {
        $log.error('Error retrieving user\'s info');
        toastr.error('There was an error retrieving your dashboard\'s info.')
      });
    }

    function newOrders() {
      RoutesService.pubNewOrders();
    }

    function showActiveContent() {
      RoutesService.pubActiveAds();
    }

    function pricingPausing() {
      RoutesService.pubPricingPausing();
    }

    function resubmittedContent() {
      RoutesService.pubResubmitted();
    }
  }
})();
