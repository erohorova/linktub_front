(function () {
  'use strict';

  angular
    .module('linktub.pages.search', [])
    .controller('SearchController', SearchController);

  /** @ngInject */
  function SearchController($log, $stateParams, $timeout, $anchorScroll, $mdDialog,
    $rootScope, lodash, toastr, SearchService, CampaignsService,
    RoutesService, AccountService, KEY_CODES, HTTP_STATUS) {
    var vm = this;

    var delay;
    var TIME_DELAY_MS = 800;
    var MAX_VALUE = 100;
    var MAX_PRICE = 1000;

    // Accessible attributes
    vm.urls = [];
    vm.perPage = 20;
    vm.selectedCategories = [];
    vm.input = '';
    vm.currentPage = 1;
    vm.totalPages = 0;
    vm.totalItems = 0;
    vm.showCategories = true;
    vm.advertiserCampaigns = [];
    vm.searchingCount = 0;

    // Filters' configurations
    vm.domain_authority = {
      min: 0,
      max: MAX_VALUE,
      options: {
        floor: 0,
        ceil: MAX_VALUE,
        step: 5,
        minRange: MAX_VALUE * 0.1,
        maxRange: MAX_VALUE,
        noSwitching: true,
        hideLimitLabels: true,
        hidePointerLabels: true,
        onEnd: function () {
          setupTimer();
        }
      }
    }
    vm.trust_flow = {
      min: 0,
      max: MAX_VALUE,
      options: {
        floor: 0,
        ceil: MAX_VALUE,
        step: 5,
        minRange: MAX_VALUE * 0.1,
        maxRange: MAX_VALUE,
        noSwitching: true,
        hideLimitLabels: true,
        hidePointerLabels: true,
        onEnd: function () {
          setupTimer();
        }
      }
    }
    vm.citation_flow = {
      min: 0,
      max: MAX_VALUE,
      options: {
        floor: 0,
        ceil: MAX_VALUE,
        step: 5,
        minRange: MAX_VALUE * 0.1,
        maxRange: MAX_VALUE,
        noSwitching: true,
        hideLimitLabels: true,
        hidePointerLabels: true,
        onEnd: function () {
          setupTimer();
        }
      }
    }

    vm.price_range = {
      min: 0,
      max: MAX_PRICE,
      options: {
        floor: 0,
        ceil: MAX_PRICE,
        step: 50,
        minRange: MAX_PRICE * 0.1,
        maxRange: MAX_PRICE,
        noSwitching: true,
        hideLimitLabels: true,
        hidePointerLabels: true,
        onEnd: function () {
          setupTimer();
        }
      }
    }

    // Sort filter
    vm.sortOptions = [{
        name: 'Domain Authority High to Low',
        value: 'domain_authority desc'
      },
      {
        name: 'Domain Authority Low to High',
        value: 'domain_authority asc'
      },
      {
        name: 'Trust Flow High to Low',
        value: 'trust_flow desc'
      },
      {
        name: 'Trust Flow Low to High',
        value: 'trust_flow asc'
      },
      {
        name: 'Price High to Low',
        value: 'price desc'
      }, {
        name: 'Price Low to High',
        value: 'price asc'
      }
    ];

    // Accessible functions
    vm.setupTimer = setupTimer;
    vm.executeQuery = executeQuery;
    vm.searchInput = searchInput;
    vm.queryEnter = queryEnter;
    vm.sortBy = sortBy;
    vm.retrieveUrls = retrieveUrls;
    vm.getPlaceholder = getPlaceholder;
    vm.search = search;
    vm.categoriesChanged = categoriesChanged;

    initialize();

    // Functions
    function initialize() {
      vm.filter = vm.sortOptions[0];
      // Setup params from call if they are defined
      if (angular.isDefined($stateParams.categories) && $stateParams.categories != '') {
        var categoriesNames = $stateParams.categories.split(',');
        vm.selectedCategories = lodash.map(categoriesNames, function (categoryName) {
          return {
            name: categoryName
          }
        });
      } else if (angular.isDefined($rootScope.categories)) {
        vm.selectedCategories = $rootScope.categories;
      }

      // Setup ad spots to search
      setupFilters();
      AccountService.getAdvertiser(function (response) {
        vm.advertiserCampaigns = response.data.campaigns;
        search();
      }, function () {
        $log.error('Error retrieving user\'s info');
        toastr.error('There was an error retrieving your dashboard\'s info.')
      });
    }

    function isDataSelected() {
      return vm.selectedCategories.length > 0;
    }

    function showWarning() {
      var confirm = $mdDialog.alert()
        .title('Warning!')
        .htmlContent("<p><b> You cannot search until you have one CAMPAIGN approved by admin!</b></p> </br>" +
          "<p> Click 'Create Campaign' to submit your first URL for approval. </p>")
        .ok('I Understand');

      $mdDialog.show(confirm).then(function (result) {
        RoutesService.dashboard();
      });
    }

    function setupTimer() {
      $timeout.cancel(delay);
      delay = $timeout(executeQuery, TIME_DELAY_MS);
    }

    function executeQuery() {
      vm.firstPage = true;
      vm.filters.text = vm.input;
      setupFilters();
      search();
    }

    function queryEnter(event) {
      if (event.keyCode == KEY_CODES.ENTER) {
        vm.filters.text = vm.input;
        setupTimer();
      }
    }

    function searchInput() {
      if (angular.isDefined(vm.input)) {
        vm.filters.text = vm.input;
      }
      setupTimer();
    }

    function setupFilters() {
      vm.filters = {
        domain_authority_start: vm.domain_authority.min,
        domain_authority_end: vm.domain_authority.max,
        trust_flow_start: vm.trust_flow.min,
        trust_flow_end: vm.trust_flow.max,
        citation_flow_start: vm.citation_flow.min,
        citation_flow_end: vm.citation_flow.max,
        price_start: vm.price_range.min,
        price_end: vm.price_range.max,
        text: vm.input,
        auto_approved: vm.auto_approved,
        sort_by: vm.filter.value,
        page: vm.currentPage
      };
    }

    function categoriesChanged() {
      //TODO: Trigger when categories are changed to be completed.
    }

    function sortBy() {
      vm.firstPage = true;
      vm.filters.page = 1;
      vm.filters.sort_by = vm.filter.value;
      if (isDataSelected()) {
        search();
      }
    }

    function retrieveUrls(newPage) {
      vm.filters.page = newPage;
      search();
    }

    function search() {
      if (vm.advertiserCampaigns.length > 0) {
        let names = vm.selectedCategories.filter(function (item) {return !item.byKeywords || !item.activeKey});
        let keywords = vm.selectedCategories.filter(function (item) {return item.byKeywords && item.activeKey});
        SearchService.indexUrls(vm.filters, lodash.map(names, 'name'), lodash.map(keywords, 'activeKey'), function (response) {
          var data = response.data;
          vm.urls = data.urls;
          vm.showCategories = false;
          if (vm.firstPage) {
            vm.currentPage = 1;
            vm.firstPage = false;
          }
          vm.totalPages = data.pages;
          vm.totalItems = data.results > 0 ? data.results : 0;
          vm.filters.auto_approved = data.auto_approved
          $anchorScroll('top');
          vm.searchingCount++;
        }, function (response) {
          if (response.status === parseInt(HTTP_STATUS.FORBIDDEN)) {
            showWarning();
          } else {
            $log.error('Error executing query.');
            toastr.error('There was an error executing the query, please retry');
          }
          $anchorScroll('top');
        });
      } else {
        showWarning();
      }
    }

    function getPlaceholder() {
      return (vm.selectedCategories.length > 0) ? ' ' : 'Start typing. Enter button creates string. Categories will also be suggested.';
    }
  }
})();
