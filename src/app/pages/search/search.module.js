(function () {
  'use strict';

  angular
    .module('linktub.pages.search')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('search', {
        parent: 'dashboard',
        url: '/search/:categories/:campaignId',
        templateUrl: 'app/pages/search/search.html',
        controller: 'SearchController',
        controllerAs: 'srchCtrl',
        authentication: {
          required: true,
          permittedRoles: ['advertiser']
        }
      });
  }
})();
