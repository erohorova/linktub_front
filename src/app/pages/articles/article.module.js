(function () {
  'use strict';

  angular
    .module('linktub.pages.articles', [
      'linktub.pages.articles.done',
      'linktub.pages.articles.pipeline'
    ])
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('done-articles', {
        parent: 'dashboard',
        url: '/articles/done',
        templateUrl: 'app/pages/articles/done/articlesDone.html',
        controller: 'ArticlesDoneController',
        controllerAs: 'aDoneCtrl',
        authentication: {
          required: true,
          permittedRoles: ['advertiser']
        }
      })
      .state('pipeline-articles', {
        parent: 'dashboard',
        url: '/articles/pipeline',
        templateUrl: 'app/pages/articles/pipeline/articlesPipeline.html',
        controller: 'ArticlesPipelineController',
        controllerAs: 'aPipeCtrl',
        authentication: {
          required: true,
          permittedRoles: ['advertiser']
        }
      });
  }
})();
