(function () {
  'use strict';

  angular
    .module('linktub.pages.articles.done')
    .controller('NotesArticlesController', NotesArticlesController);

  /* @ngInject */
  function NotesArticlesController($scope, $mdDialog, lodash, article, addNotes) {
    var vm = this;

    //Accesible variables
    vm.notes = [];
    vm.article = {};
    vm.noteAdv;

    //Accesible functions
    vm.ok = ok;
    vm.cancel = cancel;
    vm.title = title;
    vm.buttonText = buttonText;

    initialize();

    function initialize() {
      vm.article = article;
      vm.notes = article.notes;
      vm.addNotes = addNotes;
    }

    function title() {
      return vm.addNotes ? 'Rewrite: Tell Author What Changes are Needed' : 'Notes';
    }

    function buttonText() {
      return vm.addNotes ? 'Cancel' : 'Ok';
    }

    function ok() {
      var response = {
        article: vm.article,
        note: vm.noteAdv
      };
      $mdDialog.hide(response);
    }

    function cancel() {
      $mdDialog.hide();
    }
  }
})();
