(function () {
  'use strict';

  angular
    .module('linktub.pages.articles.done', [])
    .controller('ArticlesDoneController', ArticlesDoneController);

  /* @ngInject */
  function ArticlesDoneController($scope, $log, $rootScope, $mdEditDialog, $mdDialog,
    lodash, toastr, ArticlesService, OrdersService, ARTICLE_STATES, REGEX, ORDER_CREATED_ORIGIN) {
    var vm = this;

    //Accesible Variables
    vm.articles = [];
    vm.filteredArticles = [];
    vm.selectedArticles = [];
    vm.queryArticles = {
      filter: '',
      order: 'state',
      limit: 20,
      page: 1
    };
    vm.search;

    //Accesible functions
    vm.isRejected = isRejected;
    vm.isApproved = isApproved;
    vm.isPending = isPending;
    vm.toggle = toggle;
    vm.removeFilter = removeFilter;
    vm.updateWord = updateWord;
    vm.showNotes = showNotes;
    vm.checkout = checkout;
    vm.instructions = instructions;
    vm.ok = ok;
    vm.setArticleHoverView = setArticleHoverView;
    vm.setArticleHoverUpload = setArticleHoverUpload;
    vm.setArticleHoverDownload = setArticleHoverDownload;
    vm.setArticleHoverNotes = setArticleHoverNotes;
    vm.setArticleHoverRewrite = setArticleHoverRewrite;
    vm.setArticlePendingHoverView = setArticlePendingHoverView;
    vm.setArticlePendingHoverUpload = setArticlePendingHoverUpload;
    vm.setArticlePendingHoverDownload = setArticlePendingHoverDownload;
    vm.setArticlePendingHoverNotes = setArticlePendingHoverNotes;
    vm.setArticlePendingHoverRewrite = setArticlePendingHoverRewrite;

    initialize();

    function initialize() {
      vm.search = false;
      ArticlesService.indexArticles(function (response) {
        vm.articles = response.data.articles;
        vm.zip = response.data.zip;
        lodash.map(vm.articles, function (item) {
          item.checked = false;
        });
        vm.filteredArticles = vm.articles;
      }, function (response) {
        toastr.error('There was an error retrieving articles. Please try again.');
      });
    }

    function isApproved(article) {
      return lodash.includes([ARTICLE_STATES.APPROVED], article.state);
    }

    function isRejected(article) {
      return lodash.includes([ARTICLE_STATES.ADMIN_REJECTED, ARTICLE_STATES.AUTHOR_REJECTED, ARTICLE_STATES.AGENCY_REJECTED], article.state);
    }

    function isPending(article) {
      return article.state == ARTICLE_STATES.DRAFT;
    }

    $scope.editText = function (event, article, anchor) {
      event.stopPropagation(); // in case autoselect is enabled
      if (!isRejected(article) && !isPending(article)) {
        var editDialog = {
          modelValue: anchor ? article.anchor : article.title,
          placeholder: anchor ? 'Anchor' : 'Title',
          save: function (input) {
            if (anchor) {
              article.anchor = input.$modelValue;
            } else {
              article.title = input.$modelValue;
            }
            updateField(article);
          },
          targetEvent: event,
          title: anchor ? 'Anchor' : 'Title'
        };
        var promise;
        promise = $mdEditDialog.large(editDialog).then(function (ctrl) {});
      }
    };

    $scope.editURL = function (event, article) {
      event.stopPropagation(); // in case autoselect is enabled
      if (!isRejected(article) && !isPending(article)) {
        var editDialog = {
          modelValue: article.destination_url,
          placeholder: 'http://destination.com',
          save: function (input) {
            article.destination_url = input.$modelValue.replace(/ /g, '');
            updateField(article);
          },
          targetEvent: event,
          title: 'Destination URL'
        };
        var promise;
        promise = $mdEditDialog.large(editDialog).then(function (ctrl) {
          var input = ctrl.getInput();
          input.$viewChangeListeners.push(function () {
            var expression = REGEX.URL;
            var regex = new RegExp(expression);
            var reg = regex.test(input.$modelValue);
            input.$setValidity('url', reg);
          });
        });
      }
    }

    function toggle(article) {
      article.checked = !article.checked;
      var pos = lodash.findIndex(vm.selectedArticles, ['id', article.id]);
      (pos < 0) ? vm.selectedArticles.push(article): vm.selectedArticles.splice(pos, 1);
    }

    function removeFilter() {
      vm.search = false;
      vm.queryArticles.filter = '';
    }

    $scope.$watch('aDoneCtrl.queryArticles.filter', function (newValue, oldValue) {
      var bookmark = '';

      if (!oldValue) {
        bookmark = vm.queryArticles.page;
      }

      if (newValue !== oldValue) {
        vm.queryArticles.page = 1;
      }

      if (!newValue) {
        vm.queryArticles.page = bookmark;
      }

      if (newValue == '') {
        vm.queryArticles.page = 1;
      }
    });

    function downloadContent(article) {
      return article.blog_post_doc;
    }

    function updateField(article) {
      ArticlesService.updateArticle(article.id, article, function (response) {
        toastr.success('Article was updated successfully!');
        initialize();
      }, function (response) {
        toastr.error('There was an error updating the article. Please try again.');
      });
    }

    function updateWord(article) {
      updateField(article);
    }

    function showNotes(article, add) {
      $mdDialog.show({
        controller: 'NotesArticlesController',
        controllerAs: 'noteCtrl',
        templateUrl: 'app/pages/articles/done/partials/notes.html',
        clickOutsideToClose: false,
        escapeToClose: true,
        focusOnOpen: true,
        locals: {
          article: article,
          addNotes: add
        }
      }).then(function (response) {
        if (response) {
          ArticlesService.createNote(response.article.id, response.note, add, function (successResponse) {
            toastr.success('Note was saved successfully!');
            initialize();
          }, function (errorResponse) {
            toastr.error('There was an error saving note. Try again.');
          });
        }
      });
    }

    function checkout() {
      ArticlesService.createOrders(lodash.map(vm.selectedArticles, 'id'), ORDER_CREATED_ORIGIN.ARTICLE, function () {
        toastr.success('This article has been added to your cart.');
        OrdersService.indexOrders(function (response) {
          $log.log('Successfully added order to cart');
        }, function (error) {
          $log.error('Error adding article to cart counter');
        });
        initialize();
      }, function () {
        toastr.error('There was an error adding these articles to your cart, please retry.');
      });
    }

    function instructions() {
      $mdDialog.show({
        controller: 'ArticlesDoneController',
        controllerAs: 'aDoneCtrl',
        templateUrl: 'app/pages/articles/done/partials/instructions.html',
        clickOutsideToClose: false,
        escapeToClose: true,
        focusOnOpen: false
      })
    }

    function ok() {
      $mdDialog.hide();
    }

    function setArticleHoverView(article, show) {
      article.hoverView = show;
    }

    function setArticleHoverUpload(article, show) {
      article.hoverUpload = show;
    }

    function setArticleHoverDownload(article, show) {
      article.hoverDownload = show;
    }

    function setArticleHoverNotes(article, show) {
      article.hoverNotes = show;
    }

    function setArticleHoverRewrite(article, show) {
      article.hoverRewrite = show;
    }

    function setArticlePendingHoverView(article, show) {
      article.pendingHoverView = show;
    }

    function setArticlePendingHoverUpload(article, show) {
      article.pendingHoverUpload = show;
    }

    function setArticlePendingHoverDownload(article, show) {
      article.pendingHoverDownload = show;
    }

    function setArticlePendingHoverNotes(article, show) {
      article.pendingHoverNotes = show;
    }

    function setArticlePendingHoverRewrite(article, show) {
      article.pendingHoverRewrite = show;
    }
  }
})();
