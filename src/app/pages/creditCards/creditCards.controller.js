(function () {
  'use strict';

  angular
    .module('linktub.pages.creditCards', [])
    .controller('CreditCardController', CreditCardController);

  /* @ngInject */
  function CreditCardController($mdDialog, $log, $mdMedia, $state, $stateParams, $scope, $rootScope,
    toastr, lodash, StatesService, CountriesService, StorageService, CreditCardService,
    RoutesService, PaymentsService, ERROR_CODES) {

    var vm = this;

    //Accesible variables
    vm.ccType = { logo: '', network: '' };
    vm.country = 'United States of America';
    vm.perPage = 1;
    vm.filtered = [];
    vm.expDate = {
      month: '',
      year: ''
    };
    vm.zipMask;
    vm.defaultCvvMask;
    vm.id;
    vm.token;
    vm.ccInfo;
    vm.ccNumberMask;

    //Accesible functions
    vm.toggleZipMask = toggleZipMask;
    vm.toggleCvvMask = toggleCvvMask;
    vm.toggleCreditCardMask = toggleCreditCardMask;
    vm.submit = submit;
    vm.$mdMedia = $mdMedia;
    vm.checkExpirationDate = checkExpirationDate;
    vm.headerMessage = headerMessage;
    vm.edit = edit;
    vm.showInfo = showInfo;

    initialize();

    function initialize() {
      if ($stateParams.cardId) {
        vm.id = $stateParams.cardId;
        CreditCardService.getCreditCard(vm.id, function(response) {
          vm.ccInfo = response.data;
          setCCInfo();
        }, function() {
          toastr.error('There was an error retrieving credit card information. Try again');
        })
      } else {
        vm.ccType = null;
      }

      vm.months = lodash.range(1, 13);
      vm.years = lodash.range(moment().year(), moment().year() + 20);
      vm.states = StatesService.getAllUSStates();
      vm.countries = CountriesService.getAllCountries();

      // Watch on change of credit card type to setup the mask
      $scope.$watch('vm.ccType', function () {
        if (vm.ccType) {
          switch (vm.ccType.digits) {
          case 14:
            vm.ccNumberMask = '9999 999999 9999';
            break;
          case 15:
            vm.ccNumberMask = '9999 999999 99999';
            vm.defaultCvvMask = '9999'
            break;
          case 16:
            vm.ccNumberMask = '9999 9999 9999 9999';
            vm.defaultCvvMask = '999'
            break;
          }
        }
      });
    }

    function setCCInfo() {
      vm.expDate.month = vm.ccInfo.month;
      vm.expDate.year = vm.ccInfo.year;
      vm.address = vm.ccInfo.street_address;
      vm.address2 = vm.ccInfo.extended_address;
      vm.firstName = vm.ccInfo.cardholder_name.split(/ +/)[0];
      vm.lastName = vm.ccInfo.cardholder_name.split(/ +/)[1];
      vm.city = vm.ccInfo.city;
      vm.state = vm.ccInfo.state;
      vm.zipCode = vm.ccInfo.zip_code;
      vm.country = vm.ccInfo.country_name;
      vm.token = vm.ccInfo.token;
      vm.ccNumber = vm.ccInfo.masked_number;
      vm.ccType.logo = vm.ccInfo.image_url;
      vm.ccType.network = vm.ccInfo.card_type;
    }

    function toggleZipMask() {
      if (!vm.zipMask) {
        vm.zipMask = '99999';
      } else if (!vm.zipCode) {
        vm.zipMask = '';
      }
    }

    function toggleCreditCardMask() {
      if (!vm.ccNumberMask) {
        vm.ccNumberMask = '9999 9999 9999 9999';
      } else if (!vm.ccNumber) {
        vm.ccNumberMask = '';
      }
    }

    function toggleCvvMask() {
      if (!vm.cvvMask || vm.cvvNumber) {
        vm.cvvMask = vm.defaultCvvMask || '999?9';
      } else if (!vm.cvvNumber) {
        vm.cvvMask = '';
      }
    }

    function showInfo() {
      $mdDialog.show(
        $mdDialog.alert()
        .clickOutsideToClose(true)
        .title('CVV number')
        .textContent("The CVV Number (\"Card Verification Value\") on your credit card or debit card is a 3 digit number on VISA®, MasterCard® and Discover® branded credit and debit cards. On your American Express® branded credit or debit card it is a 4 digit numeric code.")
        .ariaLabel('Notes')
        .ok('Got it!')
      );
    }

    function checkExpirationDate() {
      if (vm.expDate.month && vm.expDate.year) {
        var toDay = new Date(moment().year() + '-' + (moment().month() + 1));
        var date = new Date(vm.expDate.year + '-' + vm.expDate.month);
        $scope.paymentForm.expDateMonth.$setValidity('past', date >= toDay);
      }
    }

    function headerMessage() {
      return vm.edit() ? 'Edit Credit Card' : 'Add New Credit Card';
    }

    function submit() {
      PaymentsService.getPaymentToken(function (response) {
        var creditCard = {
          cardholderName: vm.firstName + ' ' + vm.lastName,
          expirationMonth: vm.expDate.month,
          expirationYear: vm.expDate.year,
          billingAddress: {
            postalCode: vm.zipCode,
            firstName: vm.firstName,
            lastName: vm.lastName,
            streetAddress: vm.address,
            countryName: vm.country,
            locality: vm.city,
            region: vm.state
          }
        }

        if(!vm.edit()) {
          creditCard.number = vm.ccNumber;
          creditCard.cvv = vm.cvvNumber;
        }
        if (angular.isDefined(vm.address2)) {
          creditCard.billingAddress.extendedAddress = vm.address2;
        }
        sendToken(response.data.token, creditCard);
      }, function () {
        $log.error('There was an error getting token.');
      });
    }

    function sendToken(token, cardInfo) {
      $rootScope.isLoading = true;
      if (token == null) {
        toastr.error('There was an error creating a token');
        $log.error('Token is null');
        return;
      }
      var client = new braintree.api.Client({
        clientToken: token
      });

      if (client) {
        client.tokenizeCard(cardInfo, function (err, nonce) {
          if (err) {
            $log.error(err);
            toastr.error(err);
            return;
          }
          (vm.token && vm.id) ? updateCreditCard(vm.id, nonce, vm.token) : createCreditCard(nonce);
        });
      } else {
        $log.error('There was an error contacting the payment gateway.');
        toastr.error('There was an error connecting the payment gateway. Please try againg in a few minutes.');
      }
    }

    function createCreditCard(nonce) {
      CreditCardService.createCreditCard(nonce, function() {
        RoutesService.profile();
        toastr.success('Successfully added new Credit Card');
      }, function(response) {
        displayErrors(response);
      });
    }

    function updateCreditCard(id, nonce, token) {
      CreditCardService.updateCreditCard(id, nonce, token, function() {
        RoutesService.profile();
        toastr.success('Successfully updated the Credit Card information');
      }, function(response) {
        displayErrors(response);
      });
    }

    function displayErrors(response) {
      if (lodash.includes(ERROR_CODES, response.data.errors[0].code)) {
        showDialogMessage('Your Credit Card number is invalid',
          'This means you have entered an incorrect 16 digit number. ' +
          'Please retype the entire <b>Credit Card number</b>.');
      } else {
        showDialogMessage('Please check some information',
          'Please double check your <b>Billing Zip Code</b>, <b>CVV</b>, ' +
          'and <b>Expiration Date</b>.');
      }
    }

    function showDialogMessage(title, message) {
      $mdDialog.show(
        $mdDialog.alert()
        .clickOutsideToClose(true)
        .title(title)
        .htmlContent(message)
        .ok('Got it!')
      );
    }

    function edit() {
      return angular.isDefined(vm.id);
    }

    function showInfo() {
      $mdDialog.show(
        $mdDialog.alert()
        .clickOutsideToClose(true)
        .title('CVV number')
        .textContent('If you have been issued a new CVV number, then you need to enter an entirely new CC record. PCI compliance does not allow us to edit CVV for existing cards.')
        .ariaLabel('Notes')
        .ok('Got it!')
      );
    }
  }
})();
