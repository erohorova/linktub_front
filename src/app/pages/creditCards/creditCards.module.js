(function () {
  'use strict';

  angular
    .module('linktub.pages.creditCards')
      .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('credit-card', {
        parent: 'dashboard',
        url: '/user/profile/creditCard/:cardId',
        templateUrl: 'app/pages/creditCards/creditCards.html',
        controller: 'CreditCardController',
        controllerAs: 'vm',
        authentication: {
          required: true,
          permittedRoles: ['advertiser', 'publisher']
        }
      });
  }
})();
