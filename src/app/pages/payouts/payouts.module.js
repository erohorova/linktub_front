(function () {
  'use strict';

  angular
    .module('linktub.pages.payouts')
    .config(routesConfig);

  /** @ngInject */
  function routesConfig($stateProvider) {
    $stateProvider
      .state('payouts', {
        parent: 'dashboard',
        url: '/publishers/payouts',
        templateUrl: 'app/pages/payouts/payouts.html',
        controller: 'PayoutController',
        controllerAs: 'payCtrl',
        authentication: {
          required: true,
          permittedRoles: ['publisher']
        }
      });
  }
})();
