(function () {
  'use strict';

  angular
    .module('linktub.pages.payouts', [])
    .controller('PayoutController', PayoutController);

  /* @ngInject */
  function PayoutController($rootScope, toastr, PayoutService, INIT_DATE, PAGINATION_SIZE) {
    var vm = this;

    // Accessible attributes
    vm.total_earnings = '0';
    vm.projected_earnings = '0';
    vm.last_month_payouts = '0';
    vm.items = [];
    vm.query = {
      filter: '',
      order: 'domain',
      limit: 10,
      page: 1
    };
    vm.perPage = PAGINATION_SIZE.PER_PAGE;
    vm.maxSize = PAGINATION_SIZE.MAX_SIZE;

    // Accessible functions
    vm.getPayouts = getPayouts;
    vm.setChangesToDates = setChangesToDates;
    vm.hasItems = hasItems;

    initialize();

    function initialize() {
      initDates();
      getPayouts(vm.date_from, vm.date_to)
    }

    function setChangesToDates() {
      vm.maxDateFrom = vm.date_to;
      vm.minDateTo = vm.date_from;
      vm.totalDays = calculateDifference();
      vm.cannotSearch = false;
    }

    function initDates() {
      vm.myDate = moment().format('MM/DD/YYYY');
      vm.date_to = moment().format('MM/DD/YYYY');
      vm.minDateFrom = moment(INIT_DATE).format('MM/DD/YYYY');
      vm.date_from = moment().subtract(30, 'days').format('MM/DD/YYYY');

      vm.maxDateFrom = vm.date_to;
      vm.minDateTo = vm.date_from;
      vm.maxDateTo = vm.date_to;
      vm.totalDays = calculateDifference();
    }

    function calculateDifference() {
      var date_from = moment(vm.date_from, 'MM/DD/YYYY');
      var date_to = moment(vm.date_to, 'MM/DD/YYYY');
      return date_from.diff(date_to, 'days');
    }

    function getPayouts(date_from, date_to) {
      var date_from_obj = moment(date_from, 'MM/DD/YYYY').toDate();
      var date_to_obj = moment(date_to, 'MM/DD/YYYY').toDate();

      vm.cannotSearch = true;
      $rootScope.isLoading = true;
      PayoutService.indexPayoutItems(date_from_obj, date_to_obj, function (response) {
        var data = response.data;
        vm.total_earnings = data.total_earnings;
        vm.projected_earnings = data.projected_earnings;
        vm.last_month_payouts = data.last_month_payouts;
        vm.items = data.items;
      });
    }

    function hasItems() {
      return vm.items.length > 0;
    }
  }
})();
