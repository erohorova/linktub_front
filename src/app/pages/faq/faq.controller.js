(function () {
  'use strict';

  angular
    .module('linktub.pages.faq', [])
    .controller('FaqController', FaqController);

  /* @ngInject */
  function FaqController(SessionService) {
    var vm = this;

    //attributes
    vm.role = '';

    //functions

    initialize();

    function initialize() {
      vm.role = SessionService.currentRole();
    }
  }
})();
