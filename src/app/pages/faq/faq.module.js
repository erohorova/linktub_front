(function () {
  'use strict';

  angular
    .module('linktub.pages.faq')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('faq-pub', {
        parent: 'dashboard',
        url: '/publishers/faq',
        templateUrl: 'app/pages/faq/partials/faqPub.html',
        controller: 'FaqController',
        controllerAs: 'faqCtrl',
        authentication: {
          required: true,
          permittedRoles: ['publisher']
        }
      })
      .state('faq-adv', {
        parent: 'dashboard',
        url: '/advertisers/faq',
        templateUrl: 'app/pages/faq/partials/faqAdv.html',
        controller: 'FaqController',
        controllerAs: 'faqCtrl',
        authentication: {
          required: true,
          permittedRoles: ['advertiser']
        }
      });
  }
})();
