(function () {
  'use strict';

  angular
    .module('linktub.pages.campaigns.campaign.new')
    .controller('ModalCampaignController', ModalCampaignController);

  /** @ngInject */
  function ModalCampaignController($state, $log, $mdDialog) {
    var vm = this;

    //Accessible functions
    vm.submit = submit;
    vm.clearCsv = clearCsv;

    function submit() {
      $mdDialog.hide(vm.campaign.csv);
    }

    function clearCsv() {
      vm.campaign.csv = null;
    }
  }
})();
