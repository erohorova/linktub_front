(function () {
  'use strict';

  angular
    .module('linktub.pages.campaigns.all', [])
    .controller('AllCampaignsController', AllCampaignsController);

  /** @ngInject */
  function AllCampaignsController($log, $mdDialog, $anchorScroll, lodash, toastr, CampaignsService) {
    var vm = this;

    // Accessible attributes
    vm.perPage = 5;
    vm.campaigns = [];

    // Accessible functions
    vm.deleteCampaign = deleteCampaign;
    vm.goTop = goTop;

    initialize();

    // Functions
    function initialize() {
      CampaignsService.indexCampaigns(function (response) {
        vm.campaigns = response.data.campaigns;
      }, function (response) {
        toastr.error('There was an error retrieving campaigns');
        $log.error(response);
      })
    }

    function deleteCampaign(campaignId) {
      var confirm = $mdDialog.confirm()
        .title('Would you like to delete this campaign?')
        .textContent('This campaign will be removed permanently.')
        .ariaLabel('Removing campaign')
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function () {
        CampaignsService.deleteCampaign(campaignId, function () {
          lodash.remove(vm.campaigns, function (campaign) {
            return campaign.id == campaignId;
          });
          toastr.success('Campaign was removed successfuly.');
        }, function () {
          $log.error('Error deleting campaign.');
          toastr.error('There was an error deleting campaign.');
        });
      });
    }

    function goTop() {
      $anchorScroll('top');
    }
  }
})();
