(function () {
  'use strict';

  angular
    .module('linktub.pages.campaigns', [
      'linktub.pages.campaigns.all',
      'linktub.pages.campaigns.campaign.new',
      'linktub.pages.campaigns.campaign.edit',
      'linktub.pages.campaigns.site.new'
    ])
    .config(routesConfig);

  /** @ngInject */
  function routesConfig($stateProvider) {
    $stateProvider
      .state('all-campaigns', {
        parent: 'dashboard',
        url: '/campaigns/all',
        templateUrl: 'app/pages/campaigns/allCampaigns/allCampaigns.html',
        controller: 'AllCampaignsController',
        controllerAs: 'allCampCtrl',
        authentication: {
          required: true,
          permittedRoles: ['advertiser']
        }
      })
      .state('new-campaign', {
        parent: 'dashboard',
        url: '/campaigns/new',
        templateUrl: 'app/pages/campaigns/newCampaign/newCampaign.html',
        controller: 'NewCampaignController',
        controllerAs: 'newCampCtrl',
        authentication: {
          required: true,
          permittedRoles: ['advertiser']
        }
      })
      .state('new-site', {
        parent: 'dashboard',
        url: '/site/new/:campaignId',
        templateUrl: 'app/pages/campaigns/newSite/newSite.html',
        controller: 'NewSitePageController',
        controllerAs: 'newSteCtrl',
        authentication: {
          required: true,
          permittedRoles: ['advertiser']
        }
      })
      .state('edit-campaign', {
        parent: 'dashboard',
        url: '/campaigns/edit/:campaignId',
        templateUrl: 'app/pages/campaigns/editCampaign/editCampaign.html',
        controller: 'EditCampaignController',
        controllerAs: 'editCampCtrl',
        authentication: {
          required: true,
          permittedRoles: ['advertiser']
        }
      });
  }
})();
