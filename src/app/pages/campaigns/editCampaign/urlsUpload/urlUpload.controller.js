(function () {
  'use strict';

  angular
    .module('linktub.pages.campaigns.campaign.edit')
    .controller('UrlUploadController', UrlUploadController);

  /** @ngInject */
  function UrlUploadController($state, $log, $mdDialog, $rootScope, Assets) {
    var vm = this;

    //Accessible functions
    vm.submit = submit;
    vm.clearCsv = clearCsv;
    vm.download = download;
    vm.getFileRoute = getFileRoute;

    function submit() {
      $mdDialog.hide(vm.csv);
    }

    function clearCsv() {
      vm.csv = null;
    }

    function getFileRoute() {
      return Assets + 'example_file.csv';
    }

    function download() {
      $rootScope.sampleSpreadsheet = true;
    }
  }
})();
