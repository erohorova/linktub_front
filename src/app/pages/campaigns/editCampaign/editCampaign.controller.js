(function () {
  'use strict';

  angular
    .module('linktub.pages.campaigns.campaign.edit', [])
    .controller('EditCampaignController', EditCampaignController);

  /* @ngInject */
  function EditCampaignController($log, $stateParams, $mdDialog, $scope, $rootScope, $anchorScroll, lodash, toastr,
    RoutesService, CampaignsService, LinksService, PAGINATION_SIZE, REGEX) {
    var vm = this;

    //Accesible Variables
    vm.showCategories = false;
    vm.links = [];
    vm.query = {
      filter: '',
      order: '-href',
      limit: 20,
      page: 1
    };
    vm.perPage = PAGINATION_SIZE.PER_PAGE;
    vm.maxSize = PAGINATION_SIZE.MAX_SIZE;
    vm.csv;
    vm.spreadsheetError = false;
    $rootScope.sampleSpreadsheet = false;
    vm.hasChanged = false;
    vm.categories = [];
    vm.currentPage = 1;
    vm.totalPages = 0;
    vm.totalItems = 0;

    initialize();

    // Functions
    vm.save = save;
    vm.cancel = cancel;
    vm.destroy = destroy;
    vm.removeFilter = removeFilter;
    vm.addDomain = addDomain;
    vm.showXlsIcon = showXlsIcon;
    vm.showSearchIcon = showSearchIcon;
    vm.clearCsv = clearCsv;
    vm.showCsvDialog = showCsvDialog;
    vm.sendFileToAdmin = sendFileToAdmin;
    vm.saveHeaderChanges = saveHeaderChanges;
    vm.categoriesChanged = categoriesChanged;
    vm.saveCategories = saveCategories;
    vm.categoriesNames = categoriesNames;
    vm.deleteLink = deleteLink;
    vm.canOpenRow = canOpenRow;
    vm.goTop = goTop;
    vm.retrieveMoreLinks = retrieveMoreLinks;
    vm.getLinkUrls = getLinkUrls;
    vm.isReportNull = isReportNull;
    vm.linkNotRefounded = linkNotRefounded;
    vm.togglePanel = togglePanel;
    vm.showNoReportAlert = showNoReportAlert;

    function initialize() {
      getCampaignData(vm.currentPage);
    }

    function save() {
      if (vm.csv) {
        vm.spreadsheetError = true;
      } else {
        CampaignsService.updateCampaign(vm.campaign.id, vm.campaign, function () {
            toastr.success('Campaign successfuly updated.');
            RoutesService.dashboard();
          },
          function () {
            toastr.error('Error');
          });
      }
    }

    function cancel() {
      RoutesService.dashboard();
    }

    function destroy() {
      var confirm = $mdDialog.confirm()
        .title('Would you like to delete this campaign?')
        .textContent('This campaign will be removed permanently.')
        .ariaLabel('Removing campaign')
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function () {
        CampaignsService.deleteCampaign(vm.campaign.id, function () {
          toastr.success('Campaign was removed successfuly.');
          RoutesService.dashboard();
        }, function () {
          $log.error('Error deleting campaign.');
          toastr.error('There was an error deleting campaign.');
        });
      });
    }

    function removeFilter() {
      vm.search = false;
      vm.query.filter = '';
    }

    $scope.$watch('editCampCtrl.query.filter', function (newValue, oldValue) {
      var bookmark = '';

      if (!oldValue) {
        bookmark = vm.query.page;
      }

      if (newValue !== oldValue) {
        vm.query.page = 1;
      }

      if (!newValue) {
        vm.query.page = bookmark;
      }

      if (newValue == '') {
        vm.query.page = 1;
      }
    });

    function addDomain() {
      $mdDialog.show({
        templateUrl: 'app/pages/campaigns/editCampaign/newUrlModal/newUrlModal.html',
        controller: 'NewUrlModalController',
        controllerAs: 'newUrlCtrl',
        preserveScope: true,
        clickOutsideToClose: false,
        fullscreen: true,
        locals: {
          url: ''
        }
      }).then(function (url) {
        if (url) {
          CampaignsService.addUrl(vm.campaign.id, url, function () {
            CampaignsService.indexLinks(vm.campaign.id, function (response) {
              vm.links = response.data.links;
              toastr.success('Url was added successfuly.');
            }, function () {
              toastr.error('Cannot retrieve Urls. Please try again.');
            });
          }, function (response) {
            if (response.data.errors.href[0] === 'has already been taken') {
              return showAlertHTML('Duplicate Url', 'This URL is already in our DB.' +
                " Please click 'Contact Us' if you feel this is an error.", 'Got it!').then(function (result) {
                return false;
              });
            } else if (response.data.errors.href[0] === 'is an invalid URL') {
              toastr.error('Invalid URL. Cannot add Url. Please try again.');
            } else {
              toastr.error('Cannot add Url. Please try again.');
            }
          });
        }
      });
    }

    function categoriesChanged() {
      //TODO: Trigger when categories are changed to be completed.
    }

    function showXlsIcon() {
      return 'assets/images/xlsUpload.svg';
    }

    function showSearchIcon() {
      return 'assets/images/search_icon.svg';
    }

    function sendFileToAdmin() {
      CampaignsService.updateSpreadsheetCampaign(vm.campaign.id, vm.csv, function (response) {
        CampaignsService.indexLinks(vm.campaign.id, function (response) {
          showUploadedFileDialog();
          clearCsv();
          vm.links = response.data.links;
          toastr.success('Urls were added successfuly.');
        }, function () {
          toastr.error('Cannot retrieve Urls. Please try again.');
        });
      }, function (response) {
        var errors = response.data.errors;
        if (errors.href[0] === 'has already been taken') {
          return showAlertHTML('Duplicate Url', 'There are URL already in our DB.' +
            " Please click 'Contact Us' if you feel this is an error.", 'Got it!').then(function (result) {
            return false;
          });
        } else if (errors.href[0] === 'is an invalid URL') {
          toastr.error('Invalid URL. Cannot upload spreadsheet. Please try again.');
        } else {
          toastr.error('Cannot upload spreadsheet. Please try again.');
        }
      });
    }

    function showUploadedFileDialog() {
      return showAlertHTML('Spreadsheet uploaded', 'Thank you for uploading these domains. ' +
        'Our admin department will review them one at a time and you will receive notifications ' +
        'when they are either approved or rejected.', 'Got it!').then(function (result) {
        return false;
      });
    }

    function showAlertHTML(title, text, confirmText) {
      return $mdDialog.show(
        $mdDialog.alert()
        .title(title)
        .htmlContent(text)
        .escapeToClose(false)
        .ok(confirmText)
      );
    }

    function showCsvDialog() {
      $mdDialog.show({
        controller: 'UrlUploadController',
        controllerAs: 'urlUploadCtrl',
        templateUrl: 'app/pages/campaigns/editCampaign/urlsUpload/urlUpload.html',
        clickOutsideToClose: true
      }).then(function (uploadedFile) {
        if (angular.isDefined(uploadedFile)) {
          vm.csv = uploadedFile;
          sendFileToAdmin();
        }
      });
    }

    function clearCsv() {
      vm.csv = null;
    }

    function saveHeaderChanges(form) {
      CampaignsService.updateCampaignHeader(vm.campaign.id, {
        image: vm.campaign.image,
        name: vm.campaign.name
      }, function () {
        form.$setPristine();
        toastr.success('Campaign\'s name/image successfuly updated.');
      }, function () {
        toastr.error('There was an error updating this campaign, please retry.');
      });
    }

    function saveCategories() {
      vm.newCampaign = {
        categories_ids: lodash.map(vm.categories, 'id')
      }
      CampaignsService.updateCampaignCategories(vm.campaign.id, vm.newCampaign, function () {
        vm.hasChanged = false;
        toastr.success('Campaign\'s categories successfuly updated.');
      }, function () {
        toastr.error('There was an error updating this campaign\'s categories, please retry.');
      });
    }

    function categoriesNames() {
      return lodash.map(vm.categories, 'name');
    }

    function deleteLink(link) {
      var confirm = $mdDialog.confirm()
        .title('Are you sure?')
        .textContent("You will delete " + link.href)
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function (result) {
        LinksService.deleteLink(link.id, function () {
            lodash.remove(vm.links, function (campaignLinks) {
              return campaignLinks.id === link.id;
            });
            toastr.success('URL was removed successfuly.');
          },
          function () {
            $log.error('Error deleting url.');
            toastr.error('There was an error deleting URL.');
          });
      });
    }

    function canOpenRow(link) {
      return link.urls.length === 0;
    }

    function goTop() {
      $anchorScroll('top');
    }

    function retrieveMoreLinks(newPage) {
      vm.currentPage = newPage;
      getCampaignData(vm.currentPage);
    }

    function getCampaignData(page) {
      CampaignsService.showCampaign($stateParams.campaignId, page, function (response) {
          var data = response.data;
          vm.totalPages = data.pages;
          vm.totalItems = data.results;
          vm.campaign = data;
          vm.links = data.links;
          vm.categories = data.categories;
          CampaignsService.getCampaignNames(function (response) {
            vm.campaignNames = data.campaign_names;
            lodash.remove(vm.campaignNames, function (name) {
              return vm.campaign.name === name;
            });
          });
        },
        function () {
          $log.log('Error retrieving the campaign.');
          toastr.error('There was an error retrieving the campaign.');
        });
    }

    function getLinkUrls(mainLink) {
      var indexOfLink = lodash.findIndex(vm.links, function (link) {
        return link.id === mainLink.id;
      });
      var linksOrdered = vm.links[indexOfLink];
      if (angular.isUndefined(linksOrdered.orders_built)) {
        LinksService.getLinks(mainLink.id, function (response) {
          linksOrdered.orders_built = response.data.orders_built;
        });
      }
    }

    function isReportNull() {
      return !vm.campaign || !vm.campaign.campaign_report;
    }

    function linkNotRefounded(str) {
      var pattern = new RegExp(REGEX.URL);
      return pattern.test(str);
    }

    function togglePanel(link) {
      link.isOpen = !link.isOpen;
      if(link.isOpen) getLinkUrls(link);
    }

    function showNoReportAlert() {
      $mdDialog.show(
        $mdDialog.alert()
        .clickOutsideToClose(true)
        .title('No Report')
        .textContent("You have not purchased any content for this campaign. There is no report to generate.")
        .ariaLabel('No Report')
        .ok('Got it!')
      );
    }
  }
})();
