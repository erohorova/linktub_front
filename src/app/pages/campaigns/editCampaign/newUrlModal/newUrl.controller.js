(function () {
  'use strict';

  angular
    .module('linktub.pages.campaigns.campaign.edit')
    .controller('NewUrlModalController', NewUrlModalController);

  /** @ngInject */
  function NewUrlModalController($mdDialog, $rootScope, CampaignsService, MIN_VALUES,
    PUB_URL_STATE, PricesService, url) {

    var vm = this;
    vm.url = {};

    //functions
    vm.checkURL = checkURL;
    vm.submit = submit;
    vm.cancel = cancel;

    init();

    function init() {
      vm.title = 'Add New Domain';
    }

    function submit() {
      $mdDialog.hide(vm.url);
    }

    function checkURL() {
      //TODO: ask if DA & TF will be necessary to check
    }

    function cancel() {
      $mdDialog.cancel();
    }
  }
})();
