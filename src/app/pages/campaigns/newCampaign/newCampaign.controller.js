(function () {
  'use strict';

  angular
    .module('linktub.pages.campaigns.campaign.new', [])
    .controller('NewCampaignController', NewCampaignController);

  /** @ngInject */
  function NewCampaignController($log, $mdDialog, toastr, CampaignsService, RoutesService) {
    var vm = this;

    // Accessible attributes
    vm.campaign = {
      categories: []
    };
    vm.showCategories = false;

    // Accessible functions
    vm.submit = submit;
    vm.clearCsv = clearCsv;
    vm.showDialog = showDialog;
    vm.avatarUploaded = avatarUploaded;
    vm.checkURL = checkURL;
    vm.toggleCategories = toggleCategories;
    vm.toggleCategoriesActionMsg = toggleCategoriesActionMsg;

    initialize();

    // Functions

    function initialize() {
      CampaignsService.getCampaignNames(function (response) {
        vm.campaignNames = response.data.campaign_names;
      });
    }

    function checkURL(form) {
      var regex = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
      var valid = regex.test(vm.campaign.url);
      form.campaignUrl.$setValidity('url', valid);
    }

    function showDialog() {
      $mdDialog.show({
        controller: 'ModalCampaignController',
        controllerAs: 'modCampCtrl',
        templateUrl: 'app/pages/campaigns/modalCampaign/modalCampaign.html',
        clickOutsideToClose: true
      }).then(function (uploadedFile) {
        vm.campaign.csv = uploadedFile;
      });
    }

    function submit() {
      if (vm.campaign.csv) {
        delete vm.campaign.url;
      }
      CampaignsService.createCampaign(vm.campaign,
        function () {
          vm.campaign = {};
          toastr.success('Campaign successfully created!');
          RoutesService.dashboard();
        },
        function () {
          $log.error('Error submiting new campaign.');
          toastr.error('There was an error while submiting the new campaign. Please retry.');
        });
    }

    function clearCsv() {
      vm.campaign.csv = null;
    }

    function avatarUploaded() {
      return angular.isDefined(vm.campaign.image) && vm.campaign.image != null;
    }

    function toggleCategories() {
      vm.showCategories = !vm.showCategories;
    }

    function toggleCategoriesActionMsg() {
      return vm.showCategories ? 'Hide Categories' : 'Show Categories';
    }
  }
})();
