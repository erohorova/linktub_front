(function () {
  'use strict';

  angular
    .module('linktub.pages.billing', [])
    .controller('BillingController', BillingController);

  /* @ngInject */
  function BillingController($mdExpansionPanel, $mdExpansionPanelGroup, $scope,
    $rootScope, $anchorScroll, toastr, lodash, PAGINATION_SIZE, OrdersService, InvoiceService) {
    var vm = this;

    // Accessible variables
    vm.invoices = [];
    vm.total_invoices = 0;
    vm.query = {
      filter: '',
      order: '-url.domain_authority',
      limit: 10,
      page: 1
    };
    vm.perPage = PAGINATION_SIZE.PER_PAGE;
    vm.maxSize = PAGINATION_SIZE.MAX_SIZE;

    // Accessible functions
    vm.goTop = goTop;
    vm.paginateInvoices = paginateInvoices;

    initialize();

    function initialize() {
      fetchInvoices();
    }

    $scope.collapseOne = function () {
      $mdExpansionPanel('expansionPanelOne').collapse();
    };

    function goTop() {
      $anchorScroll('top');
    }

    function paginateInvoices(newPageNumber, oldPageNumber) {
      vm.query.page = newPageNumber;
      fetchInvoices();
      $anchorScroll('top');
    }

    function fetchInvoices() {
      InvoiceService.indexInvoice(vm.query, function (response) {
        vm.invoices = response.data.invoices;
        vm.total_invoices = response.data.results;
      }, function () {
        toastr.error('There was an error retrieving your billing info.');
      });

      $mdExpansionPanel().waitFor('expansionPanelOne').then(function (instance) {
        instance.expand();
      });
    }
  }
})();
