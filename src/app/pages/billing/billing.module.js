(function () {
  'use strict';

  angular
    .module('linktub.pages.billing')
    .config(routesConfig);

  /** @ngInject */
  function routesConfig($stateProvider) {
    $stateProvider
      .state('billing', {
        parent: 'dashboard',
        url: '/billing',
        templateUrl: 'app/pages/billing/billing.html',
        controller: 'BillingController',
        controllerAs: 'billCtrl',
        authentication: {
          required: true,
          permittedRoles: ['advertiser', 'publisher']
        }
      });
  }
})();
