(function () {
  'use strict';

  angular
    .module('linktub.pages.landing.signUp', [])
    .controller('SignUpController', SignUpController);

  /* @ngInject */
  function SignUpController($rootScope, $scope, $stateParams, $mdDialog, $mdMedia, $log, toastr, lodash, MIN_PASS_SIZE,
    PasswordMeter, UserService, AuthenticationService, AccountService, RoutesService) {
    var vm = this;
    vm.user = {};
    vm.password = '';
    vm.passEval;
    vm.passTextStrength = '';
    vm.advertiser = false;
    vm.publisher = false;
    vm.noTypeSelected = false;
    vm.oldEmail;
    vm.attemptEmail = false;
    vm.accountType = $stateParams.accountType;

    // Accessible Functions
    vm.submit = submit;
    vm.samePassword = samePassword;
    vm.canChangePassword = canChangePassword;
    vm.checkStrength = checkStrength;
    vm.displayCheckboxError = displayCheckboxError;
    vm.toggleOption = toggleOption;
    vm.checkEmail = checkEmail;
    vm.updateAttempt = updateAttempt;

    init();

    function init() {
      vm.user = {
        first_name: '',
        last_name: '',
        email: '',
        phone: '',
        password: '',
        password_confirmation: ''
      };
      vm.oldEmail = vm.user.email;
      if($stateParams.accountType === 'influencer') {
        vm.publisher = true;
        $rootScope.title = 'Influencers! Get Paid to Work | Scalefluence.com';
        $rootScope.metaDesc = 'Influencers and Bloggers Can Collaborate with Brands. Register today at Scalefluence.com!';
      }
      else {
        vm.advertiser = true;
        $rootScope.title = 'Influencer Marketing for Agencies and Brands | Scalefluence.com';
        $rootScope.metaDesc = 'An influencer marketing platform designed to give deep insight through over a dozen different data points. Marketers can collaborate with thousands of influencers with Scalefluence.com';
      }
    }

    function submit(ev) {
      vm.user.password = vm.password;
      UserService.signUp(vm.user, vm.advertiser, vm.publisher, function (response) {
        RoutesService.signIn();
        $mdDialog.show(
          $mdDialog.alert()
          .clickOutsideToClose(true)
          .title('Confirmation Link')
          .htmlContent(
            "Hey " + vm.user.first_name +
            ", please check your email now to click the confirmation link and sign in! <br><br> The Scalefluence Team")
          .ariaLabel('signup notification')
          .ok('Ok!')
          .targetEvent(ev)
        );
      }, function () {
        toastr.error('There was an error retrieving your dashboard\'s info.')
      });
    }

    function samePassword() {
      return angular.equals(vm.password, vm.user.password_confirmation);
    }

    function canChangePassword() {
      $scope.signUpForm.confirmPassword.$setValidity('no-match', samePassword());
    }

    function checkStrength() {
      vm.passEval = vm.password ? (vm.password.length > MIN_PASS_SIZE && PasswordMeter.evaluatePassword(vm.password) || 0) : null;
      if (vm.passEval == 4) return 'Strong';
      return vm.passEval in [0, 1] ? 'Weak' : 'Medium';
    }

    function signIn() {
      vm.credentials = {
        email: vm.user.email,
        password: vm.user.password
      };

      AuthenticationService.login(vm.credentials,
        function () {
          RoutesService.dashboard();
        },
        function (response) {
          var message = response.data.error || response.data.errors[0];
          if (lodash.includes(message, 'authentication')) {
            toastr.error('Incorrect email or password');
          } else if (lodash.includes(message, 'suspended')) {
            toastr.error(message);
          }
        });
    }

    function displayCheckboxError() {
      vm.noTypeSelected = !(vm.advertiser || vm.publisher);
    }

    function toggleOption(opt) {
      switch (opt) {
      case 'adv':
        vm.noTypeSelected = vm.advertiser && !vm.publisher;
        break;
      case 'pub':
        vm.noTypeSelected = vm.publisher && !vm.advertiser;
      }
    }

    function checkEmail() {
      vm.attemptEmail = true;
      if (vm.user.email !== vm.oldEmail) {
        vm.oldEmail = vm.user.email;
        $scope.signUpForm.email.$setValidity('in-use', true);
        UserService.checkDuplicatedEmail(vm.user.email, function (response) {
          var data = response.data;
          $scope.signUpForm.email.$setValidity('in-use', !data.email_exists);
        }, function (err) {
          $log.error('Error checking duplicated email');
        });
      }
    }

    function updateAttempt() {
      vm.attemptEmail = false;
    }
  }
})();
