(function () {
  'use strict';

  angular
    .module('linktub.pages.landing.signIn', [])
    .controller('SignInController', SignInController);

  /* @ngInject */
  function SignInController($mdDialog, $state, lodash, toastr, KEY_CODES, TOSTATE, AuthenticationService, RoutesService, StorageService) {
    var vm = this;

    // Accessible attributes
    vm.credentials = {};

    // Accessible Functions
    vm.submit = submit;
    vm.submitEnter = submitEnter;

    // Functions
    function submitEnter(event) {
      if (event.keyCode === KEY_CODES.ENTER) {
        submit();
      }
    }

    function submit() {
      AuthenticationService.login(vm.credentials,
        function () {
          var toState = StorageService.get(TOSTATE);
          if (toState) {
            StorageService.remove(TOSTATE);
            $state.go(toState.name);
          } else {
            RoutesService.dashboard();
          }
        },
        function (response) {
          var message = response.data.error || response.data.errors[0];
          if (lodash.includes(message, 'authentication')) {
            toastr.error('Incorrect email or password');
          } else {
            toastr.error(message);
          }
        });
    }
  }
})();
