(function () {
  'use strict';

  angular
    .module('linktub.pages.landing', [
      'linktub.pages.landing.signUp',
      'linktub.pages.landing.signIn'])
    .config(routesConfig);

  /** @ngInject */
  function routesConfig($stateProvider) {
    $stateProvider
      .state('sign-up', {
        url: '/signUp?accountType',
        templateUrl: 'app/pages/landing/signUp/signUp.html',
        controller: 'SignUpController',
        controllerAs: 'signUpCtrl',
        authentication: {
          required: false,
          permittedRoles: ['advertiser', 'publisher']
        }
      })
      .state('sign-in', {
        url: '/',
        templateUrl: 'app/pages/landing/signIn/signIn.html',
        controller: 'SignInController',
        controllerAs: 'signInCtrl',
        authentication: {
          required: false,
          permittedRoles: ['advertiser', 'publisher']
        }
      });
  }
})();
