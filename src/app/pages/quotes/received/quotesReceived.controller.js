(function () {
  'use strict';

  angular
    .module('linktub.pages.quotes.received', [])
    .controller('QuotesReceivedController', QuotesReceivedController);

  /* @ngInject */
  function QuotesReceivedController($state, $log, $rootScope, $mdMedia, toastr, ContentService) {
    var vm = this;

    // Accessible attributes
    vm.activeItem;
    vm.contents = [];
    vm.perPage = 20;
    vm.dirId = 'Rec';
    vm.$mdMedia = $mdMedia;

    // Accesible functions
    vm.showQuotesFrom = showQuotesFrom;
    vm.goTop = goTop;
    vm.getDoingContentText = getDoingContentText;

    initialize();

    // Functions
    function initialize() {
      ContentService.indexReceivedContents(function (response) {
        vm.contents = response.data.contents;
        $rootScope.quotesReady = vm.contents.length;
      }, function () {
        $log.error('Error retrieving received content');
        toastr.error('There was an error retrieving received content. Please try again.')
      });
    }

    function showQuotesFrom(item) {
      vm.activeItem = item;
      $state.go('quotes-for', {
        quoteId: item.id
      })
    }

    function goTop() {
      $anchorScroll('top');
    }

    function getDoingContentText(quote) {
      return quote.agency_content ? 'Agency is doing content' : 'Scalefluence is doing content';
    }
  }
})();
