(function () {
  'use strict';

  angular
    .module('linktub.pages.quotes.quoteFor', [])
    .controller('QuoteForController', QuoteForController);

  /* @ngInject */
  function QuoteForController($stateParams, $window, $interpolate, $log, $anchorScroll, $mdMedia, $mdDialog, toastr, lodash, ContentService,
    StorageService, ArticlesService, RoutesService, OrdersService, NotificationService, TYPE_STATE, ROLE, ORDER_CREATED_ORIGIN, PUB_URL_STATE) {
    var vm = this;

    // Accessible variables
    vm.urls = [];
    vm.quote = {};
    vm.perPage = 5;
    vm.$mdMedia = $mdMedia;
    vm.buttonText = '';

    // Accessible functions
    vm.toggle = toggle;
    vm.cannotSubmit = cannotSubmit;
    vm.setGreenBackground = setGreenBackground;
    vm.goTop = goTop;
    vm.submit = submit;
    vm.downloadReport = downloadReport;
    vm.upload = upload;
    vm.prepareForSubmit = prepareForSubmit;
    vm.urlNotPaused = urlNotPaused;

    initialize();

    // Functions
    function initialize() {
      if (angular.isDefined($stateParams.quoteId)) {
        ContentService.showContent($stateParams.quoteId, function (response) {
          vm.quote = response.data;
          vm.downloaded = StorageService.get('downloaded_' + vm.quote.campaign.name);
          vm.buttonText = setTextButton(vm.quote);
          if (vm.quote.urls.length > 0) {
            var aux = lodash.map(vm.quote.urls, function (url) {
              url.showMessage = false;
              url.checked = false;
              url.quote = {
                blog_post_title: '',
                href: '',
                link_text: '',
                notes: ''
              };
              url.campaign = response.data.campaign.id;
              url.url_id = url.id;
              url.content_id = vm.quote.id;

              return url;
            });
            vm.quote.urls = aux;
          } else {
            RoutesService.mainQuotes();
          }
        }, function () {
          $log.error('Error retrieving the content.');
          toastr.error('There was an error retrieving the content. Please retry.');
        });
      }
    }

    function toggle(url) {
      url.showMessage = url.checked && !vm.quote.agency_content;
      var pos = lodash.findIndex(vm.urls, ['id', url.id]);
      (pos < 0) ? vm.urls.push(url): vm.urls.splice(pos, 1);
    }

    function cannotSubmit() {
      if (vm.urls.length < 1) {
        return true;
      }
      var pos = lodash.findIndex(vm.urls, function (url) {
        return checkQuoteContent(url);
      });

      return pos != -1;
    }

    function checkQuoteContent(url) {
      if (vm.quote.agency_content) {
        return false;
      } else {
        if (url.quote.href && url.quote.link_text) {
          url.showMessage = false;
          return false;
        }

        if (url.checked) {
          url.showMessage = true;
        }
        return true;
      }
    }

    function setGreenBackground(url) {
      if (url) { 
        return url.checked && !url.showMessage;
      }
    }

    function goTop() {
      $anchorScroll('top');
    }

    function submit() {
      if (vm.quote.agency_content) {
        var url_ids = lodash.map(vm.urls, function (url)  {
          return url.id
        });
        OrdersService.createOrders(url_ids, vm.quote.id, ORDER_CREATED_ORIGIN.QUOTE, function () {
          toastr.success('Quotes successfully submitted');
          removeNotification();
          initialize();
          vm.urls = [];
        }, function () {
          $log.error('Error adding content to your cart.');
          toastr.error('There was an error adding this content to your cart, please retry.');
        });
      } else {
        ArticlesService.createArticles(vm.urls, function (response) {
          toastr.success('Quotes successfully submitted');
          removeNotification();
          initialize();
          vm.urls = [];
        }, function (response) {
          if (response.data.errors.href) {
            toastr.error('Target URL ' + response.data.errors.href[0]);
          } else {
            toastr.error('There was an error creating the Quotes. Please try again.');
          }
        });
      }
    }

    function setTextButton(quote) {
      return quote.agency_content ? 'Send to cart' : 'Submit to Our authors';
    }

    function removeNotification() {
      NotificationService.deleteNotification(TYPE_STATE.QUOTES, ROLE.ADV);
    }

    function downloadReport() {
      toastr.info('Quote downloading. Make sure your browser is not "blocking popups".');
      ContentService.downloadReport(vm.quote.id, function (response) {
        vm.downloaded = true;
        StorageService.set('downloaded_' + vm.quote.campaign.name, true);
        $window.open(response.data.urls_spreadsheet_doc.url, '_blank');
      }, function (err) {
        $log.error(err);
      });
    }

    function upload(campaignName) {
      var template = $interpolate(
        '<md-dialog class="file-content" aria-label="{{ ariaLabel }}">' +
        '<file-upload-dialog title="{{ title }}" file-ext=".xls, .xlsx"></file-upload-dialog></md-dialog>');
      $mdDialog.show({
        template: template({
          ariaLabel: 'Upload file',
          title: 'Upload Order for ' + campaignName
        }),
        autoWrap: false,
        clickOutsideToClose: false,
        escapeToClose: true,
        focusOnOpen: true
      }).then(function (file) {
        ContentService.uploadReport(vm.quote.id, file, function (response) {
          toastr.success('Successfully updated!');
          RoutesService.mainQuotes();
        }, function (err) {
          toastr.error(err.data.error);
          $log.error(err);
        });
      });
    }

    function prepareForSubmit(url) {
      if (!url.checked) {
        url.checked = true;
        toggle(url);
      }
    }

    function urlNotPaused(url) {
      return url.state !== PUB_URL_STATE.PAUSED;
    }
  }
})();
