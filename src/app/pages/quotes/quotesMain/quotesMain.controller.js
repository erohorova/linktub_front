(function () {
  'use strict';

  angular
    .module('linktub.pages.quotes.main', [])
    .controller('QuotesMainController', QuotesMainController);

  /* @ngInject */
  function QuotesMainController($rootScope) {
    var vm = this;

    // Accessible attributes
    // 'activeQuotes' or 'pendingQuotes'
    vm.quotesTab = 'activeQuotes';

    // Accessible functions
    vm.setQuotesTab = setQuotesTab;
    vm.activeQuotesOn = activeQuotesOn;

    function setQuotesTab(selTab) {
      vm.quotesTab = selTab === 'activeQuotes' ? 'activeQuotes' : 'pendingQuotes';
    }

    function activeQuotesOn() {
      return vm.quotesTab === 'activeQuotes';
    }
  }
})();
