(function () {
  'use strict';

  angular
    .module('linktub.pages.quotes', [
      'linktub.pages.quotes.pending',
      'linktub.pages.quotes.received',
      'linktub.pages.quotes.newQuote',
      'linktub.pages.quotes.quoteFor',
      'linktub.pages.quotes.main'
    ])
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('received-quotes', {
        parent: 'dashboard',
        url: '/quotes/received',
        templateUrl: 'app/pages/quotes/received/quotesReceived.html',
        controller: 'QuotesReceivedController',
        controllerAs: 'qRecCtrl',
        authentication: {
          required: true,
          permittedRoles: ['advertiser']
        }
      })
      .state('pending-quotes', {
        parent: 'dashboard',
        url: '/quotes/pending',
        templateUrl: 'app/pages/quotes/pending/quotesPending.html',
        controller: 'QuotesPendingController',
        controllerAs: 'qPenCtrl',
        authentication: {
          required: true,
          permittedRoles: ['advertiser']
        }
      })
      .state('new-quote', {
        parent: 'dashboard',
        url: '/quote/new',
        templateUrl: 'app/pages/quotes/newQuote/newQuote.html',
        controller: 'NewQuoteController',
        controllerAs: 'nQuoteCtrl',
        authentication: {
          required: true,
          permittedRoles: ['advertiser']
        }
      })
      .state('quotes-for', {
        parent: 'dashboard',
        url: '/quotes/for/:quoteId',
        templateUrl: 'app/pages/quotes/quoteFor/quoteFor.html',
        controller: 'QuoteForController',
        controllerAs: 'quoteForCtrl',
        authentication: {
          required: true,
          permittedRoles: ['advertiser']
        }
      })
      .state('quotes-main', {
        parent: 'dashboard',
        url: '/quotes',
        templateUrl: 'app/pages/quotes/quotesMain/quotesMain.html',
        controller: 'QuotesMainController',
        controllerAs: 'quotesMainCtrl',
        authentication: {
          required: true,
          permittedRoles: ['advertiser']
        }
      });
  }
})();
