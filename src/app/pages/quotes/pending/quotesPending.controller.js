(function () {
  'use strict';

  angular
    .module('linktub.pages.quotes.pending', [])
    .controller('QuotesPendingController', QuotesPendingController);

  /* @ngInject */
  function QuotesPendingController($log, $rootScope, toastr, ContentService) {
    var vm = this;

    // Accessible attributes
    vm.contents = [];
    vm.activeItem;
    vm.perPage = 20;
    vm.dirId = 'Pending';

    // Accesible functions
    vm.showQuotesFrom = showQuotesFrom;
    vm.goTop = goTop;
    vm.getDoingContentText = getDoingContentText;
    vm.toggleQuote = toggleQuote;
    vm.hideQuote = hideQuote;

    initialize();

    // Functions
    function initialize() {
      ContentService.indexPendingContents(function (response) {
        vm.contents = response.data.contents;
        $rootScope.quotesPending = vm.contents.length
      }, function () {
        $log.error('Error retrieving received content.');
        toastr.error('There was an error retrieving received content. Please try again.')
      });
    }

    function showQuotesFrom(item) {
      vm.activeItem = item;
    }

    function goTop() {
      $anchorScroll('top');
    }

    function getDoingContentText(quote) {
      return quote.agency_content ? 'Agency is doing content' : 'Scalefluence is doing content';
    }

    function toggleQuote(quote) {
      quote.show = !quote.show;
    }

    function hideQuote(quote) {
      quote.show = false;
    }
  }
})();
