(function () {
  'use strict';

  angular
    .module('linktub.pages.quotes.newQuote', [])
    .controller('NewQuoteController', NewQuoteController);

  /* @ngInject */
  function NewQuoteController($state, $log, $mdDialog, toastr, CampaignsService, ContentService, AUTHOR_QUOTE) {
    var vm = this;

    // Accessible attributes
    vm.categories = [];
    vm.campaigns = [];
    vm.content = {
      campaign: null,
      categories: [],
      minTF: 10,
      maxTF: 100,
      minDA: 10,
      maxDA: 100,
    };
    vm.da_range = {
      options: {
        floor: 10,
        ceil: 100,
        step: 10,
        noSwitching: true,
        hideLimitLabels: true,
        hidePointerLabels: true,
        showTicksValues: true
      }
    };
    vm.tf_range = {
      options: {
        floor: 10,
        ceil: 100,
        step: 10,
        noSwitching: true,
        hideLimitLabels: true,
        hidePointerLabels: true,
        showTicksValues: true
      }
    };

    // Accessible functions
    vm.submit = submit;
    vm.getCampaign = getCampaign;
    vm.showInfo = showInfo;
    vm.showAuthorErrorMessage = showAuthorErrorMessage;
    vm.showAuthorContent = showAuthorContent;
    vm.setAuthorAgency = setAuthorAgency;
    vm.setAuthorScalefluence = setAuthorScalefluence;

    initialize();

    // Functions
    function initialize() {
      vm.showAuthorError = false;
      CampaignsService.indexScopedCampaigns('active', function (response) {
        vm.campaigns = response.data.campaigns;
      }, function () {
        $log.error('Error retrieving the campaigns.');
        toastr.error('There was an error retrieving the campaigns.');
      });
    }

    function getCampaign() {
      if (vm.content.campaign != null && angular.isDefined(vm.content.campaign.id)) {
        vm.content.categories = vm.content.campaign.categories;
        return vm.content.campaign.name;
      } else {
        return 'Please select a campaign';
      }
    }

    function submit() {
      if (!vm.contentAuthor) {
        vm.showAuthorError = true;
        toastr.error('Select who is doing content please.');
      } else {
          vm.content.agency_content = vm.contentAuthor === AUTHOR_QUOTE.AGENCY ? true : false;
          ContentService.createContent(vm.content, function () {
            $state.go('quotes-main');
            toastr.success('You content was created succesfuly.');
          }, function () {
            $log.error('Error creating the content request.');
            toastr.error('There was an error creating the content request. Please try again.');
          });
      }
    }

    function showAuthorContent() {
      return showAlertHTML('Who is doing content?', 'If your agency chooses ' +
      'to do the content, we will drop the price of each publisher by ' +
      '$50.', 'Ok').then(function (result) {
        return false;
      });
    }

    function showInfo() {
      return showAlertHTML('Higher Authority = Higher Price', 'Please give us a range to select publishers. ' +
        'Most agencies give a range of 30 to 40 points for both Domain Authority and Trust Flow. ' +
        '<br> <br>' +
        'REMEMBER: The higher the authority, the more expensive the link. ' +
        'Assume DA 40 is around $200. Assume DA 60+ is around $350 or more. ', 'Ok').then(function (result) {
        return false;
      });
    }

    function showAlertHTML(title, text, confirmText) {
      return $mdDialog.show(
        $mdDialog.alert()
        .title(title)
        .htmlContent(text)
        .escapeToClose(false)
        .ok(confirmText)
      );
    }

    function showAuthorErrorMessage() {
      return vm.contentAuthor ? '' : 'Must select who is doing content!';
    }

    function setAuthorAgency() {
      vm.contentAuthor = AUTHOR_QUOTE.AGENCY;
    }

    function setAuthorScalefluence() {
      vm.contentAuthor = AUTHOR_QUOTE.SF;
    }
  }
})();
