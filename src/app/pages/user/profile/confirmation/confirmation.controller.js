(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('ConfirmationController', ConfirmationController);

  /* @ngInject */
  function ConfirmationController($mdDialog, user) {
    var vm = this;

    //Accesible functions
    vm.save = save;
    vm.closeDialog = closeDialog;

    // Functions
    function save() {
      user.current_password = vm.current_password;
      $mdDialog.hide(user)
    }

    function closeDialog() {
      $mdDialog.hide();
    }
  }
})();
