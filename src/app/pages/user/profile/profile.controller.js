(function () {
  'use strict';

  angular
    .module('linktub.pages.user.profile', [])
    .controller('ProfileController', ProfileController);

  /* @ngInject */
  function ProfileController($log, $scope, $mdDialog, $state, toastr, UserService, SessionService,
    StatesService, RoutesService, AuthenticationService) {
    var vm = this;

    //Accesible variables
    vm.user = {
      password: '',
      password_confirmation: '',
      current_password: '',
      payment_method: ''
    };
    vm.oldEmail = '';
    vm.showEmailChangeNotice = false;
    vm.payment_email = 'paypal_email';

    //Accesible functions
    vm.submit = submit;
    vm.samePassword = samePassword;
    vm.canChangePassword = canChangePassword;
    vm.showOrderHistory = showOrderHistory;
    vm.isAdvertiser = isAdvertiser;
    vm.toggleZipMask = toggleZipMask;
    vm.showInfo = showInfo;
    vm.checkEmail = checkEmail;
    vm.updateAttempt = updateAttempt;
    vm.setPaymentMethod = setPaymentMethod;
    vm.setEmailChangeNotice = setEmailChangeNotice;

    initialize();

    // Functions
    function initialize() {
      UserService.getUser(function (response) {
        vm.user = response.data.user;
        vm.oldEmail = vm.user.email;
        vm.payment_email = vm.user.payment_method && (vm.user.payment_method.toLowerCase()+'_email') || 'paypal_email';
      }, function () {
        $log.error('Error retrieving user\'s info');
        toastr.error('There was an error retrieving your dashboard\'s info.')
      });

      vm.states = StatesService.getAllUSStates();
    }

    function checkEmail(form) {
      vm.attemptEmail = true;
      if (vm.user.email !== vm.oldEmail) {
        form.email.$setValidity('in-use', true);
        UserService.checkDuplicatedEmail(vm.user.email, function (response) {
          var data = response.data;
          form.email.$setValidity('in-use', !data.email_exists);
        }, function (err) {
          $log.error('Error checking duplicated email');
        });
      }
    }

    function updateAttempt() {
      vm.attemptEmail = false;
    }

    function submit(form) {
      if (typedNewPassword() && samePassword()) {
        updateUserInfo(false);
      } else if (!samePassword()) {
        form.confirmPassword.$setValidity("no-match", false);
      } else {
        if (emailedChanged()) {
          showEmailChangedPopup();
        } else {
          updateUserInfo(false);
        }
      }
    }

    function showChangePassword(ev) {
      $mdDialog.show({
        contentElement: '#password',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: false
      });
    };

    function showEmailChangedPopup() {
      $mdDialog.show({
        controller: 'ConfirmationController',
        controllerAs: 'confirmCtrl',
        templateUrl: 'app/pages/user/profile/confirmation/confirmation.html',
        hasBackdrop: true,
        clickOutsideToClose: true,
        escapeToClose: true,
        focusOnOpen: true,
        locals: {
          user: vm.user
        }
      }).then(function (user) {
        if (angular.isDefined(user)) {
          updateUserInfo(true);
        }
      });
    }

    function updateUserInfo(signOut) {
      UserService.updateUser(vm.user, function () {
        if (typedNewPassword() && samePassword()) {
          vm.user.current_password = '';
          vm.user.password = '';
          vm.user.password_confirmation = '';
        }

        if (emailedChanged() && signOut) {
          emailUpdated();
          AuthenticationService.logout(RoutesService.signIn);
        }

        toastr.success('User updated correctly');
        RoutesService.dashboard();
      }, function (response) {
        var error = response.data.error.current_password[0];
        if (error) {
          toastr.error('Cannot update user. Current password ' + error);
        } else {
          toastr.error('Cannot update user.');
        }
      });
    }

    function emailUpdated() {
      $mdDialog.show(
        $mdDialog.alert()
        .clickOutsideToClose(true)
        .title('Updating email')
        .htmlContent('Please check new email address. We just sent a confirmation to the address provided. Click the link to confirm that address.')
        .ariaLabel('Notes')
        .ok('Got it!')
      );
    }

    function emailedChanged() {
      return !angular.equals(vm.user.email, vm.oldEmail);
    }

    function samePassword() {
      return angular.equals(vm.user.password, vm.user.password_confirmation);
    }

    function canChangePassword(form) {
      form.confirmPassword.$setValidity("no-match", samePassword());
    }

    function showOrderHistory() {
      $state.go('billing');
    }

    function typedNewPassword() {
      return angular.isDefined(vm.user.password) &&
        angular.isDefined(vm.user.current_password);
    }

    function isAdvertiser() {
      return SessionService.isAdvertiser();
    }

    function toggleZipMask() {
      if (!vm.zipMask) {
        vm.zipMask = '99999';
      } else if (!vm.zipCode) {
        vm.zipMask = '';
      }
    }

    function showInfo(userProp) {
      switch (userProp) {
      case 'email':
        return showAlertHTML('Changing Your Email?', 'Changing your email will require ' +
          're-authorization via the new email address!', 'Ok').then(function (result) {
          return false;
        });
      }
    }

    function showAlertHTML(title, text, confirmText) {
      return $mdDialog.show(
        $mdDialog.alert()
        .title(title)
        .htmlContent(text)
        .escapeToClose(false)
        .ok(confirmText)
      );
    }

    function setPaymentMethod(paymentMethod, form) {
      form.$dirty = true;
      vm.user.payment_method = paymentMethod;
      vm.payment_email = vm.user.payment_method && (vm.user.payment_method.toLowerCase()+'_email') || 'paypal_email';
    }

    function setEmailChangeNotice(show) {
      vm.showEmailChangeNotice = show;
    }
  }
})();
