(function () {
  'use strict';

  angular
    .module('linktub.pages.user', [
      'linktub.pages.user.profile'
    ])
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('show-profile', {
        parent: 'dashboard',
        url: '/user/profile',
        templateUrl: 'app/pages/user/profile/profile.html',
        controller: 'ProfileController',
        controllerAs: 'profCtrl',
        authentication: {
          required: true,
          permittedRoles: ['advertiser', 'publisher']
        }
      });
  }
})();
