(function () {
  'use strict';

  angular
    .module('linktub.pages.contactUs')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('contact-us', {
        parent: 'dashboard',
        url: '/contactUs',
        templateUrl: 'app/pages/contactUs/contactUs.html',
        controller: 'ContactUsController',
        controllerAs: 'contactCtrl',
        authentication: {
          required: true,
          permittedRoles: ['publisher', 'advertiser']
        }
      });
  }
})();
