(function () {
  'use strict';

  angular
    .module('linktub.pages.contactUs', [])
    .controller('ContactUsController', ContactUsController);

  /* @ngInject */
  function ContactUsController($rootScope, $scope, $log, lodash, toastr,
    MessagesService, NotificationService, SessionService, PAGINATION_SIZE, MESSAGE_STATE, TYPE_STATE) {
    var vm = this;

    //attributes
    vm.role = '';
    vm.messages = [];
    vm.perPage = PAGINATION_SIZE.PER_PAGE;
    vm.maxSize = PAGINATION_SIZE.MAX_SIZE;
    vm.unreadItems = 0;
    vm.currentPage = 1;
    vm.totalPages = 0;
    vm.totalItems = 0;

    //functions
    vm.hasReply = hasReply;
    vm.getLastDate = getLastDate;
    vm.readReply = readReply;
    vm.retrieveMoreMessages = retrieveMoreMessages;
    vm.createMessage = createMessage;
    vm.isUnread = isUnread;
    vm.decodeMessage = decodeMessage;
    vm.toggleMessage = toggleMessage;

    initialize();

    function initialize() {
      removeNotification();
      getAllMessages(vm.currentPage);
    }

    function hasReply(msg) {
      return msg.state !== MESSAGE_STATE.SENT;
    }

    function isUnread(message) {
      return message.state === MESSAGE_STATE.UNREAD;
    }

    function getLastDate(message) {
      return (isNotNullOrEmpty(message.replied_at)) ? message.replied_at : message.date;
    }

    function readReply(message) {
      if (isUnread(message)) {
        MessagesService.updateState(message, function () {
          lodash.forEach(vm.messages, function (item) {
            if (item.id === message.id) {
              item.state = MESSAGE_STATE.READ;
              vm.unreadItems--;
            }
          });
        }, function (error) {
          $log.error(error);
        });
      }
    }

    function getAllMessages(currentPage) {
      MessagesService.getMessages(currentPage, function (response) {
        var data = response.data;
        vm.totalPages = data.pages;
        vm.totalItems = data.results;
        vm.messages = data.messages;
        vm.unreadItems = data.unread_messages_count;
      }, function (error) {
        toastr.error('There was an error retrieving messages');
        $log.error(response);
      });
    }

    function createMessage(form) {
      var msg = {};
      msg.subject = encodeURIComponent(vm.subject);
      msg.question_message = encodeURIComponent(vm.question_message);
      MessagesService.createMessage(msg, function (response) {
        toastr.success('Message sent to our admins');
        getAllMessages();
        form.$setPristine();
        vm.subject = '';
        vm.question_message = '';
      }, function (error) {
        toastr.error('Cannot sent message. Please, try again');
        $log.error(error);
      });
    }

    function retrieveMoreMessages(page) {
      vm.currentPage = page;
      getAllMessages(vm.currentPage);
    }

    function isNotNullOrEmpty(value) {
      return value !== '' && value !== null;
    }

    function removeNotification() {
      NotificationService.deleteNotification(TYPE_STATE.MESSAGES, SessionService.currentRole());
    }

    function decodeMessage(message) {
      return decodeURIComponent(message);
    }

    function toggleMessage(message) {
      message.active = !message.active;
    }
  }
})();
