(function () {
  'use strict';

  angular
    .module('linktub.pages.profiles.profile.edit')
    .controller('EditInfoDialogController', EditInfoDialogController);

  /** @ngInject */
  function EditInfoDialogController($mdDialog, $rootScope, ProfilesService, MIN_VALUES,
    PUB_URL_STATE, PricesService, url, isContributor) {

    var vm = this;
    vm.url = {};
    vm.showMinValues = false;
    vm.title = '';
    vm.showPause = true;
    vm.pausedValue = false;
    vm.isContributor = false;
    vm.suggestedPrice = '';
    vm.pricePlaceholder = 'Payout';
    vm.showRejectedReasons = false;
    vm.disable = false;

    //functions
    vm.checkURL = checkURL;
    vm.submit = submit;
    vm.cancel = cancel;
    vm.getDescriptionTitle = getDescriptionTitle;
    vm.rejectedReasons = rejectedReasons;
    vm.followLinkOptionIsSet = followLinkOptionIsSet;
    vm.setUrlRequiredLength = setUrlRequiredLength;
    vm.setUrlAllowHomepage = setUrlAllowHomepage;
    vm.setUrlFollowLink = setUrlFollowLink;
    vm.setUrlAllowBlogPost = setUrlAllowBlogPost;

    init();

    function init() {
      vm.isContributor = isContributor;
      if (url.href) {
        vm.url = url;
        vm.pausedValue = PUB_URL_STATE.PAUSED == vm.url.state;
        vm.title = PUB_URL_STATE.ACTIVE == vm.url.state ? 'Edit Information' : 'Re-Submit Edited URL Back to Us';
        vm.showRejectedReasons = PUB_URL_STATE.REJECTED == vm.url.state;
        vm.disable = PUB_URL_STATE.PAUSED == vm.url.state;
        vm.showPause = PUB_URL_STATE.REJECTED != vm.url.state;
      } else {
        vm.url.follow_link = true;
        vm.title = 'Add New Domain';
        vm.showPause = false;
      }
    }

    function submit() {
      url.state = vm.pausedValue;
      $mdDialog.hide(vm.url);
    }

    function checkURL(form) {
      var regex = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
      form.domainUrl.$setValidity('url', true);
      if (regex.test(vm.url.href)) {
        ProfilesService.getDomainInformation(vm.url.href, function (response) {
          vm.domInfo = response.data;
          if (vm.domInfo.trust_flow < MIN_VALUES.TF ||
            vm.domInfo.domain_authority < MIN_VALUES.DA) {
            vm.showMinValues = true;
            vm.suggestedPrice = '';
            vm.pricePlaceholder = 'Payout';
          } else {
            vm.showMinValues = false;
            vm.da = vm.domInfo.domain_authority;
            vm.tf = vm.domInfo.trust_flow;

            PricesService.getPrice(vm.url.href, function (response) {
              vm.pricePlaceholder = 'Suggested Price: ' + response.data.suggested_price;
            }, function (error) {
              $log.error(error);
              vm.pricePlaceholder = 'Suggested Price: ';
            });
          }
        });
      } else {
        form.domainUrl.$setValidity('url', false);
      }
    }

    function cancel() {
      $mdDialog.cancel();
    }

    function getDescriptionTitle(tooltip) {
      if (vm.url.description && !tooltip) {
        return 'Description';
      } else if (isContributor()) {
        return 'Description: Please tell us what you love to write about.' +
          ' This will enable us to pinpoint the most relevant opportunities for you. ' +
          'Also please indicate if you do Product Roundups, CEO quotes, and if you ' +
          'can link to the home page with a branded link.'
      }
    }

    function rejectedReasons() {
      return vm.url.rejected_reason;
    }

    function followLinkOptionIsSet(url) {
      return (url.follow_link === "true" || url.follow_link === "false")
    }

    function setUrlRequiredLength(requiredLength) {
      switch(requiredLength) {
        case '1500-+':
        case '1250-1500':
        case '1000-1250':
          vm.url.required_length = requiredLength;
          break;

        default:
          vm.url.required_length = '750-1000'
      }
    }

    function setUrlAllowHomepage(allow) {
      vm.url.allow_home_page = allow ? 'true' : 'false';
      if(allow) vm.url.allow_blog_post = 'false';
    }

    function setUrlFollowLink(isDoFollow) {
      vm.url.follow_link = isDoFollow ? 'true' : 'false';
    }

    function setUrlAllowBlogPost(allow) {
      vm.url.allow_blog_post = allow ? 'true' : 'false';
    }
  }
})();
