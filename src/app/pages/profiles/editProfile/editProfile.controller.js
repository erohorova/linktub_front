(function () {
  'use strict';

  angular
    .module('linktub.pages.profiles.profile.edit', [])
    .controller('EditProfileController', EditProfileController);

  /** @ngInject */
  function EditProfileController($state, $stateParams, $rootElement, $scope, $rootScope, $log, $mdDialog, $mdEditDialog,
    $mdMedia, toastr, lodash, ProfilesService, RoutesService, PricingService, SocialHandlesService, UrlsService, PROFILE_TYPE,
    SOCIAL, PAGINATION_SIZE, PUB_URL_STATE, MIN_VALUES, HTTP_STATUS, ERRORS_RESPONSES, PricesService, GoogleService, FacebookService) {
    var vm = this;

    // Accessible attributes
    vm.showCategories = false;
    vm.url = {};
    vm.disableMetaButton = false;
    vm.profile = {};
    vm.categories = [];
    vm.copyProfile = {};
    vm.profileNames = [];
    vm.followers = [];
    vm.query = {
      filter: '',
      order: 'state',
      limit: 20,
      page: 1
    };
    vm.perPage = PAGINATION_SIZE.PER_PAGE;
    vm.maxSize = PAGINATION_SIZE.MAX_SIZE;
    vm.urls = [];
    vm.csv;
    vm.search = false;
    vm.filtered = [];
    vm.filteredPausedStatus = [];
    vm.suggestedPrice = '';
    vm.pricePlaceholder = 'Payout';
    $rootScope.sampleDownloaded = false;
    vm.hasChanged = false;
    vm.web_content = {};

    // SEO price grid
    $scope.da;
    $scope.tf;
    $scope.close = function () {
      $mdDialog.cancel();
    }

    // Accessible functions
    vm.submit = submit;
    vm.avatarUploaded = avatarUploaded;
    vm.showPriceGa = showPriceGa;
    vm.addNewInput = addNewInput;
    vm.removeNewInput = removeNewInput;
    vm.canSubmit = canSubmit;
    vm.showPriceSEO = showPriceSEO;
    vm.isInfluencer = isInfluencer;
    vm.isBroker = isBroker;
    vm.isBlogger = isBlogger;
    vm.isContributor = isContributor;
    vm.cantSubmit = cantSubmit;
    vm.showInfo = showInfo;
    vm.editDomain = editDomain;
    vm.setFollowValue = setFollowValue;
    vm.clearCsv = clearCsv;
    vm.showCsvDialog = showCsvDialog;
    vm.removeFilter = removeFilter;
    vm.setPausedValue = setPausedValue;
    vm.pausedStateChanged = pausedStateChanged;
    vm.nonPausableUrl = nonPausableUrl;
    vm.checkSocialMinValues = checkSocialMinValues;
    vm.getPricingLabel = getPricingLabel;
    vm.canEditDomainInfo = canEditDomainInfo;
    vm.canEditSponsoredWeb = canEditSponsoredWeb;
    vm.canEditSocialHandle = canEditSocialHandle;
    vm.displayPendingStateText = displayPendingStateText;
    vm.showUploadedFileDialog = showUploadedFileDialog;
    vm.sendFileToAdmin = sendFileToAdmin;
    vm.checkURL = checkURL;
    vm.getPencilTooltip = getPencilTooltip;
    vm.categoriesChanged = categoriesChanged;
    vm.showXlsIcon = showXlsIcon;
    vm.showSearchIcon = showSearchIcon;
    vm.saveHeaderChanges = saveHeaderChanges;
    vm.saveCategories = saveCategories;
    vm.saveWebContent = saveWebContent;
    vm.saveSocialContent = saveSocialContent;
    vm.saveSearchContent = saveSearchContent;
    vm.checkGAMinValues = checkGAMinValues;
    vm.getPricingLabelGA = getPricingLabelGA;
    vm.showWarningPopup = showWarningPopup;
    vm.changeIcon = changeIcon;
    vm.getIconTooltip = getIconTooltip;
    vm.showRejectionReasons = showRejectionReasons;
    vm.isRejected = isRejected;
    vm.getPausedStatus = getPausedStatus;
    vm.handleFacebookAuthentication = handleFacebookAuthentication;
    vm.loadSocialHandle = loadSocialHandle;
    vm.authorize = authorize;
    vm.getYouTubeFollowers = getYouTubeFollowers;
    vm.queryAccounts = queryAccounts;
    vm.authenticateTwitter = authenticateTwitter;


    init();

    function init() {
      if (angular.isDefined($stateParams.profileId)) {
        ProfilesService.showProfile($stateParams.profileId, function (response) {
            vm.profile = response.data;
            if (!vm.profile.web_content) {
              vm.profile.web_content = {};
            }
            vm.url = vm.profile.urls[0];
            vm.urls = vm.profile.urls;
            vm.filtered = vm.profile.urls;
            vm.filteredPausedStatus = vm.filtered.map(function(url) { return setPausedValue(url); });
            vm.categories = vm.profile.categories;
            setSocialHandles();
            angular.copy(vm.profile, vm.copyProfile);
            ProfilesService.getProfileNames(function (response) {
              vm.profileNames = response.data.profile_names;
              lodash.remove(vm.profileNames, function (name) {
                return vm.profile.name == name;
              });
            });
            checkURL();
          },
          function () {
            $log.log('Error retrieving the profile.');
            toastr.error('There was an error retrieving the profile.');
          });
      }
      $scope.$mdMedia = $mdMedia;
    }

    function categoriesChanged() {}

    function getPencilTooltip(state) {
      switch (state) {
      case PUB_URL_STATE.REJECTED:
        return 'Make some changes and we\'ll approve this domain';
      case PUB_URL_STATE.CANCELLED:
        return 'We have rejected this domain. Sorry!';
      case PUB_URL_STATE.ACTIVE:
        return 'Url is live. Click here to edit.';
      case PUB_URL_STATE.PAUSED:
        return 'This URL is paused from search. Click here to change.';
      case PUB_URL_STATE.RESUBMITTED:
        return 'URL is Pending Approval by Admin';
      default:
        return 'URL is Pending Approval by Admin';
      }
    }

    function changeIcon(state) {
      switch (state) {
      case PUB_URL_STATE.PENDING:
        return 'hourglass_empty';
      case PUB_URL_STATE.RESUBMITTED:
        return 'hourglass_empty';
      case PUB_URL_STATE.REJECTED:
        return 'warning';
      case PUB_URL_STATE.ACTIVE:
        return 'check_circle';
      default:
        return 'help'
      }
    }

    function getIconTooltip(state) {
      switch (state) {
      case PUB_URL_STATE.PENDING:
        return 'Waiting on admin approval.';
      case PUB_URL_STATE.RESUBMITTED:
        return 'Waiting on admin approval.';
      case PUB_URL_STATE.ACTIVE:
        return 'Active product.';
      case PUB_URL_STATE.REJECTED:
        return 'Rejected product. Click to see why.';
      case PUB_URL_STATE.UNDER_REVIEW:
        return 'Waiting for admin approval.';
      default:
        return 'No tooltip found for this state.';
      }
    }

    function isRejected(state) {
      return state === PUB_URL_STATE.REJECTED;
    }

    function showRejectionReasons(product) {
      if (isRejected(product.state)) {
        $mdDialog.show({
          controller: 'NotesController',
          controllerAs: 'noteCtrl',
          templateUrl: 'app/pages/profiles/editProfile/notes/notes.html',
          clickOutsideToClose: false,
          escapeToClose: true,
          focusOnOpen: true,
          locals: {
            product: product
          }
        }).then(function (response) {
          product.canEdit = response.canEdit;
        });
      }
    }

    function setSocialHandles() {
      if (vm.profile.facebook_handles.length < 1) {
        addNewInput(SOCIAL.Facebook);
      }
      if (vm.profile.instagram_handles.length < 1) {
        addNewInput(SOCIAL.Instagram);
      }
      if (vm.profile.youtube_handles.length < 1) {
        addNewInput(SOCIAL.Youtube);
      }
      if (vm.profile.twitter_handles.length < 1) {
        addNewInput(SOCIAL.Twitter);
      }
      if (vm.profile.pinterest_handles.length < 1) {
        addNewInput(SOCIAL.Pinterest);
      }
    }

    function getPricingLabelGA(network) {
      return vm.web_content.muv ? 'Suggested price: ' + PricingService.getPrice(vm.web_content.muv, network) : 'Your price per post (USD):';
    }

    function addNewInput(socialHandle) {
      switch (socialHandle) {
      case SOCIAL.Facebook:
        vm.profile.facebook_handles.push({
          url: '',
          price: ''
        });
        break;
      case SOCIAL.Twitter:
        vm.profile.twitter_handles.push({
          url: '',
          price: ''
        });
        break;
      case SOCIAL.Youtube:
        vm.profile.youtube_handles.push({
          url: '',
          price: ''
        });
        break;
      case SOCIAL.Instagram:
        vm.profile.instagram_handles.push({
          url: '',
          price: ''
        });
        break;
      case SOCIAL.Pinterest:
        vm.profile.pinterest_handles.push({
          url: '',
          price: ''
        });
      }
    }

    function removeNewInput(socialHandle, item) {
      switch (socialHandle) {
      case SOCIAL.Facebook:
        lodash.remove(vm.profile.facebook_handles, item);
        break;
      case SOCIAL.Twitter:
        lodash.remove(vm.profile.twitter_handles, item);
        break;
      case SOCIAL.Youtube:
        lodash.remove(vm.profile.youtube_handles, item);
        break;
      case SOCIAL.Instagram:
        lodash.remove(vm.profile.instagram_handles, item);
        break;
      case SOCIAL.Pinterest:
        lodash.remove(vm.profile.pinterest_handles, item);
      }
    }

    function checkGAMinValues(display) {
      if (vm.profile.web_content.href) {
        ProfilesService.getAnalyticsMUV(vm.profile.web_content.href, function (response) {
          vm.web_content.muv = response.data.visitors;
          if (vm.web_content.muv < MIN_VALUES.GA) {
            showAlert('Minimum monthly unique visitors', 'We apologize but we need a minimum of 3000 unique visitors ' +
              'per month to participate in this product. You currently do not meet ' +
              'that criteria. When you do reach that traffic, you can come back to ' +
              'this profile and submit it then.', 'Ok I understand').then(function (result) {
              vm.profile.web_content.href = '';
              vm.profile.web_content.price = '';
            });
          } else {
            $scope.followers = vm.web_content.muv;
            $scope.network = 'GA';
            $scope.rowHeader = 'Suggested Pricing for Sponsored Web Content';
            $scope.content = 'Content will be disclosed and will have no-follow links. ' +
              'Should be placed on front page for best performance. ';
            $scope.contentLineTwo = 'Content must be up 6 months or longer.';
            $scope.header = 'Monthly Visitors';
            if (display) {
              displayRowGridPrice();
            }
          }
        }, function (response)  {
          toastr.error(response.error);
        });
      } else {
        vm.web_content.muv = '';
        getPricingLabelGA('GA');
      }
    }

    function checkGAUncompleted() {
      var bothSet = ((angular.isDefined(vm.profile.web_content.href) && vm.profile.web_content.href != '') &&
        vm.profile.web_content.price > 0);
      var noneSet = !(vm.profile.web_content.href || vm.profile.web_content.price);
      return bothSet || noneSet;
    }

    function checkSocialMinValues(type, socialMedia, display) {
      if (!socialMedia.url) {
        socialMedia.followers = '';
        getPricingLabel(socialMedia, type);
      } else {
        var handles = [];
        var socialMediaFlag = false;
        switch (type) {
        case SOCIAL.Facebook:
          socialMediaFlag = true;
          handles = vm.profile.facebook_handles;
          break;
        case SOCIAL.Twitter:
          socialMediaFlag = true;
          handles = vm.profile.twitter_handles;
          break;
        case SOCIAL.Youtube:
          handles = vm.profile.youtube_handles;
          break;
        case SOCIAL.Instagram:
          handles = vm.profile.instagram_handles;
          break;
        case SOCIAL.Pinterest:
          socialMediaFlag = true;
          handles = vm.profile.pinterest_handles;
        }
        var pos = lodash.findIndex(handles, ['$$hashKey', socialMedia.$$hashKey]);
        ProfilesService.getSocialHandleFollowers(socialMedia.url, type, function (response) {
            socialMedia.followers = response.data.followers;
            if (socialMedia.followers < MIN_VALUES.SOCIAL) {
              showAlert('Minimum followers', 'We apologize but we need a minimum of 1000 followers for ' +
                'EACH social handle for you to participate in this product. You ' +
                'currently do not meet that criteria for this handle. When you do ' +
                'reach that engagement, you can come back to this profile and submit it then.', 'Ok I understand').then(function (result) {
                handles[pos].url = '';
                handles[pos].price = '';
              });
            } else {
              $scope.followers = socialMedia.followers
              $scope.network = type;
              $scope.rowHeader = socialMediaFlag ? 'Suggested Pricing for Facebook, Twitter & Pinterest' : 'Suggested Pricing for Instagram & YouTube';
              $scope.content = 'For your protection, this content will be disclosed per FTC guidelines.';
              $scope.contentLineTwo = '';
              $scope.header = 'Followers';
              if (display) {
                displayRowGridPrice();
              }
            }
          },
          function (response) {
            toastr.error(response.error);
          });
      }
    }

    function canSubmit() {
      return !checkGAUncompleted();
    }

    function saveHeaderChanges(form) {
      ProfilesService.updateProfile(vm.profile.id, {
        image: vm.profile.image,
        name: vm.profile.name
      }, function () {
        form.$setPristine();
        toastr.success('Profile\'s name/image successfuly updated.');
      }, function () {
        toastr.error('There was an error updating this profile, please retry.');
      });
    }

    function saveCategories() {
      vm.newProfile = {
        categories_ids: lodash.map(vm.categories, 'id')
      }
      ProfilesService.updateProfile(vm.profile.id, vm.newProfile, function () {
        vm.hasChanged = false;
        toastr.success('Profile\'s categories successfuly updated.');
      }, function () {
        toastr.error('There was an error updating this profile\'s categories, please retry.');
      });
    }

    function saveSearchContent(form) {
      // Submit search content product
      ProfilesService.updateProfile(vm.profile.id, {
        urls: [{
          id: vm.url.id,
          href: vm.url.href,
          price: vm.url.price,
          description: vm.url.description
        }]
      }, function () {
        form.$setPristine();
        ProfilesService.showProfile(vm.profile.id, function (response) {
          var data = response.data;
          vm.url = data.urls[0];
          vm.urls = data.urls;
        }, function () {
          toastr.error('There was an error updating this profile\'s social content, try to refresh the page.');
        });
        toastr.success('Profile\'s web content successfuly updated.');
      }, function () {
        toastr.error('There was an error updating this profile\'s web content, please retry.');
      });

    }

    function saveWebContent(form) {
      ProfilesService.updateProfile(vm.profile.id, {
        web_content: {
          id: vm.profile.web_content.id,
          price: vm.profile.web_content.price,
          href: vm.profile.web_content.href
        }
      }, function () {
        form.$setPristine();
        ProfilesService.showProfile(vm.profile.id, function (response) {
          var data = response.data;
          vm.profile.web_content = data.web_content;
        }, function () {
          toastr.error('There was an error updating this profile\'s social content, try to refresh the page.');
        });
        toastr.success('Profile\'s web content successfuly updated.');
      }, function () {
        toastr.error('There was an error updating this profile\'s web content, please retry.');
      });
    }

    function saveSocialContent(form) {
      var profile = vm.profile;
      ProfilesService.updateProfile(profile.id, {
        facebook_handles: profile.facebook_handles,
        twitter_handles: profile.twitter_handles,
        instagram_handles: profile.instagram_handles,
        youtube_handles: profile.youtube_handles,
        pinterest_handles: profile.pinterest_handles
      }, function () {
        ProfilesService.showProfile(profile.id, function (response) {
          var data = response.data;
          if (data.facebook_handles.length > 0) {
            vm.profile.facebook_handles = data.facebook_handles;
          }
          if (data.twitter_handles.length > 0) {
            vm.profile.twitter_handles = data.twitter_handles;
          }
          if (data.instagram_handles.length > 0) {
            vm.profile.instagram_handles = data.instagram_handles;
          }
          if (data.youtube_handles.length > 0) {
            vm.profile.youtube_handles = data.youtube_handles;
          }
          if (data.pinterest_handles.length > 0) {
            vm.profile.pinterest_handles = data.pinterest_handles;
          }
        }, function () {
          toastr.error('There was an error updating this profile\'s social content, try to refresh the page.');
        });
        form.$setPristine();
        toastr.success('Profile\'s social content successfuly updated.');
      }, function (response) {
        if (response.data.errors) {
          var errors = lodash.map(Object.keys(response.data.errors), function (err) {
            return response.data.errors[err]
          });
          lodash.each(errors, function (error) {
            toastr.error(error[0]);
          })
        }
      });
    }

    function submit() {
      if (socialHandlesRepeated()) {
        showAlert('Repeated', 'You are submitting the same Social handle. ' +
          'We do not allow duplicate handles. Please submit a different handle.', 'Got it').then(function (result) {
          return false;
        });
      } else {
        vm.noCategories = false;
        if (!vm.categories.length) {
          vm.noCategories = true;
        } else {
          vm.profile.categories_ids = lodash.map(vm.categories, 'id');
          if (vm.profile.urls.length == 1) {
            vm.profile.urls = [];
            vm.profile.urls.push(vm.url);
          }
          if (isBlogger()) {
            ProfilesService.getDomainInformation(vm.url.href, function (response) {
              vm.domInfo = response.data;
              if (vm.domInfo.trust_flow < MIN_VALUES.TF ||
                vm.domInfo.domain_authority < MIN_VALUES.DA) {
                showSEODialog();
              } else {
                if (vm.profile.web_content.id && vm.profile.web_content.href == '' &&
                  vm.profile.web_content.price == '') {
                  vm.profile.web_content._destroy = 1;
                }
                continueSubmit();
              }
            });
          } else {
            continueSubmit();
          }
        }
      }
    }

    function continueSubmit() {
      if (vm.csv) {
        vm.profile.csv = vm.csv;
      }
      ProfilesService.updateProfile($stateParams.profileId, vm.profile, function (response) {
        toastr.success('Successfully updated the profile!');
        if (socialHandlesChange()) {
          showAlert('New social handle', 'Thanks for submitting new handles! ' +
            'We will need to vet these submission before accepting. Please respond to our email! Thank you!.', 'Got it').then(function (result) {
            RoutesService.dashboard();
          });
        } else {
          RoutesService.dashboard();
        }
      }, function (response) {
        handleErrors(response.data.errors);
      });
    }

    function handleErrors(errors) {
      lodash.forEach(errors, function (error) {
        toastr.error(error[0]);
      });
    }

    function showSEODialog() {
      showAlert('Minimum Values', 'We apologize but we need a minimum MOZ Domain Authority ' +
        'score of 15. And a minimum of of Majestic Trust Flow score of 5 for ' +
        'you to participate in this product. You currently do not meet one, ' +
        'or both, of these criteria. When you do reach these targets, you can ' +
        'come back to this profile and submit it then.', 'Ok I understand').then(function (result) {
        return false;
      });
    }

    function avatarUploaded() {
      return angular.isDefined(vm.profile.image) && vm.profile.image != null;
    }

    function showPriceGa(network, socialHandle, display) {
      if (network === 'GA') {
        if (angular.isDefined(vm.web_content.muv)) {
          $scope.followers = 0;
        }
        checkGAMinValues(display);
      } else {
        if (angular.isUndefined(socialHandle.followers)) {
          $scope.followers = 0;
          $scope.network = '';
        }
        checkSocialMinValues(network, socialHandle, display);
      }
    }

    function displayRowGridPrice() {
      $mdDialog.show({
        templateUrl: 'app/pages/profiles/editProfile/partials/rowDialog.html',
        parent: $rootElement,
        scope: $scope,
        controller: 'EditProfileController',
        preserveScope: true,
        clickOutsideToClose: false,
        fullscreen: true
      });
    }

    function cantSubmit() {
      if (!vm.profile.name) {
        return true;
      } else {
        if (isBlogger()) {
          return !(oneProductIsSet() && (vm.url.description && vm.url.href));
        } else if (isBroker() || isContributor()) {
          return !(vm.url.description && vm.url.price);
        } else if (isInfluencer()) {
          return !(vm.url.description && socialMediaProduct());
        }
      }
    }

    function oneProductIsSet() {
      var webProduct = vm.profile.web_content.href && vm.profile.web_content.price;
      var socialProduct = socialMediaProduct();
      var seoProduct = vm.url.price;

      return webProduct || socialProduct || seoProduct;
    }

    function socialMediaProduct() {
      var socialMediaProduct = [true, true, true, true, true];

      updateSocialmedia(socialMediaProduct, vm.profile.facebook_handles, 0);
      updateSocialmedia(socialMediaProduct, vm.profile.twitter_handles, 1);
      updateSocialmedia(socialMediaProduct, vm.profile.youtube_handles, 2);
      updateSocialmedia(socialMediaProduct, vm.profile.instagram_handles, 3);
      updateSocialmedia(socialMediaProduct, vm.profile.pinterest_handles, 4);

      return lodash.some(socialMediaProduct);
    }

    function socialHandlesRepeated() {
      return SocialHandlesService.anyRepeated(vm.profile);
    }

    function socialHandlesChange() {
      return SocialHandlesService.anyChange(vm.profile, vm.copyProfile);
    }

    function updateSocialmedia(socialMediaProduct, social_handle, index) {
      var count = 0;
      lodash.map(social_handle, function (s) {
        s._destroy = (s.url && s.price) ? 0 : 1;
        if (!s.url || !s.price) {
          count++;
        }
      });
      if (count == social_handle.length) {
        socialMediaProduct[index] = false;
      }
    }

    function isInfluencer() {
      return vm.profile.profile_type == PROFILE_TYPE.INFLUENCER;
    }

    function isBlogger() {
      return vm.profile.profile_type == PROFILE_TYPE.BLOGGER;
    }

    function isBroker() {
      return vm.profile.profile_type == PROFILE_TYPE.BROKER;
    }

    function isContributor() {
      return vm.profile.profile_type == PROFILE_TYPE.CONTRIBUTOR;
    }

    function showPriceSEO() {
      $mdDialog.show({
        templateUrl: 'app/pages/profiles/editProfile/partials/gridDialog.html',
        parent: $rootElement,
        scope: $scope,
        controller: 'EditProfileController',
        preserveScope: true,
        clickOutsideToClose: false,
        fullscreen: true
      });
    }

    function sendFileToAdmin() {
      ProfilesService.updateSpreadsheetProfile(vm.profile.id, vm.profile.profile_type, vm.csv, function (response) {
        refreshUrls();
        if (response.data && response.data.errors[0] == ERRORS_RESPONSES.ALREADY_TAKEN) {
          showSomeUrlsDuplicatedDialog();
        } else if (response.data && response.data.errors[0] == ERRORS_RESPONSES.INVALID) {
          showBadUrlDialog();
        } else {
          showUploadedFileDialog();
        }
        clearCsv();
      }, function (response) {
        if (response.status == HTTP_STATUS.BAD_REQUEST) {
          if (response.data.error == ERRORS_RESPONSES.ALREADY_TAKEN) {
            showDuplicateDialog();
          } else if (response.data.error == ERRORS_RESPONSES.INVALID) {
            showBadUrlDialog();
          } else {
            toastr.error(response.data.error);
          }
          clearCsv();
        }
      });
    }

    function showSomeUrlsDuplicatedDialog() {
      showAlertHTML('Some Duplicated Urls', 'Some Urls are already in out DB.' +
        " Please click 'Contact Us' if you feel this is an error.", 'Got it!').then(function (result) {
        return false;
      });
    }

    function showUploadedFileDialog() {
      return showAlertHTML('Spreadsheet uploaded', 'Thank you for uploading these domains. ' +
        'Our admin department will review them one at a time and you will receive notifications ' +
        'when they are either approved or rejected.', 'Got it!').then(function (result) {
        return false;
      });
    }

    function showDuplicateDialog() {
      return showAlertHTML('Duplicate Url', 'This URL is already in our DB.' +
        " Please click 'Contact Us' if you feel this is an error.", 'Got it!').then(function (result) {
        return false;
      });
    }

    function showBadUrlDialog() {
      return showAlertHTML('Bad Url format', 'Please take a look at your file, some urls are not correct.' +
        " Please click 'Contact Us' if you feel this is an error.", 'Got it!').then(function (result) {
        return false;
      });
    }

    function showInfo(step) {
      switch (step) {
      case 2:
        return showAlertHTML('Search Content (SEO)', "This product is for SEO firms. " +
          "You are accepting content that is 'natural and organic'. It is NOT a sales pitch. " +
          "Content will NOT be disclosed as paid. YOU ARE NOT recommending a product or service " +
          "to purchase per FTC guidelines. And links will be do-follow to help clients out with ranking higher.", 'Ok').then(function (result) {
          return false;
        });
        break;
      case 3:
        return showAlertHTML('Sponsored Web Content', 'Sponsored Web Content will read more like featured content. ' +
          'You will need to disclose compensation for the content per FTC regulations. ' +
          'Links inside the content will be NO-FOLLOW. You will have editorial control as well.' +
          '<br>' + '<br>' + 'By supplying your Google Analytics username below, our technology can see how ' +
          'many people engaged with each blog post. We will not be retrieving any ' +
          'private data from your Analytics account. Only the number of monthly ' +
          'unique visitors coming to your blog and previous content placements done through Scalefluence.', 'Ok').then(function (result) {
          return false;
        });
        break;
      case 4:
        return showAlertHTML('Sponsored Social Content', 'Sponsored Social Content is featured content on your social handle. ' +
          'You will need to disclose compensation for the content per FTC regulations ' +
          'by using #Ad or #Sponsored hashtags. Content will also come with a Tiny URL ' +
          'to track clicks and engagement of your Social Post. ' +
          '<br>' + '<br>' + 'By supplying your Social handles below, our technology can see how many ' +
          'people engaged with the social post. We will not be retrieving any private ' +
          'data of your account. Only the number of followers and previous content ' +
          'placements done through Scalefluence.', 'Ok').then(function (result) {
          return false;
        });
      }

      $mdDialog.show(confirm).then(function (result) {
        return false;
      });
    }

    function editDomain(ev, url) {
      var copiedUrl = {};
      var canEdit = true;
      if (url) {
        angular.copy(url, copiedUrl);

        if (url.state == PUB_URL_STATE.PENDING) {
          canEdit = false;
          showAlert('Pending approval', 'This URL is pending approval by our Admin team. ' +
            'You cannot edit it until we approve it.', 'Got it');
        } else if (url.state == PUB_URL_STATE.REJECTED) {
          canEdit = false;
          showAlertHTML('Rejected URL', 'Here are the changes we need to accept this ' +
            'domain into our marketplace. ' +
            '<br><br> <span class="bold-uppercase">' + url.rejected_reason + '</span>', 'Ok, Go to edit').then(function () {
            showEditInfoDialog(copiedUrl, url);
          });
        } else if (url.state == PUB_URL_STATE.CANCELLED) {
          canEdit = false;
          showAlert('Cancelled URL', 'After going through our Quality Assessment tests; ' +
            'this domain has been permanently rejected. We apologize for the inconvenience.', 'Got it');
        } else if (url.state == PUB_URL_STATE.UNDER_REVIEW) {
          canEdit = false;
          showAlert('Pending approval', 'This URL is pending price approval by our Admin team. ' +
            'You cannot edit it until we re approve it.', 'Got it');
        }
      }
      if (canEdit) {
        showEditInfoDialog(copiedUrl, url, null);
      }
    }

    function showEditInfoDialog(copiedUrl, url) {
      $mdDialog.show({
        templateUrl: 'app/pages/profiles/editProfile/editModalInfo/editInfoDialog.html',
        controller: 'EditInfoDialogController',
        controllerAs: 'editInfoCtrl',
        preserveScope: true,
        clickOutsideToClose: false,
        fullscreen: true,
        locals: {
          url: copiedUrl,
          isContributor: isContributor()
        }
      }).then(function (response) {
        if (url) {
          ProfilesService.updateUrl(vm.profile.id, {
            id: response.id,
            href: response.href,
            follow_link: response.follow_link,
            price: response.price,
            description: response.description,
            pause: response.state
          }, function () {
            refreshUrls();
          }, function () {
            $log.error('error');
          })
        } else {
          ProfilesService.updateProfile(vm.profile.id, {
            urls: [{
              href: response.href,
              follow_link: response.follow_link,
              allow_blog_post: response.allow_blog_post,
              allow_home_page: response.allow_home_page,
              required_length: response.required_length,
              price: response.price,
              description: response.description
            }]
          }, function () {
            refreshUrls();
          }, function () {
            $log.error('error');
          })
        }
      });
    }

    function refreshUrls() {
      ProfilesService.indexUrls(vm.profile.id, function (response) {
        vm.urls = response.data.urls;
      }, function () {
        $log.error('error');
      });
    }

    function setDataToUrl(urlX, newUrl) {
      urlX.follow_link = newUrl.follow_link;
      urlX.href = newUrl.href;
      urlX.description = newUrl.description;
      urlX.state = newUrl.state;
      urlX.price = newUrl.price;
    }

    function setFollowValue(url) {
      return url.follow_link ? 'Do-Follow Link' : 'No-Follow Link';
    }

    function setPausedValue(url) {
      return { id: url.id, paused: url.state === PUB_URL_STATE.PAUSED };
    }

    function pausedStateChanged(url, pos) {
      url.paused = updatePausedStatus(url);

      UrlsService.updateUrl(url, function(response) {
        var message = url.paused ? 'paused' : 'unpaused';
        toastr.success('Your URL was ' + message)
      }, function(error) {
        updatePausedStatus(url);
        toastr.error(response.error);
      });
    }

    function getPausedStatus(url) {
      var filteredObj = lodash.find(vm.filteredPausedStatus, function(filteredObj) {
        return filteredObj.id === url.id;
      });

      return filteredObj ? filteredObj.paused : false;
    }

    function updatePausedStatus(url) {
      var filteredObj = lodash.find(vm.filteredPausedStatus, function(filteredObj) {
        return filteredObj.id === url.id;
      });

      if(filteredObj) {
        filteredObj.paused = !filteredObj.paused;
      }

      return filteredObj ? filteredObj.paused : false;
    }

    function nonPausableUrl(url) {
      return url.state !== PUB_URL_STATE.ACTIVE && url.state !== PUB_URL_STATE.PAUSED;
    }

    function clearCsv() {
      vm.csv = null;
    }

    function getPricingLabel(socialHandle, network) {
      return socialHandle.followers ? 'Suggested price: ' + PricingService.getPrice(socialHandle.followers, network) : 'Your price per post (USD):';
    }

    function checkURL() {
      ProfilesService.getDomainInformation(vm.url.href, function (response) {
        vm.domInfo = response.data;
        if (vm.domInfo.trust_flow < MIN_VALUES.TF ||
          vm.domInfo.domain_authority < MIN_VALUES.DA) {
          vm.showMinValues = true;
          vm.suggestedPrice = '';
          vm.pricePlaceholder = 'Payout';
        } else {
          vm.showMinValues = false;
          vm.da = vm.domInfo.domain_authority;
          vm.tf = vm.domInfo.trust_flow;
          $scope.da = vm.da;
          $scope.tf = vm.tf;

          PricesService.getPrice(vm.url.href, function (response) {
            vm.pricePlaceholder = 'Suggested Price: ' + response.data.suggested_price;
          }, function (error) {
            $log.error(error);
            vm.pricePlaceholder = 'Suggested Price: ';
          });
        }
      });
    }

    function showCsvDialog() {
      $mdDialog.show({
        controller: 'CsvModalController',
        controllerAs: 'csvModalCtrl',
        templateUrl: 'app/pages/profiles/editProfile/csvModal/csvModal.html',
        clickOutsideToClose: true,
        locals: {
          isContributor: isContributor()
        }
      }).then(function (uploadedFile) {
        vm.csv = uploadedFile;
      });
    }

    function removeFilter() {
      vm.search = false;
      vm.query.filter = '';
    }

    $scope.$watch('editProfCtrl.query.filter', function (newValue, oldValue) {
      var bookmark = '';

      if (!oldValue) {
        bookmark = vm.query.page;
      }

      if (newValue !== oldValue) {
        vm.query.page = 1;
      }

      if (!newValue) {
        vm.query.page = bookmark;
      }

      if (newValue == '') {
        vm.query.page = 1;
      }
    });

    function showAlert(title, text, confirmText) {
      return $mdDialog.show(
        $mdDialog.alert()
        .title(title)
        .textContent(text)
        .escapeToClose(false)
        .ok(confirmText)
      );
    }

    function showAlertHTML(title, text, confirmText) {
      return $mdDialog.show(
        $mdDialog.alert()
        .title(title)
        .htmlContent(text)
        .escapeToClose(false)
        .ok(confirmText)
      );
    }

    function canEditDomainInfo() {
      return !lodash.includes([PUB_URL_STATE.PENDING, PUB_URL_STATE.RESUBMITTED, PUB_URL_STATE.PAUSED], vm.url.state) && vm.url.canEdit;
    }

    function canEditSponsoredWeb() {
      return !lodash.includes([PUB_URL_STATE.PENDING, PUB_URL_STATE.RESUBMITTED], vm.profile.web_content) && vm.profile.web_content.canEdit;
    }

    function canEditSocialHandle(sh) {
      return !lodash.includes([PUB_URL_STATE.PENDING, PUB_URL_STATE.RESUBMITTED], sh.state) && sh.canEdit;
    }

    function displayPendingStateText() {
      switch (vm.url.state) {
        case PUB_URL_STATE.PAUSED:
          return 'You must unpause this URL first, before changing anything.'
        default:
          return 'Admin must approve your submission before you can edit';
      }
    }

    function showXlsIcon() {
      return 'assets/images/xlsUpload.svg';
    }

    function showSearchIcon() {
      return 'assets/images/search_icon.svg';
    }

    function showWarningPopup() {
      showAlert('Suggested Pricing', 'In order to be able to display the suggested price you must sign in to the social network.', 'Got it!');
    }

    function handleFacebookAuthentication(ev, socialHandle, socialNetwork) {
      FacebookService.authenticate(function (userId) {
        switch (socialNetwork) {
          case 'facebook':
            FacebookService.getFanPageData(function (response) {
              loadSocialHandle(socialHandle, socialNetwork, response.username, response.fan_count);
            }, function (error) {
              toastr.error(error);
            });
            break;
          case 'instagram':
            FacebookService.checkForInstagramBusinessAccount(function (response) {
              loadSocialHandle(socialHandle, socialNetwork, response.username, response.followers_count);
            }, function (error) {
              toastr.error(error);
            });
            break;
        }
      }, function (error) {
        toastr.error(error);
      });
    }

    function loadSocialHandle(socialHandle, socialNetwork, username, count) {
      socialHandle.url = 'https://www.' + socialNetwork + '.com/' + username;
      socialHandle.followers = count;
      checkSocialMinValues(socialNetwork, socialHandle);
    }

    function authorize(event, isYoutube, socialHandle) {
      GoogleService.authorize(event, function (response) {
        if (response.error) {
          vm.hidden = false;
        } else {
          vm.hidden = true;
          if (isYoutube) {
            getYouTubeFollowers(socialHandle);
          } else {
            queryAccounts();
          }
        }
      });
    }

    function getYouTubeFollowers(socialHandle) {
      GoogleService.queryYouTubeStatistics(function (response) {
        if (response.result.items.length) {
          socialHandle.url = 'www.youtube.com/channel/' + response.result.items[0].id;
          socialHandle.followers = response.result.items[0].statistics.subscriberCount;
          checkSocialMinValues('youtube', socialHandle);
        } else {
          toastr.error('', 'No channel associated to your account.');
        }
      });
    }

    function queryAccounts() {
      vm.analytics = [];
      vm.showProgressCircular = true;
      GoogleService.queryAccounts(handleAccounts, function (error) {
        toastr.error('No Google Analytics account found');
        vm.showProgressCircular = false;
      });
    }

    function authenticateTwitter(ev, socialHandle) {
      $mdDialog.show({
        controller: 'TwitterHandleController',
        controllerAs: 'ctrl',
        templateUrl: 'app/pages/profiles/newProfile/partials/twitterHandleDialog.html',
        targetEvent: ev,
        clickOutsideToClose: true
      }).then(function (response) {
        var handle = response.handle;
        if (handle) {
          var regex_twitter_profile = /(.*twitter\.com\/)?(.*)/
          var account = regex_twitter_profile.exec(handle)[2] || ''
          ProfilesService.getTwitterFollowersCount(account, function (response) {
            socialHandle.url = 'www.twitter.com/' + account;
            socialHandle.followers = response.data.count;
            checkSocialMinValues('twitter', socialHandle);
          }, function (error) {
            toastr.error(error.data.error);
          });
        }
      }, function () {
        $log.log('Dialog dismissed');
      });
    }
  }
})();
