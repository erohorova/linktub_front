(function () {
  'use strict';

  angular
    .module('linktub.pages.profiles.profile.edit')
    .controller('CsvModalController', CsvModalController);

  /** @ngInject */
  function CsvModalController($state, $log, $mdDialog, $rootScope, Assets, isContributor) {
    var vm = this;
    vm.isContributor = isContributor;

    //Accessible functions
    vm.submit = submit;
    vm.clearCsv = clearCsv;
    vm.download = download;
    vm.getPrice = getPrice;
    vm.getFileRoute = getFileRoute;

    function submit() {
      $mdDialog.hide(vm.csv);
    }

    function clearCsv() {
      vm.csv = null;
    }

    function download() {
      $rootScope.sampleDownloaded = true;
    }

    function getPrice() {
      return vm.isContributor ? '200' : '130';
    }

    function getFileRoute() {
      return  vm.isContributor ? Assets + 'contributor_example.xlsx' : Assets + 'broker_example.xlsx';
    }
  }
})();
