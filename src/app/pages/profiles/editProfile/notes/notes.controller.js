(function () {
  'use strict';

  angular
    .module('linktub.pages.profiles.profile.edit')
    .controller('NotesController', NotesController);

  /* @ngInject */
  function NotesController($scope, $mdDialog, lodash, product) {
    var vm = this;

    //Accesible variables
    vm.note = '';
    vm.product = {};

    //Accesible functions
    vm.ok = ok;

    initialize();

    function initialize() {
      vm.product = product;
      vm.note = product.rejected_reason;
    }

    function ok() {
      $mdDialog.hide({
        canEdit: true
      });
    }
  }
})();
