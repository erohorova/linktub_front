(function () {
  'use strict';

  angular
    .module('linktub.pages.profiles', [
      'linktub.pages.profiles.all',
      'linktub.pages.profiles.profile.new',
      'linktub.pages.profiles.profile.edit'
    ])
    .config(routesConfig);

  /** @ngInject */
  function routesConfig($stateProvider) {
    $stateProvider
      .state('all-profiles', {
        parent: 'dashboard',
        url: '/profiles/all',
        templateUrl: 'app/pages/profiles/allProfiles/allProfiles.html',
        controller: 'AllProfilesController',
        controllerAs: 'allProfCtrl',
        authentication: {
          required: true,
          permittedRoles: ['publisher']
        }
      })
      .state('new-profile', {
        parent: 'dashboard',
        url: '/profiles/new',
        templateUrl: 'app/pages/profiles/newProfile/newProfile.html',
        controller: 'NewProfileController',
        controllerAs: 'newProfCtrl',
        authentication: {
          required: true,
          permittedRoles: ['publisher']
        }
      })
      .state('edit-profile', {
        parent: 'dashboard',
        url: '/profiles/edit/:profileId',
        templateUrl: 'app/pages/profiles/editProfile/editProfile.html',
        controller: 'EditProfileController',
        controllerAs: 'editProfCtrl',
        authentication: {
          required: true,
          permittedRoles: ['publisher']
        }
      });
  }
})();
