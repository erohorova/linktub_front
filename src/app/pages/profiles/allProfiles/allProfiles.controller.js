(function () {
  'use strict';

  angular
    .module('linktub.pages.profiles.all', [])
    .controller('AllProfilesController', AllProfilesController);

  /** @ngInject */
  function AllProfilesController($log, $anchorScroll, toastr, ProfilesService) {
    var vm = this;

    // Accessible attributes
    vm.profiles = [];
    vm.perPage = 5;

    //Accessible functions
    vm.goTop = goTop;

    initialize();

    //Functions
    function initialize() {
      ProfilesService.indexProfiles(function (response) {
        vm.profiles = response.data.profiles;
      }, function (response) {
        toastr.error('There was an error retrieving profiles');
        $log.error(response);
      });
    }

    function goTop() {
      $anchorScroll('top');
    }
  }
})();
