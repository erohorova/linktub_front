(function () {
  'use strict';

  angular
    .module('linktub.pages.profiles.profile.new', [])
    .controller('NewProfileController', NewProfileController);

  /** @ngInject */
  function NewProfileController($scope, $log, $mdDialog, $mdMedia, $rootElement, $rootScope,
    $anchorScroll, $location, toastr, lodash, EnvironmentConfig, RoutesService,
    ProfilesService, SocialHandlesService, PricesService, PricingService,
    GoogleService, FacebookService, PROFILE_TYPE, SOCIAL, MIN_VALUES, SCOPE) {
    var vm = this;
    const TABS = 4;

    // Accessible attributes
    $rootScope.da;
    $rootScope.tf;
    $scope.da;
    $scope.tf;
    $scope.close = function () {
      $mdDialog.cancel();
    }
    vm.domInfo;
    vm.selectedIndex = 0
    vm.stepsLocker = [false, true, true, true, true];
    vm.stepNames = ['Create', 'Domain Info', 'Sponsored Blog', 'Sponsored Social', 'SEO'];
    vm.influencer = {
      description: ''
    };
    vm.url = {
      description: '',
      href: '',
      price: '',
      author_page: ''
    };
    vm.profile = {
      categories_ids: [],
      urls: [],
      facebook_handles: [],
      twitter_handles: [],
      instagram_handles: [],
      youtube_handles: [],
      pinterest_handles: [],
      web_content: {
        href: '',
        price: ''
      }
    };
    vm.web_content = {};
    vm.gaValidating = false;
    vm.categories = [];
    vm.seoFirstTime = true;
    vm.disableSEO = false;
    vm.profileNames = [];
    vm.canContinue = false;
    vm.analytics = [];
    vm.selectedAnalytics = {};
    vm.showProgressCircular = false;
    vm.showHomePageError = false;
    vm.showFollowLinkError = false;
    vm.showAllowBlogPostError = false;
    vm.priceLabel = '';
    vm.domainUrlValid = false;
    vm.categoriesOn = true;

    // Accessible functions
    vm.next = next;
    vm.previous = previous;
    vm.nextDisabled = nextDisabled;
    vm.showPriceSEO = showPriceSEO;
    vm.showPriceGa = showPriceGa;
    vm.lastStep = lastStep;
    vm.submit = submit;
    vm.getCurrentStepName = getCurrentStepName;
    vm.checkAvatar = checkAvatar;
    vm.getMetaDescription = getMetaDescription;
    vm.noSocialHandlesSet = noSocialHandlesSet;
    vm.noWebContentSet = noWebContentSet;
    vm.addNewInput = addNewInput;
    vm.getDescriptionTitle = getDescriptionTitle;
    vm.getTileForInfluencersDescription = getTileForInfluencersDescription;
    vm.checkGAMinValues = checkGAMinValues;
    vm.checkSocialMinValues = checkSocialMinValues;
    vm.cantSubmit = cantSubmit;
    vm.setAsInfluencer = setAsInfluencer;
    vm.setAsBroker = setAsBroker;
    vm.setAsContributor = setAsContributor;
    vm.setAsBlogger = setAsBlogger;
    vm.isInfluencer = isInfluencer;
    vm.isBroker = isBroker;
    vm.isContributor = isContributor;
    vm.isBlogger = isBlogger;
    vm.influencer = influencer;
    vm.blogger = blogger;
    vm.broker = broker;
    vm.contributor = contributor;
    vm.removeNewInput = removeNewInput;
    vm.oneProductIsSet = oneProductIsSet;
    vm.getFinishText = getFinishText;
    vm.showInfo = showInfo;
    vm.isInvalidProfileName = isInvalidProfileName;
    vm.checkSEO = checkSEO;
    vm.setPriceLabel = setPriceLabel;
    vm.getPricingLabel = getPricingLabel;
    vm.getPricingLabelGA = getPricingLabelGA;
    vm.showWarningPopup = showWarningPopup;
    vm.authorize = authorize;
    vm.analyticSelected = analyticSelected;
    vm.showHomePageErrorMessage = showHomePageErrorMessage;
    vm.showFollowLinkErrorMessage = showFollowLinkErrorMessage;
    vm.setContentType = setContentType;
    vm.typeSelected = typeSelected;
    vm.authenticateTwitter = authenticateTwitter;
    vm.handleFacebookAuthentication = handleFacebookAuthentication;
    vm.toggleCategories = toggleCategories;
    vm.categoriesActionMsg = categoriesActionMsg;
    vm.setUrlRequiredLength = setUrlRequiredLength;
    vm.setUrlAllowHomepage = setUrlAllowHomepage;
    vm.setUrlFollowLink = setUrlFollowLink;
    vm.setUrlAllowBlogPost = setUrlAllowBlogPost;

    init();

    function init() {
      $scope.$mdMedia = $mdMedia;
      vm.profile.profileType = '';
      vm.profile.contentType = '';
      vm.showTypeError = false;
      vm.showMissingContentTypeError = false;
      vm.profileTypeOld = vm.profile.profileType;

      // Add one blank item per social handle
      addNewInput(SOCIAL.Facebook);
      addNewInput(SOCIAL.Twitter);
      addNewInput(SOCIAL.Youtube);
      addNewInput(SOCIAL.Instagram);

      ProfilesService.getProfileNames(function (response) {
        vm.profileNames = response.data.profile_names;
      });
    }

    function analyticSelected() {
      vm.showProgressCircular = true;
      vm.profile.web_content.href = JSON.parse(vm.selectedAnalytics).selfLink;
      GoogleService.queryProfiles(vm.selectedAnalytics, handleProfiles, function (err) {
        $log.error(err);
      });
    }

    function showFollowLinkErrorMessage() {
      return 'You MUST select one option.';
    }

    function showHomePageErrorMessage() {
      return 'You MUST select one option.';
    }

    function authorize(event, isYoutube, socialHandle) {
      GoogleService.authorize(event, function (response) {
        if (response.error) {
          vm.hidden = false;
        } else {
          vm.hidden = true;
          if (isYoutube) {
            getYouTubeFollowers(socialHandle);
          } else {
            queryAccounts();
          }
        }
      });
    }

    function queryAccounts() {
      vm.analytics = [];
      vm.showProgressCircular = true;
      GoogleService.queryAccounts(handleAccounts, function (error) {
        toastr.error('No Google Analytics account found');
        vm.showProgressCircular = false;
      });
    }

    function handleAccounts(response) {
      var accounts = response.result.items;
      if (accounts && accounts.length) {
        lodash.map(accounts, function (account) {
          GoogleService.queryProperties(account, handleProperties, function (err) {
            $log.error(err);
          });
        })
      } else {
        $log.log('There are no accounts to show');
      }
      vm.showProgressCircular = false;
    }

    function handleProperties(response) {
      var properties = response.result.items;
      if (properties && properties.length) {
        lodash.map(properties, function (property) {
          vm.analytics.push(property);
        })
      } else {
        $log.log('There are no properties to show');
      }
    }

    function handleProfiles(response) {
      var profiles = response.result.items;
      if (profiles && profiles.length) {
        var firstProfileId = profiles[0].id;
        GoogleService.queryCoreReportingApi(firstProfileId, handleReports, function (err) {
          $log.error(err);
        });
      } else {
        $log.log('There are no profiles to show');
      }
    }

    function handleReports(response) {
      var formattedJson = JSON.stringify(response.result, null, 2);
      vm.web_content.muv = parseInt(response.result.totalsForAllResults['ga:users']) + 3000;

      $scope.followers = vm.web_content.muv;
      $scope.network = 'GA';
      $scope.rowHeader = 'Suggested Pricing for Sponsored Web Content';
      $scope.content = 'Content will be disclosed and will have no-follow links. ' +
        'Should be placed on front page for best performance.';
      $scope.contentLineTwo = 'Content must be up 6 months or longer.';
      $scope.header = 'Monthly Visitors';

      getPricingLabelGA('GA');
      vm.showProgressCircular = false;
    }

    // Functions for Gbutton
    window.authorize = authorize;

    function toggleLock(positions, lock) {
      lodash.map(positions, function (pos) {
        vm.stepsLocker[pos] = lock;
      });
    }

    function previousStepsUnlock(position) {
      if (position == 0) {
        return !vm.stepsLocker[position];
      }
      return lodash.reduce(vm.stepsLocker.slice(0, position), function (carry, n) {
        return carry && !n;
      }, true);
    }

    function addNewInput(socialHandle) {
      var blank = {
        url: '',
        price: ''
      };
      switch (socialHandle) {
      case SOCIAL.Facebook:
        vm.profile.facebook_handles.push(blank);
        break;
      case SOCIAL.Twitter:
        vm.profile.twitter_handles.push(blank);
        break;
      case SOCIAL.Youtube:
        vm.profile.youtube_handles.push(blank);
        break;
      case SOCIAL.Instagram:
        vm.profile.instagram_handles.push(blank);
        break;
      case SOCIAL.Pinterest:
        vm.profile.pinterest_handles.push(blank);
      }
    }

    function removeNewInput(socialHandle, item) {
      switch (socialHandle) {
      case SOCIAL.Facebook:
        lodash.remove(vm.profile.facebook_handles, item);
        break;
      case SOCIAL.Twitter:
        lodash.remove(vm.profile.twitter_handles, item);
        break;
      case SOCIAL.Youtube:
        lodash.remove(vm.profile.youtube_handles, item);
        break;
      case SOCIAL.Instagram:
        lodash.remove(vm.profile.instagram_handles, item);
        break;
      case SOCIAL.Pinterest:
        lodash.remove(vm.profile.pinterest_handles, item);
      }
    }

    function showPriceGa(network, socialHandle, display) {
      if (network == 'GA') {
        if (angular.isDefined(vm.web_content.muv)) {
          $scope.followers = 0;
        }
        checkGAMinValues(display);
      } else {
        if (angular.isUndefined(socialHandle.followers)) {
          $scope.followers = 0;
          $scope.network = '';
        }
        checkSocialMinValues(network, socialHandle, display)
      }
    }

    function displayRowGridPrice() {
      $mdDialog.show({
        templateUrl: 'app/pages/profiles/newProfile/partials/rowDialog.html',
        parent: $rootElement,
        scope: $scope,
        controller: 'NewProfileController',
        preserveScope: true,
        clickOutsideToClose: false,
        fullscreen: true
      });
    }

    function checkAvatar(ev, tabSelection) {
      if (!vm.profile.image && typeSelected() && contentTypeSelected()) {
        var confirm = $mdDialog.confirm()
          .title('Upload a Photo!')
          .textContent('Profiles with Photos receive far more work than those that don\'t!')
          .targetEvent(ev)
          .ok('Ok Do it Now!')
          .cancel('No Thanks.');

        $mdDialog.show(confirm).then(function (result) {
          vm.selectedIndex = 0;
        }, function () {
          if (!tabSelection && typeSelected() && contentTypeSelected()) {
            findNextIndex();
          }
        });
      } else if (typeSelected() && contentTypeSelected()) {
        findNextIndex();
      }
    }

    function findNextIndex() {
      var pos = lodash.findIndex(vm.stepsLocker, function (step, index) {
        return !step && index > vm.selectedIndex;
      });
      vm.selectedIndex = Math.max(pos, 0);
    }

    function setAsInfluencer() {
      vm.profile.profileType = PROFILE_TYPE.INFLUENCER;
    }

    function setAsBlogger() {
      vm.profile.profileType = PROFILE_TYPE.BLOGGER;
    }

    function setAsBroker() {
      vm.profile.profileType = PROFILE_TYPE.BROKER;
    }

    function setAsContributor() {
      vm.profile.profileType = PROFILE_TYPE.CONTRIBUTOR;
    }

    function isInfluencer() {
      return vm.profile.profileType == PROFILE_TYPE.INFLUENCER;
    }

    function isBlogger() {
      return vm.profile.profileType == PROFILE_TYPE.BLOGGER;
    }

    function isBroker() {
      return vm.profile.profileType == PROFILE_TYPE.BROKER;
    }

    function isContributor() {
      return vm.profile.profileType == PROFILE_TYPE.CONTRIBUTOR;
    }

    function influencer() {
      return PROFILE_TYPE.INFLUENCER;
    }

    function blogger() {
      return PROFILE_TYPE.BLOGGER;
    }

    function broker() {
      return PROFILE_TYPE.BROKER;
    }

    function contributor() {
      return PROFILE_TYPE.CONTRIBUTOR;
    }

    function isInvalidProfileName() {
      var invalid = false;
      lodash.map(vm.profileNames, function (name) {
        if (vm.profile.name && (vm.profile.name.toUpperCase() == name.toUpperCase())) {
          invalid = true;
        }
      });
      return invalid;
    }

    function nextDisabled() {
      var lock;
      switch (vm.selectedIndex) {
      case 0:
        lock = !vm.profile.name;
        if (vm.profile.profileType != vm.profileTypeOld) {
          vm.profileTypeOld = vm.profile.profileType;
          toggleLock([1, 2, 3, 4], true);
          var name = vm.profile.name;
          var image = vm.profile.image;
          vm.profile.name = name;
          vm.profile.image = image;
          vm.profile.profileType = vm.profileTypeOld;
        }

        if (isBlogger()) {
          toggleLock([1, 2, 3, 4], false);
        } else if (isInfluencer()) {
          toggleLock([1,3], false);
          toggleLock([2,4], true);
        } else if (isContributor() || isBroker()) {
          toggleLock([1,4], false);
        }
        return lock;
        break;
      case 1:
        lock = !isDomainInfoCompleted();
        return lock;
        break;
      case 2:
        lock = !isAnalyticsCompleted();
        toggleLock([3, 4], false);
        setUrl();
        return lock;
        break;
      case 3:
        return false;
        break;
      case 4:
        if (vm.seoFirstTime || (vm.oldUrl != vm.url.href)) {
          vm.seoFirstTime = false;
        }
      }
      return true;
    }

    /* STEP 2 */
    function setUrl() {
      if (!vm.oldUrl) {
        vm.oldUrl = vm.url.href;
      }
    }

    function lockStepInvalidName(step) {
      if (isInvalidProfileName()) {
        toggleLock([step], true);
      } else {
        toggleLock([step], !vm.profile.name);
      }
    }

    ////////// Checks for each step (start) //////////
    /* STEP 2 */
    function isDomainInfoCompleted() {
      if(vm.isInfluencer()) {
        return vm.url.description && vm.categories.length;
      }
      return vm.url.href && vm.url.description && vm.categories.length;
    }

    /* STEP 3 */
    function isAnalyticsCompleted() {
      return bothSetOrEmpty(vm.profile.web_content.href, vm.profile.web_content.price);
    }

    /* STEP 4 */
    function areSocialHandlesCompleted() {
      var flag = [true, true, true, true, true];

      lodash.map(vm.profile.facebook_handles, function (fb) {
        if (emptyFields(fb.url, fb.price)) {
          flag[0] = false;
        }
      });

      lodash.map(vm.profile.twitter_handles, function (tw) {
        if (emptyFields(tw.url, tw.price)) {
          flag[1] = false;
        }
      });

      lodash.map(vm.profile.youtube_handles, function (yt) {
        if (emptyFields(yt.url, yt.price)) {
          flag[2] = false;
        }
      });

      lodash.map(vm.profile.instagram_handles, function (inst) {
        if (emptyFields(inst.url, inst.price)) {
          flag[3] = false;
        }
      });

      lodash.map(vm.profile.pinterest_handles, function (pin) {
        if (emptyFields(pin.url, pin.price)) {
          flag[4] = false;
        }
      });

      return lodash.some(flag);
    }

    ////////// Checks for each step (end) //////////
    function emptyFields(val1, val2) {
      return !(val1 || val2);
    }

    function bothSetOrEmpty(value1, value2) {
      return (value1 && value2) || emptyFields(value1, value2);
    }

    function getMetaDescription() {
      ProfilesService.getMetaDescription(vm.url.href, function (response) {
        vm.url.description = response.data.description;
      }, function (response) {
        toastr.error(response.data.error);
      });
    }

    /* STEP 3 */
    function checkGAMinValues(display) {
      if (vm.profile.web_content.href) {
        if (vm.web_content.muv < MIN_VALUES.GA) {
          showAlert('Minimum monthly unique visitors', 'We apologize but we need a minimum of 3000 unique visitors ' +
            'per month to participate in this product. You currently do not meet ' +
            'that criteria. When you do reach that traffic, you can come back to ' +
            'this profile and submit it then.', 'Ok I understand').then(function (result) {
            toggleLock([3, 4], false);
            vm.profile.web_content.href = '';
            vm.profile.web_content.price = '';
          });
        } else {
          $scope.followers = vm.web_content.muv;
          $scope.network = 'GA';
          $scope.rowHeader = 'Suggested Pricing for Sponsored Web Content';
          $scope.content = 'Content will be disclosed and will have no-follow links. ' +
            'Should be placed on front page for best performance.';
          $scope.contentLineTwo = 'Content must be up 6 months or longer.';
          $scope.header = 'Monthly Visitors';
          if (display) {
            displayRowGridPrice();
          }
        }
      } else {
        vm.web_content.muv = '';
        getPricingLabelGA('GA');
      }
    }

    function showPriceSEO() {
      $mdDialog.show({
        templateUrl: 'app/pages/profiles/newProfile/partials/gridDialog.html',
        parent: $rootElement,
        scope: $scope,
        controller: 'NewProfileController',
        preserveScope: true,
        clickOutsideToClose: false,
        fullscreen: true
      });
    }

    /* STEP 4 */
    function getHandleFromType(type) {
      var handles = [];
      switch (type) {
      case SOCIAL.Facebook:
        handles = vm.profile.facebook_handles;
        break;
      case SOCIAL.Twitter:
        handles = vm.profile.twitter_handles;
        break;
      case SOCIAL.Youtube:
        handles = vm.profile.youtube_handles;
        break;
      case SOCIAL.Instagram:
        handles = vm.profile.instagram_handles;
        break;
      case SOCIAL.Pinterest:
        handles = vm.profile.pinterest_handles;
        break;
      }
      return handles;
    }

    function checkSocialMinValues(type, socialMedia, display) {
      if (!socialMedia.url) {
        socialMedia.followers = '';
        getPricingLabel(socialMedia, type);
      } else {
        var handles = getHandleFromType(type);
        var pos = lodash.findIndex(handles, ['$$hashKey', socialMedia.$$hashKey]);

        if (socialMedia.followers < MIN_VALUES.SOCIAL) {
          showAlert('Minimum followers', 'We apologize but we need a minimum of 1000 followers for ' +
            'EACH social handle for you to participate in this product. You ' +
            'currently do not meet that criteria for this handle. When you do ' +
            'reach that engagement, you can come back to this profile and submit it then.', 'Ok I understand').then(function (result) {
            toggleLock([3, 4], false);
            handles[pos].url = '';
            handles[pos].price = '';
          });
        } else {
          $scope.followers = socialMedia.followers;
          $scope.network = type;
          $scope.rowHeader = typeIsFB(type) ? 'Suggested Pricing for Facebook & Twitter' : 'Suggested Pricing for Instagram & YouTube';
          $scope.content = 'For your protection, this content will be disclosed per FTC guidelines.';
          $scope.contentLineTwo = '';
          $scope.header = 'Followers';
          if (display) {
            displayRowGridPrice();
          }
        }
      }
    }

    function typeIsFB(type) {
      return lodash.includes([SOCIAL.Facebook, SOCIAL.Twitter, SOCIAL.Pinterest], type);
    }

    function noSocialHandlesSet(ev, tabSelection) {
      if (!socialMediaProduct()) {
        var confirm = $mdDialog.confirm()
          .title('Are you SURE?!?!')
          .textContent('If you do not submit social handles, you will NOT be able to monetize Tweets and Posts!')
          .targetEvent(ev)
          .ok('Ok I\'ll Do it Now!')
          .cancel('No Thanks.');

        $mdDialog.show(confirm).then(function (result) {
          vm.selectedIndex = 3;
        }, function () {
          if (!tabSelection) {
            findNextIndex();
          }
        });
      } else {
        findNextIndex();
      }
    }

    function getCurrentStepName() {
      return vm.stepNames[vm.selectedIndex];
    }

    function next(ev) {
      switch (vm.selectedIndex) {
      case 0:
        vm.showTypeError = !typeSelected();
        vm.showMissingContentTypeError = !contentTypeSelected();
        if(vm.isBlogger() && (vm.url.href === '' || !vm.domainUrlValid)) {
          break;
        }
        checkAvatar(ev, false);
        break;
      case 1:
        findNextIndex();
        break;
      case 2:
        noWebContentSet(ev, false);
        break;
      case 3:
        noSocialHandlesSet(ev, false);
        setPriceLabel();
        break;
      }
    }

    function previous() {
      var pos = lodash.findLastIndex(vm.stepsLocker, function (step, index) {
        return !step && index < vm.selectedIndex;
      });
      vm.selectedIndex = Math.max(pos, 0);
    }

    $scope.$on('tab-change', function (event, tabEvent) {
      // TODO: Do nothing when tabs are clicked.
      tabEvent.reject();
    });

    function noWebContentSet(ev, tabSelection) {
      if (!vm.profile.web_content.href) {
        var confirm = $mdDialog.confirm()
          .title('Please Select Analytics Account!')
          .textContent('By selecting an Google Analytics account, you are only authorizing Scalefluence to validate your monthly traffic.' +
            ' This is required for our Sponsored Content product.')
          .targetEvent(ev)
          .ok('Ok Got It!')
          .cancel('Move to next step');

        $mdDialog.show(confirm).then(function (result) {
          vm.selectedIndex = 2;
        }, function () {
          if (!tabSelection) {
            findNextIndex();
          }
        });
      } else {
        findNextIndex();
      }
    }

    /* STEP 5 */
    function showSEODialog() {
      showAlert('Minimum Values', 'We apologize but we need a minimum MOZ Domain Authority ' +
        'score of 15. And a minimum of of Majestic Trust Flow score of 5 for ' +
        'you to participate in this product. You currently do not meet one, ' +
        'or both, of these criteria. When you do reach these targets, you can ' +
        'come back to this profile and submit it then.', 'Ok I understand').then(function (result) {
        return false;
      });
    }

    function checkSEO(form) {
      var regex = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
      var valid = regex.test(vm.url.href);
      vm.domainUrlValid = valid;

      if (form.domainUrl) {
        form.domainUrl.$setValidity('url', valid);
      } else {
        form.url.$setValidity('url', valid);
      }

      if (valid) {
        if (vm.oldUrl != vm.url.href) {
          vm.oldUrl = vm.url.href;
          vm.seoFirstTime = true;
          vm.disableSEO = false;
          vm.canContinue = false;
        }

        if (vm.seoFirstTime) {
          ProfilesService.getDomainInformation(vm.url.href, function (response) {
            vm.domInfo = response.data;
            if (vm.domInfo.trust_flow < MIN_VALUES.TF ||
              vm.domInfo.domain_authority < MIN_VALUES.DA) {
              toggleLock([3, 4], false);
              vm.url.price = '';
              vm.disableSEO = true;
              vm.canContinue = false;
            } else {
              vm.canContinue = true;
              setPriceLabel();
            }
          }, function (response)  {
            return true;
          });
        } else {
          setPriceLabel();
        }
      }
    }

    function lastStep() {
      return vm.selectedIndex == TABS || (vm.selectedIndex == 3 && isInfluencer());
    }

    function submit() {
      // Check if there are social handles repeated
      if (socialHandlesRepeated()) {
        showAlert('Repeated', 'You are submitting the same Social handle. ' +
          'We do not allow duplicate handles. Please submit a different handle.', 'Got it').then(function (result) {
          return false;
        });
      } else if (oneProductIsSet()) {
        // If there is at least on product set then check if there are categories selected
        var influencer = isInfluencer();
        vm.noCategories = vm.categories.length == 0;
        vm.showHomePageError = !vm.url.allow_home_page && !influencer;
        vm.showFollowLinkError = !vm.url.follow_link && !influencer;
        vm.showAllowBlogPostError = !vm.url.allow_blog_post && !influencer;
        if (!vm.noCategories && !vm.showHomePageError && !vm.showFollowLinkError) {
          if (influencer) {
            continueSubmit();
          } else if (isBroker() || isBlogger() || isContributor()) {
            vm.canContinue ? continueSubmit() : showSEODialog();
          }
        } else if (isContributor() || isBroker()) {
          $anchorScroll('scrollHere');
        }
      } else {
        showAlert('We are sorry', 'Please accept our sincerest apologies. But your domain ' +
          'and social handles do not meet minimum standards set by our Agency ' +
          'Partners. These are: *Web Traffic = 3000 / month, Social Following ' +
          '= 1000 followers and Domain Strength = DA of 15 and TF of 5.', 'Ok I understand').then(function (result) {
          return false;
        });
      }
    }

    function continueSubmit() {
      vm.profile.categories_ids = lodash.map(vm.categories, 'id');
      vm.profile.urls = [];
      if (!isInfluencer()) {
        vm.profile.urls.push(vm.url);
      } else {
        SocialHandlesService.setDescriptionToSocialHandles(vm.profile, vm.url.description);
      }
      ProfilesService.createProfile(vm.profile, function (response) {
        if (isInfluencer()) {
          showAlert('Take me live', 'Please note that we will need to vet your Social Handles ' +
            'through email to you. If you do not respond via email, then we will ' +
            'not approve your Profile. This means we will not be sending you any work.', 'OK. I understand ' +
            'and will respond via email.').then(function (result) {
            toastr.success('Successfully created the profile!');
            RoutesService.dashboard();
          });
        } else if (isBroker()) {
          showAlert('Submit', 'As a broker, you will be vetted by email. If you do ' +
            'not respond by email, we will not approve of any submissions.', 'OK. I understand ' +
            'and will respond via email.').then(function (result) {
            toastr.success('Successfully created the profile!');
            RoutesService.dashboard();
          });
        } else {
          toastr.success('Successfully created the profile!');
          RoutesService.dashboard();
        }
      }, function (response) {
        handleErrors(response.data.errors);
      });
    }

    function handleErrors(errors) {
      lodash.forEach(errors, function (error) {
        toastr.error(error[0]);
      });
    }

    function socialHandlesRepeated() {
      return SocialHandlesService.anyRepeated(vm.profile);
    }

    function oneProductIsSet(checkStep) {
      var webProduct = vm.profile.web_content.href && vm.profile.web_content.price;
      var socialProduct = socialMediaProduct();
      var seoProduct = vm.url.price;
      switch (checkStep) {
      case 3:
        if (!webProduct && !isBroker()) {
          showWarningPopup(checkStep);
        }
        break;
      case 4:
        if (!socialProduct && !isBroker() && !isContributor() && vm.profile.image) {
          showWarningPopup(checkStep);
        }
        break;
      default:
        return webProduct || socialProduct || seoProduct;
      }
    }

    function showWarningPopup(checkStep) {
      var confirm = $mdDialog.confirm()
        .title('Are you SURE?')
        .textContent('Skipping this step means you will earn less money with us!')
        .ok('Wait! Take me back!')
        .cancel('I understand');

      $mdDialog.show(confirm).then(function (result) {
        previous();
      }, function () {
        vm.selectedIndex = checkStep;
      });
    }

    function showInfo(step) {
      switch (step) {
      case 3:
        return showAlertHTML('Sponsored Web Content', 'Sponsored Web Content will read more like featured content. ' +
          'You will need to disclose compensation for the content per FTC regulations. ' +
          'Links inside the content will be NO-FOLLOW. You will have editorial control as well.' +
          '<br>' + '<br>' + 'By supplying your Google Analytics username below, our technology can see how ' +
          'many people engaged with each blog post. We will not be retrieving any ' +
          'private data from your Analytics account. Only the number of monthly ' +
          'unique visitors coming to your blog and previous content placements done through Scalefluence.', 'Ok').then(function (result) {
          return false;
        });
      case 4:
        return showAlertHTML('Sponsored Social Content', 'Sponsored Social Content is featured content on your social handle. ' +
          'You will need to disclose compensation for the content per FTC regulations ' +
          'by using #Ad or #Sponsored hashtags. Content will also come with a Tiny URL ' +
          'to track clicks and engagement of your Social Post. ' +
          '<br>' + '<br>' + 'By supplying your Social handles below, our technology can see how many ' +
          'people engaged with the social post. We will not be retrieving any private ' +
          'data of your account. Only the number of followers and previous content ' +
          'placements done through Scalefluence.', 'Ok').then(function (result) {
          return false;
        });
      case 51: // Case 5.1
        return showAlertHTML('Allow Home Page Links?', "Home page links are, on the whole, branded anchor text. " +
          "This means including the brand name in the link. For example: According to Forbes. " +
          " With ' Forbes ' being the anchor text. ", 'Ok').then(function (result) {
          return false;
        });
      case 52: // Case 5.2
        return showAlertHTML('Are Links Do-Follow?', 'Do-Follow links are purchased about 5 times more than ' +
          'No-Follow links. You can change this selection at any time after ' +
          'our team approves your site.', 'Ok').then(function (result) {
          return false;
        });
      case 53: // Case 5.3
        return showAlertHTML('I only allow links to blog posts', 'While linking to blog content is the best practice; ' +
          'SEO firms will request links to service pages. ' + "Even if you select 'yes', you will still have the ability  " +
          'to accept (or decline) content that links elsewhere. Upon order, you can determine if the link is ' +
          'appropriate and request changes.', 'Ok').then(function (result) {
          return false;
        });
      case 54: // Case 5.
        return showAlertHTML('Required article length?', 'Please give the amount of words you require per article. Thank you!', 'Ok').then(function (result) {
          return false;
        });
      }

      $mdDialog.show(confirm).then(function (result) {
        return false;
      });
    }

    function getPricingLabel(socialHandle, network) {
      return socialHandle.followers ? 'Suggested price: ' + PricingService.getPrice(socialHandle.followers, network) : 'Sign in to Submit Price';
    }

    function getPricingLabelGA(network) {
      return vm.web_content.muv ? 'Suggested price: ' + PricingService.getPrice(vm.web_content.muv, network) : 'Your price per post (USD):';
    }

    function socialMediaProduct() {
      return lodash.findIndex(SocialHandlesService.getAllSocialHandle(vm.profile), function (socialHandle) {
        if (socialHandle.url && socialHandle.price) {
          return true;
        }
      }) != -1;
    }

    function seoProduct() {
      return vm.url.href && vm.url.description && vm.url.price;
    }

    function getDescriptionTitle(tooltip) {
      if (vm.url.description && !tooltip && !isContributor()) {
        return "Description";
      } else if (isContributor()) {
        return 'Description: Describe what you write about. Be specific so that we can ' +
          'pitch you the right topics'
      }
      return "Description: Use \"My Meta Description\" or write your own." +
        "If you write your own, include friendly KWs. IE >> \"Health | Travel | Tech \" etc.";
    }

    function getFinishText() {
      return isInfluencer() && vm.selectedIndex == 3 ? 'Take Me Live!' : 'Finish';
    }

    function cantSubmit() {
      if (isBroker()) {
        return !seoProduct();
      } else if (isInfluencer()) {
        return !socialMediaProduct();
      } else if (isBlogger()) {
        return false;
      } else if (isContributor()) {
        return !vm.url.description || !vm.url.price;
      }
    }

    function getTileForInfluencersDescription() {
      return vm.influencer.description ? "Description" : "Description: Tell Us " +
        "About Yourself and Your Audience. What type of content best resonates?";
    }

    function setPriceLabel() {
      if (!vm.url.href) {
        vm.priceLabel = 'Your price per post (USD):';
      } else if (vm.domInfo && vm.domInfo.trust_flow >= MIN_VALUES.TF &&
        vm.domInfo.domain_authority >= MIN_VALUES.DA) {

        PricesService.getPrice(vm.url.href, function (response) {
          vm.priceLabel = 'Suggested price: ' + response.data.suggested_price;
        }, function (error) {
          $log.error(error);
          vm.priceLabel = 'Your price per post (USD):';
        });

        $rootScope.da = vm.domInfo.domain_authority;
        $rootScope.tf = vm.domInfo.trust_flow;
      } else {
        vm.priceLabel = 'Your price per post (USD):';
      }
    }

    function showWarningPopup() {
      showAlert('Suggested Pricing', 'In order to be able to display the suggested price, you must sign in to the social network', 'Got it!');
    }

    function showAlert(title, text, confirmText) {
      return $mdDialog.show(
        $mdDialog.alert()
        .title(title)
        .textContent(text)
        .escapeToClose(false)
        .ok(confirmText)
      );
    }

    function showAlertHTML(title, text, confirmText) {
      return $mdDialog.show(
        $mdDialog.alert()
        .title(title)
        .htmlContent(text)
        .escapeToClose(false)
        .ok(confirmText)
      );
    }
    
    function typeSelected() {
      // will be non-empty string if selected
      return !!vm.profile.profileType;
    }

    function setContentType(type) {
      vm.profile.contentType = type;
    }

    function contentTypeSelected() {
      // will be non-empty if selected
      return !!vm.profile.contentType;
    }

    function getYouTubeFollowers(socialHandle) {
      GoogleService.queryYouTubeStatistics(function (response) {
        if (response.result.items.length) {
          socialHandle.url = 'www.youtube.com/channel/' + response.result.items[0].id;
          socialHandle.followers = response.result.items[0].statistics.subscriberCount;
          checkSocialMinValues('youtube', socialHandle);
        } else {
          toastr.error('', 'No channel associated to your account.');
        }
      });
    }

    function authenticateTwitter(ev, socialHandle) {
      $mdDialog.show({
        controller: 'TwitterHandleController',
        controllerAs: 'ctrl',
        templateUrl: 'app/pages/profiles/newProfile/partials/twitterHandleDialog.html',
        targetEvent: ev,
        clickOutsideToClose: true
      }).then(function (response) {
        var handle = response.handle;
        if (handle) {
          var regex_twitter_profile = /(.*twitter\.com\/)?(.*)/
          var account = regex_twitter_profile.exec(handle)[2] || ''
          ProfilesService.getTwitterFollowersCount(account, function (response) {
            socialHandle.url = 'www.twitter.com/' + account;
            socialHandle.followers = response.data.count;
            checkSocialMinValues('twitter', socialHandle);
          }, function (error) {
            toastr.error(error.data.error);
          });
        }
      }, function () {
        $log.log('Dialog dismissed');
      });
    }

    function handleFacebookAuthentication(ev, socialHandle, socialNetwork) {
      FacebookService.authenticate(function (userId) {
        switch (socialNetwork) {
          case 'facebook':
            FacebookService.getFanPageData(function (response) {
              loadSocialHandle(socialHandle, socialNetwork, response.username, response.fan_count);
            }, function (error) {
              toastr.error(error);
            });
            break;
          case 'instagram':
            FacebookService.checkForInstagramBusinessAccount(function (response) {
              loadSocialHandle(socialHandle, socialNetwork, response.username, response.followers_count);
            }, function (error) {
              toastr.error(error);
            });
            break;
        }
      }, function (error) {
        toastr.error(error);
      });
    }

    function loadSocialHandle(socialHandle, socialNetwork, username, count) {
      socialHandle.url = 'https://www.' + socialNetwork + '.com/' + username;
      socialHandle.followers = count;
      checkSocialMinValues(socialNetwork, socialHandle);
    }

    function toggleCategories() {
      vm.categoriesOn = !vm.categoriesOn;
    }

    function categoriesActionMsg() {
      return vm.categoriesOn ? 'Hide Categories' : 'Show Categories';
    }

    function setUrlRequiredLength(requiredLength) {
      switch(requiredLength) {
        case '1500-+':
        case '1250-1500':
        case '1000-1250':
          vm.url.required_length = requiredLength;
          break;

        default:
          vm.url.required_length = '750-1000'
      }
    }

    function setUrlAllowHomepage(allow) {
      vm.url.allow_home_page = allow ? 'true' : 'false';
      if(allow) vm.url.allow_blog_post = 'false';
    }

    function setUrlFollowLink(isDoFollow) {
      vm.url.follow_link = isDoFollow ? 'true' : 'false';
    }

    function setUrlAllowBlogPost(allow) {
      vm.url.allow_blog_post = allow ? 'true' : 'false';
    }
  }
})();
