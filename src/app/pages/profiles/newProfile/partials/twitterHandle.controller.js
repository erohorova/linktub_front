(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('TwitterHandleController', TwitterHandleController);

  /* @ngInject */
  function TwitterHandleController($scope, $mdDialog) {
    var vm = this;

    //Accesible variables
    vm.twitterHandle = '';

    //Accesible functions
    vm.ok = ok;

    function ok() {
      var response = {
        handle: vm.twitterHandle
      };
      $mdDialog.hide(response);
    }
  }
})();
