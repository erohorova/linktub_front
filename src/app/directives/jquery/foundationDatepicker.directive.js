angular.module('linktub')
  .directive('foundationDatepicker', function () {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function (scope, elem, attrs, ngModel) {
        var options = {};
        options.format = 'mm/dd/yyyy';
        options.leftArrow = '<svg class="icon icon--datepicker-prev"><use xlink:href="assets/images/icons.svg#datepicker-prev"></use></svg>';
        options.rightArrow = '<svg class="icon icon--datepicker-next"><use xlink:href="assets/images/icons.svg#datepicker-next"></use></svg>';
        if(typeof(attrs.minDate) !== 'undefined') options.startDate = attrs.minDate;
        if(typeof(attrs.maxDate) !== 'undefined') options.endDate = attrs.maxDate;

        var dp = angular.element(elem).fdatepicker(options)
        .on('changeDate', function(ev){
          ngModel.$setViewValue(moment.utc((new Date(ev.date))).format('MM/DD/YYYY'));
          ngModel.$render();
        }).data('datepicker');
      }
    }
  });
