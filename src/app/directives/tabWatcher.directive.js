(function () {
  'use strict';

  angular
    .module('linktub')
    .directive('tabWatcher', function () {
      return {
        require: '^?mdTabs',
        restrict: 'A',
        link: function (scope, el, attrs, controllers) {
          var mdTabsCtrl = controllers;
          var origSelectFn = mdTabsCtrl.select;

          // overwrite original function with our own
          mdTabsCtrl.select = function (index, canSkipClick) {
            // emit an event with accept/reject functions
            scope.$emit('tab-change', {
              index: index,
              accept: function () {
                return origSelectFn(index, canSkipClick);
              },
              reject: function () {}
            });
          };

          scope.$watch(attrs.mdSelected, function () {
            if (scope.$eval(attrs.mdSelected) != 0) {
              scope.$emit('tab-change', {
                index: attrs.mdSelected,
                accept: function () {
                  return origSelectFn(scope.$eval(attrs.mdSelected), false);
                },
                reject: function () {}
              });
            }
          });
        }
      };
    });
})();
