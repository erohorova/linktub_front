(function () {
  'use strict';

  angular
    .module('linktub')
    .directive('urlInput', function ($filter, $browser) {
      return {
        require: ['^form', 'ngModel'],
        link: function ($scope, $element, $attrs, ngModelCtrl) {
          var regex = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}[^'"@^():;*]*$/;

          $element.bind('blur', function () {
            var val = $element.val();
            var valid = regex.test(val);
            ngModelCtrl[0][$element.attr('name')].$setValidity('pattern', val && valid);
          });
        }
      };
    });
  })();
