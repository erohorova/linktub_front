(function () {
  'use strict';

  angular
    .module('linktub')
    .directive('noCharactersAllowed', directive);

  /* @ngInject */
  function directive() {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function ($scope, $element, attrs, ngModel) {
        $element.bind('keydown', function (e) {
          e.preventDefault();
          return false;
        });
      }
    }
  }
})();
