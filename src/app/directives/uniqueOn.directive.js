(function () {
  'use strict';

  angular
    .module('linktub')
    .directive('uniqueOn', uniqueOn);

  /* @ngInject */
  function uniqueOn(lodash) {
    var directive = {
      restrict: 'A',
      link: linkFunc
    };

    return directive;

    function linkFunc(scope, el, attr) {
      scope.$watch(attr.ngModel, function (newVal) {
        if (angular.isUndefined(newVal)) {
          return;
        }
        var items = scope.$eval(attr.uniqueOn) || [];
        var form = scope.form;
        var valid = lodash.findIndex(items, function (item) {
          return item.toUpperCase() == newVal.toUpperCase()
        }) == -1;
        form.name.$setValidity('repeated', valid);
      });
    }
  }
})();
