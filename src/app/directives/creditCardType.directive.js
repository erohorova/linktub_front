(function () {
  'use strict';

  angular
    .module('linktub')
    .directive('ngCreditCardType', ngCreditCardType);

  /* @ngInject */
  function ngCreditCardType($interpolate) {
    var directive = {
      restrict: 'A',
      require: 'ngModel',
      link: linkFunc
    };

    return directive;

    function linkFunc(scope, el, attr, ctrl) {
      ctrl.$parsers.unshift(function (value) {
        scope.vm.ccType =
          (/^5[1-5]/.test(value)) ? {
            network: 'MasterCard',
            digits: 16,
            logo: 'https://assets.braintreegateway.com/payment_method_logo/mastercard.png'
          } :
          (/^4/.test(value)) ? {
            network: 'Visa',
            digits: 16,
            logo: 'https://assets.braintreegateway.com/payment_method_logo/visa.png'
          } :
          (/^3[47]/.test(value)) ? {
            network: 'Amex',
            digits: 15,
            logo: 'https://assets.braintreegateway.com/payment_method_logo/american_express.png'
          } :
          (/^6011|65|64[4-9]|622(1(2[6-9]|[3-9]\d)|[2-8]\d{2}|9([01]\d|2[0-5]))/.test(value)) ? {
            network: 'Discover',
            digits: 16,
            logo: 'https://assets.braintreegateway.com/payment_method_logo/discover.png'
          } :
          (/^2131|1800|35/.test(value)) ? {
            network: 'JCB',
            digits: 16,
            logo: 'https://assets.braintreegateway.com/payment_method_logo/jcb.png'
          } :
          undefined
        ctrl.$setValidity('invalid', !!scope.vm.ccType)
        return value;
      })
    }
  }
})();
