angular.module('linktub')
  .directive('searchChip', function () {
    return {
      restrict: 'A',
      link: function (scope, elem, attrs) {
        var chipTemplateClass = elem.name;
        var mdChip = elem.parent().parent();
        mdChip.addClass(scope.$chip.category);
      }
    }
  });
