(function () {
  'use strict';

  angular
    .module('linktub')
    .directive('noDoubleQuotes', directive);

    /* @ngInject */
  function directive() {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function ($scope, $element, attrs, ngModel) {
        attrs.ngTrim = 'false';

        ngModel.$parsers.unshift(function (value) {
          var singleQuoteValue = value.replace(/"/g, "'");

          if (singleQuoteValue !== value) {
            ngModel.$setViewValue(singleQuoteValue);
            ngModel.$render();
          }

          return singleQuoteValue;
        });
      }
    }
  }
})();
