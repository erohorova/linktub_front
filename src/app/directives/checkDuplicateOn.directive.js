(function () {
  'use strict';

  angular
    .module('linktub')
    .directive('checkDuplicateOn', checkDuplicateOn);

  /* @ngInject */
  function checkDuplicateOn(lodash) {
    var directive = {
      restrict: 'A',
      link: linkFunc
    };

    return directive;

    function linkFunc(scope, el, attr) {
      scope.$watch(attr.ngModel, function (newVal) {
        var items = scope.$eval(attr.checkDuplicateOn) || [];
        var form = scope.$eval(attr.over);
        var position = index(items, newVal, scope.$index);
        var valid = items.length < 2 || position < 0 || position == scope.$index;
        form.url.$setValidity('duplicate', valid);
      });
    }

    function index(items, newVal, pos) {
      for (var i = 0; i < items.length; i++) {
        if (items[i].url && items[i].url === newVal && pos !== i) {
          return i;
        }
      }
      return -1;
    }
  }
})();
