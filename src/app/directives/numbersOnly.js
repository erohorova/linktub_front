(function () {
  'use strict';

  angular
    .module('linktub')
    .directive('numbersOnly', function () {
      return {
        require: 'ngModel',
        link: function ($scope, $element, $attr, ngModelCtrl) {
          function fromUser(text) {
            if (text) {
              var cleanText = text.replace(/[^0-9]/g, '');

              if (parseInt(cleanText) <= 0) {
                cleanText = '';
              }

              cleanText = cleanText.slice(0, 6);

              if (!angular.equals(text, cleanText)) {
                ngModelCtrl.$setViewValue(cleanText);
                ngModelCtrl.$render();
              }
              return cleanText;
            }
            return '';
          }
          ngModelCtrl.$parsers.push(fromUser);
        }
      };
    });
})();
