(function () {
  'use strict';

  angular
    .module('linktub')
    .directive('checkDomain', checkDomain);

  /* @ngInject */
  function checkDomain(lodash) {
    var directive = {
      restrict: 'A',
      link: linkFunc
    };

    return directive;

    function linkFunc(scope, el, attr) {
      scope.$watch(attr.ngModel, function (newVal) {
        var domain = attr.domain + '.com';
        var form = scope.$eval(attr.over);
        var valid = (newVal !== '' && newVal.indexOf(domain) != -1) || newVal == '';
        form.url.$setValidity('domain', valid);
      });
    }
  }
})();
