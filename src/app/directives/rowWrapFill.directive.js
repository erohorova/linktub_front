(function () {
  'use strict';

  angular
    .module('linktub')
    .directive('rowWrapFill', rowWrapFill);

  /* @ngInject */
  function rowWrapFill($compile, $window) {
    var directive = {
      restrict: 'EA',
      scope: {},
      link: linkFunc
    };

    return directive;

    function linkFunc(scope, elm, attrs) {
      if (scope.$parent.$last) {
        var parentWidth = elm[0].parentNode.clientWidth || 1234;
        var childWidth = elm[0].clientWidth || 196;
        var length = scope.$parent.$index + 1;

        if (length > 0) {
          var theStyle = $window.getComputedStyle(elm[0], null);

          //If there is a border - recalculate child width
          if (theStyle.boxSizing == 'border-box') {
            var border = theStyle.borderWidth;
            border = parseInt(border.replace('px', ''));
            childWidth += border * 2;
          }

          var maxColumn = Math.floor(parentWidth / childWidth);

          //Only add elements if there is more than one row.
          if (length > maxColumn) {
            //Calculate numder of elements to add
            var temp = length % maxColumn
            if (temp != 0) {
              var toAdd = Math.abs(maxColumn - temp);

              for (var i = 0; i < toAdd; i++) {
                var blankNode = '<div style="width: ' + childWidth + 'px;box-sizing: border-box;"></div>';

                //var node = $compile(blankNode)(scope)
                elm.parent().append(blankNode)
              }
            }
          }
        }
      }
    }
  }
})();
