(function () {
  'use strict';

  angular
    .module('linktub', [
      'ngAnimate',
      'ngCookies',
      'ngSanitize',
      'ngMessages',
      'ngAria',
      'ngResource',
      'ui.router',
      'ngMaterial',
      'ngFileUpload',
      'toastr',
      'linktubConfig',
      'ngLodash',
      'dibari.angular-ellipsis',
      'textAngular',
      'angularUtils.directives.dirPagination',
      'linktub.pages',
      'md.data.table',
      'rzModule',
      'ui.mask',
      'LocalStorageModule',
      'material.components.expansionPanels',
      'ui.utils.masks',
      'ngImgCrop'
    ])
})();
