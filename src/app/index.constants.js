/* global malarkey:false, moment:false */
(function () {
  'use strict';

  angular
    .module('linktub')
    .constant('LINK_STATE', {
      NEW_LINK: 'new_link',
      APPROVED: 'approved',
      SUSPENDED: 'suspended'
    })
    .constant('ERROR_CODES', ['81717', '81715'])
    .constant('POLL_TIME', 6000)
    .constant('TYPE_STATE', {
      ACTIVE_CONTENT: 'active_content',
      AWAITING_PUBLICATION: 'awaiting_publication',
      APPROVED_URL: 'approved_url',
      REJECTED_CONTENT: 'rejected_content',
      REJECTED_OR_CANCELLED_CONTENT: 'rejected_or_cancelled_content',
      ACTIVE_ADS: 'active_ads',
      NEW_ORDERS: 'new_orders',
      PRICING_AND_PAUSING: 'pricing_and_pausing',
      RESUBMITTED_CONTENT: 'resubmitted_content',
      QUOTES: 'quotes',
      ARTICLES: 'articles',
      MESSAGES: 'messages'
    })
    .constant('KEY_CODES', {
      ENTER: 13
    })
    .constant('PAGINATION_SIZE', {
      MAX_SIZE: 6,
      PER_PAGE: 10
    })
    .constant('MASK', {
      SEVEN: '*******',
      SIX: '******'
    })
    .constant('ORDER_ORIGIN', {
      CART: 'cart',
      CONTENT: 'content_done'
    })
    .constant('ORDER_STATES', {
      CANCELLED: 'cancelled'
    })
    .constant('CHECKOUT', 'checkout')
    .constant('ROLE', {
      ADV: 'advertiser',
      PUB: 'publisher'
    })
    .constant('PROFILE_TYPE', {
      BLOGGER: 'blogger',
      INFLUENCER: 'influencer',
      BROKER: 'broker',
      CONTRIBUTOR: 'contributor'
    })
    .constant('HTTP_STATUS', {
      BAD_REQUEST: '400',
      UNAUTHORIZED: '401',
      FORBIDDEN: '403',
      NOT_FOUND: '404'
    })
    .constant('ERRORS_RESPONSES', {
      ALREADY_TAKEN: 'Validation failed: Href has already been taken',
      INVALID: 'Validation failed: Href is an invalid URL'
    })
    .constant('SESSION', {
      NAME: 'session',
      EXP: 30
    })
    .constant('SOCIAL', {
      Facebook: 'facebook',
      Twitter: 'twitter',
      Youtube: 'youtube',
      Instagram: 'instagram',
      Pinterest: 'pinterest'
    })
    .constant('MIN_VALUES', {
      GA: 3000,
      SOCIAL: 1000,
      DA: 15,
      TF: 10
    }).constant('PUB_URL_STATE', {
      PENDING: 'pending',
      PAUSED: 'paused',
      ACTIVE: 'active',
      REJECTED: 'rejected',
      RESUBMITTED: 'resubmitted',
      CANCELLED: 'cancelled',
      APPROVE_PUB: 'approved_by_publisher',
      APPROVE: 'approved',
      REJECTED_ADMIN: 'rejected_by_admin',
      UNDER_REVIEW: 'under_review'
    }).constant('Assets', 'assets/files/')
    .constant('ARTICLE_STATES', {
      DRAFT: 'draft',
      ASSIGNED: 'assigned',
      COMPLETED: 'completed',
      ADMIN_REJECTED: 'admin_rejected',
      AUTHOR_REJECTED: 'author_rejected',
      AGENCY_REJECTED: 'agency_rejected',
      APPROVED: 'admin_approved',
      CANCELLED: 'cancelled',
    })
    .constant('BLOG_POST_STATES', {
      CANCELLED: 'cancelled',
    })
    .constant('REGEX', {
      URL: '(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})'
    })
    .constant('SCOPE', {
      ANALYTICS: 'https://www.googleapis.com/auth/analytics.readonly',
      YOUTUBE: 'https://www.googleapis.com/auth/youtube.readonly'
    })
    .constant('AUTHOR_QUOTE', {
      SF: 'scalefluence',
      AGENCY: 'agency'
    })
    .constant('INIT_DATE', new Date(2016, 9, 1))
    .constant('ADV_STATES', ['cart', 'adv-dash', 'search', 'new-campaign',
      'all-campaigns', 'quotes-main', 'done-articles', 'faq-adv', 'advertiser'
    ])
    .constant('PUB_STATES', ['pub-dash', 'new-profile', 'all-profiles', 'payouts',
      'faq-pub', 'pub'
    ])
    .constant('MIN_PASS_SIZE', 7)
    .constant('ORDER_CREATED_ORIGIN', {
      QUOTE: 'quote',
      SEARCH: 'search',
      ARTICLE: 'article'
    })
    .constant('MESSAGE_STATE', {
      SENT: 'sent',
      READ: 'read',
      UNREAD: 'unread'
    })
    .constant('CAMPAIGN_STATE', {
      ACTIVE: 'active',
      REJECTED: 'rejected'
    })
    .constant('TOSTATE', 'toState');
})();
