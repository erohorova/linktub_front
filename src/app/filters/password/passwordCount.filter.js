(function () {
  'use strict';

  angular
    .module('linktub')
    .filter('passwordCount', function (MIN_PASS_SIZE) {
      return function (value, peak) {
        value = angular.isString(value) ? value : '';
        peak = isFinite(peak) ? peak : MIN_PASS_SIZE;

        return value && (value.length > peak ? peak + '+' : value.length);
      };
    });
})();
