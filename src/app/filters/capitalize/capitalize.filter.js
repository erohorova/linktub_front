(function () {
  'use strict';

  angular
    .module('linktub')
    .filter('capitalize', function () {
      return function (token) {
        if (token) {
          return token.charAt(0).toUpperCase() + token.slice(1);
        }
      }
    });
})();
