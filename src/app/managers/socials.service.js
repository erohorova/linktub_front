(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('SocialHandlesService', SocialHandlesService);

  /* @ngInject */
  function SocialHandlesService(lodash) {
    var service = {
      anyChange: anyChange,
      anyRepeated: anyRepeated,
      getAllSocialHandle: getAllSocialHandle,
      setDescriptionToSocialHandles: setDescriptionToSocialHandles
    };

    return service;

    function anyChange(profile, profileCopy) {
      return !arraysEqual(getAllSocialHandle(profile).sort(), getAllSocialHandle(profileCopy).sort());
    }

    function anyRepeated(profile) {
      var allSocialHandles = getAllSocialHandle(profile);
      return lodash.uniqWith(allSocialHandles, function (i1, i2) {
        return i1.url && i2.url && i1.url == i2.url
      }).length !== allSocialHandles.length;
    }

    function getAllSocialHandle(profile) {
      return lodash.concat(
        profile.facebook_handles,
        profile.twitter_handles,
        profile.instagram_handles,
        profile.youtube_handles,
        profile.pinterest_handles
      );
    }

    function setDescriptionToSocialHandles(profile, description) {
      lodash.each(getAllSocialHandle(profile), function (sh) {
        sh.description = description;
      });
    }

    function arraysEqual(a, b) {
      if (a === b) return true;
      if (a == null || b == null) return false;
      if (a.length != b.length) return false;

      for (var i = 0; i < a.length; ++i) {
        if (a[i].url !== b[i].url) return false;
      }
      return true;
    }
  }
})();
