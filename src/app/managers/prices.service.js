(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('PricesService', PricesService);

  /* @ngInject */
  function PricesService($http, EnvironmentConfig) {
    var SUGGEST_PRICE_URL = EnvironmentConfig.API_END_POINT + '/urls/suggested_price'
    var service = {
      getPrice: getPrice
    };

    return service;

    function getPrice(href, successCallback, errorCallback) {
      $http.get(SUGGEST_PRICE_URL, {
        params: {
          href: href
        }
      }).then(successCallback, errorCallback);
    }
  }
})();
