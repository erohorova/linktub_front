(function () {
  'use strict';

  angular
    .module('linktub')
    .run(runBlock);

  /** @ngInject */
  function runBlock($rootScope, $state, $log, $mdDialog, lodash, toastr, TOSTATE, CounterService, RoutesService, SessionService, StorageService, AdBlockerService, EnvironmentConfig) {

    const SIGNIN_STATE = 'sign-in';

    var originalUrl = window.location.href;
    var showingDialog = false;

    // Start the poller for notifications
    CounterService.poller();

    // listen for the event in the relevant $scope
    $rootScope.$on('unauthorized', function (event) {
      RoutesService.signIn();
    });

    // Redirect to login if route requires auth and you're not logged in
    var stateChangeStart = $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
      var token = getParamByName('token', originalUrl);
      var role = getParamByName('role', originalUrl);

      if (token && role && toState.name == SIGNIN_STATE) {
        originalUrl = '';
        SessionService.clearSession();
        event.preventDefault();
        SessionService.forceSessionFromAdmin(token, role);
        RoutesService.dashboard();
      }

      // Users not logged may not access "authentication required" pages
      if (!SessionService.isloggedIn() && isLoggedInRequired(toState.name)) {
        event.preventDefault();

        if (toState.name != SIGNIN_STATE) {
          StorageService.set(TOSTATE, toState);
        }
        toastr.warning('You must sign in before continue.');
        return $state.go(SIGNIN_STATE);
      } else if (toState.authentication.required && !toState.authentication.permittedRoles.includes(SessionService.currentRole())) {
        event.preventDefault();
        RoutesService.dashboard();
        toastr.warning('You don\'t have permission to continue.');
      }

      // Users are prevented to go back to landing page once they are signed in
      if (toState.name == SIGNIN_STATE && SessionService.isloggedIn()) {
        event.preventDefault();
        RoutesService.dashboard();
      }

      AdBlockerService.adBlockerEnabled(function (blocked) {
        if (blocked && !showingDialog) {
          showingDialog = true;
          showDialog('Ad Blocker detected', 'WARNING! Our software requires you to disable your ad block on your browser. Please whitelist our entire website. Thank you!', 'OK, I understand')
          .then(function () {
            showingDialog = false;
          });
        }
      });
    });

    // Private functions

    function isLoggedInRequired(name) {
      return !lodash.includes(['landing', 'sign-up', 'sign-in'], name)
    }

    function getParamByName(name, url) {
      if (!url) url = window.location.href;
      name = name.replace(/[\[\]]/g, "\\$&");
      var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    function removeParamByName(key, url) {
      if (!url) url = window.location.href;
      var rtn = url.split("?")[0],
        param,
        params_arr = [],
        queryString = (url.indexOf("?") !== -1) ? url.split("?")[1] : "";
      if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
          param = params_arr[i].split("=")[0];
          if (param === key) {
            params_arr.splice(i, 1);
          }
        }
        rtn = rtn + "?" + params_arr.join("&");
      }
      return rtn;
    }

    function showDialog(title, text, confirmText) {
      return $mdDialog.show(
        $mdDialog.alert()
        .title(title)
        .textContent(text)
        .escapeToClose(false)
        .ok(confirmText)
      );
    }

    window.fbAsyncInit = function() {
      FB.init({
        appId: EnvironmentConfig.FACEBOOK_APP_ID,
        status: true,
        cookie: true,
        xfbml: true,
        version: 'v3.2'
      });
    };

    (function(d){
      // load the Facebook javascript SDK

      var js,
      id = 'facebook-jssdk',
      ref = d.getElementsByTagName('script')[0];

      if (d.getElementById(id)) {
        return;
      }

      js = d.createElement('script');
      js.id = id;
      js.async = true;
      js.src = "//connect.facebook.net/en_US/all.js";
      js.target = "_top";

      ref.parentNode.insertBefore(js, ref);

    }(document));
  }
})();
