(function () {
  'use strict';

  angular
    .module('linktub')
    .config(config)

  /** @ngInject */
  function config($logProvider, $provide, toastrConfig, $httpProvider, $mdThemingProvider,
    paginationTemplateProvider, localStorageServiceProvider) {
    // Enable log
    $logProvider.debugEnabled(true);

    // Setup local-storage for cookies
    localStorageServiceProvider.setPrefix('linktub');
    localStorageServiceProvider.setStorageCookie(45, '/', true);

    // Setup toastr
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 5000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.closeButton = true;
    toastrConfig.progressBar = true;
    toastrConfig.newestOnTop = true;

    // Setup interceptor for $http
    $httpProvider.interceptors.push('AuthInterceptor');

    // Setup pagination template
    paginationTemplateProvider.setPath('app/components/paging/paging.html');

    // Setup theme
    $mdThemingProvider.definePalette('neonorange', {
      '50': 'ffede0',
      '100': 'ffd1b3',
      '200': 'ffb380',
      '300': 'ff944d',
      '400': 'ff7d26',
      '500': 'ff6600',
      '600': 'ff5e00',
      '700': 'ff5300',
      '800': 'ff4900',
      '900': 'ff3800',
      'A100': 'ffffff',
      'A200': 'ff6600',
      'A400': 'ffc9bf',
      'A700': 'ffb4a6',
      'contrastDefaultColor': 'dark',
      'contrastDarkColors': [
        '50',
        '100',
        '200',
        '300',
        '400',
        '500',
        '600',
        'A100',
        'A200',
        'A400',
        'A700'
      ],
      'contrastLightColors': [
        '700',
        '800',
        '900'
      ]
    });

    $mdThemingProvider.definePalette('neonblue', {
      '50': 'e0edf3',
      '100': 'b3d1e1',
      '200': '80b2cd',
      '300': '4d93b8',
      '400': '267ca9',
      '500': '00659a',
      '600': '005d92',
      '700': '005388',
      '800': '00497e',
      '900': '00376c',
      'A100': '9cc6ff',
      'A200': '69a9ff',
      'A400': '368cff',
      'A700': '1c7eff',
      'contrastDefaultColor': 'light',
      'contrastDarkColors': [
        '50',
        '100',
        '200',
        '300',
        'A100',
        'A200'
      ],
      'contrastLightColors': [
        '400',
        '500',
        '600',
        '700',
        '800',
        '900',
        'A400',
        'A700'
      ]
    });

    // // Use that theme for the primary intentions
    $mdThemingProvider.theme('default')
      .primaryPalette('neonblue')
      .accentPalette('neonorange');

    // Setup actions for textAngular
    $provide.decorator('taOptions', ['$delegate', function (taOptions) {
      taOptions.forceTextAngularSanitize = true;
      taOptions.keyMappings = [];
      taOptions.toolbar = [
        ['html', 'h1', 'h2', 'h3', 'p', 'pre', 'quote'],
        ['bold', 'italics', 'underline', 'ul', 'ol', 'clear'],
        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
        ['insertImage', 'insertLink']
      ];
      taOptions.classes = {
        focussed: '',
        toolbar: 'ta-toolbar',
        toolbarGroup: 'ta-button-group',
        toolbarButton: '',
        toolbarButtonActive: 'md-primary',
        disabled: 'disabled',
        textEditor: 'ta-text-editor',
        htmlEditor: 'md-input'
      };

      return taOptions;
    }]);

    $provide.decorator('taTools', ['$delegate', function (taTools) {
      taTools.h1.display = '<md-button aria-label="Heading 1">H1</md-button>';
      taTools.h2.display = '<md-button aria-label="Heading 2">H2</md-button>';
      taTools.h3.display = '<md-button aria-label="Heading 3">H3</md-button>';
      taTools.p.display = '<md-button aria-label="Paragraph">P</md-button>';
      taTools.pre.display = '<md-button aria-label="Pre">pre</md-button>';
      taTools.quote.display = '<md-button class="md-icon-button" aria-label="Quote"><md-icon md-font-set="material-icons">format_quote</md-icon></md-button>';
      taTools.bold.display = '<md-button class="md-icon-button" aria-label="Bold"><md-icon md-font-set="material-icons">format_bold</md-icon></md-button>';
      taTools.italics.display = '<md-button class="md-icon-button" aria-label="Italic"><md-icon md-font-set="material-icons">format_italic</md-icon></md-button>';
      taTools.underline.display = '<md-button class="md-icon-button" aria-label="Underline"><md-icon md-font-set="material-icons">format_underlined</md-icon></md-button>';
      taTools.ul.display = '<md-button class="md-icon-button" aria-label="Buletted list"><md-icon md-font-set="material-icons">format_list_bulleted</md-icon></md-button>';
      taTools.ol.display = '<md-button class="md-icon-button" aria-label="Numbered list"><md-icon md-font-set="material-icons">format_list_numbered</md-icon></md-button>';
      taTools.justifyLeft.display = '<md-button class="md-icon-button" aria-label="Align left"><md-icon md-font-set="material-icons">format_align_left</md-icon></md-button>';
      taTools.justifyRight.display = '<md-button class="md-icon-button" aria-label="Align right"><md-icon md-font-set="material-icons">format_align_right</md-icon></md-button>';
      taTools.justifyCenter.display = '<md-button class="md-icon-button" aria-label="Align center"><md-icon md-font-set="material-icons">format_align_center</md-icon></md-button>';
      taTools.justifyFull.display = '<md-button class="md-icon-button" aria-label="Justify"><md-icon md-font-set="material-icons">format_align_justify</md-icon></md-button>';
      taTools.clear.display = '<md-button class="md-icon-button" aria-label="Clear formatting"><md-icon md-font-set="material-icons">format_clear</md-icon></md-button>';
      taTools.html.display = '<md-button aria-label="Show HTML">HTML<md-icon md-font-set="material-icons">code</md-icon></md-button>';
      taTools.insertLink.display = '<md-button class="md-icon-button" aria-label="Insert link"><md-icon md-font-set="material-icons">insert_link</md-icon></md-button>';
      taTools.insertImage.display = '<md-button class="md-icon-button" aria-label="Insert photo"><md-icon md-font-set="material-icons">insert_photo</md-icon></md-button>';
      return taTools;
    }]);
  }
})();
