(function () {
  'use strict';

  angular
    .module('linktub')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
  }
})();
