(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('AuthInterceptor', interceptor);

  /** @ngInject */
  function interceptor($q, $rootScope, SessionService, HTTP_STATUS) {
    ////////// Attributes //////////
    var xhrCount = 0;

    var interceptor = {
      request: function (config) {
        incrementCounter(config);
        setupCredentials(config);
        return config;
      },

      requestError: function (config) {
        decrementCounter(config);
        return config;
      },

      response: function (res) {
        decrementCounter(res.config);
        return res;
      },

      responseError: function (res) {
        decrementCounter(res.config);
        clearCredentials(res);
        return $q.reject(res);
      }
    }

    return interceptor;

    ////////// Functions //////////

    function setupCredentials(config) {
      config.headers['Accept'] = 'application/json';
      if (SessionService.isloggedIn()) {
        config.headers['X-User-Token'] = SessionService.sessionToken();
      }
    }

    function incrementCounter(config) {
      if (!config.ignoreLoading) {
        xhrCount++;
        updateStatus();
      }
    }

    function decrementCounter(config) {
      if (!config.ignoreLoading) {
        xhrCount--;
        updateStatus();
      }
    }

    function updateStatus() {
      $rootScope.isLoading = xhrCount > 0;
    }

    function clearCredentials(response) {
      if (response.status == HTTP_STATUS.UNAUTHORIZED) {
        $rootScope.$emit('unauthorized', 'Your account has been disconnected!');
        SessionService.clearSession();
      }
    }
  }
})();
